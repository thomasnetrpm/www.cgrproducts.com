<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 			<section class="site-intro clearfix">
        <div class="si-content-container clearfix">
          <div class="si-content clearfix">
            <h1 class="si-title">Custom Cutting and Fabricating Services</h1>
            <p class="si-desc">CGR Products’ custom cutting and fabricating capabilities transform materials into high-quality, precision components.</p>
            <p class="si-cta-desc">Find Your Solutions:</p>
            <a href="/materials" class="btn si-cta si-cta-1"><span>View Our</span> Materials</a><a href="/capabilities" class="btn si-cta si-cta-2" ><span>View Our</span>Capabilities</a>
          </div>
        </div> 
      </section>

      <section class="capabilities-section">
        <div class="inner-wrap">
          <h2 class="cs-title">Cutting Capabilities<span class="cs-title-detail">for gaskets and seals</span></h2>
          <div>
            <a href="/cutting/rotary-die" class="cs-bucket">
              <h4 class="cs-bucket-title">Die Cutting</h4>
              <p class="cs-bucket-desc">Rotary Die Cutting & Flatbed Die Cutting Services</p>
            </a>
            <a href="/cutting/kiss" class="cs-bucket">
              <h4 class="cs-bucket-title">Kiss Cutting</h4>
              <p class="cs-bucket-desc">Precision Cutting for Pressure-Sensitive Materials</p>
            </a>
            <a href="/cutting/knife" class="cs-bucket">
              <h4 class="cs-bucket-title">Knife Cutting</h4>
              <p class="cs-bucket-desc">Dieless Cutting for Flexibility</p>
            </a>
            <a href="/cutting/custom-slitting" class="cs-bucket">
              <h4 class="cs-bucket-title">Slitting / Splitting  </h4>
              <p class="cs-bucket-desc">Shearing for Large Rolls and Sheets</p>
            </a>
            <a href="/cutting/waterjet" class="cs-bucket">
              <h4 class="cs-bucket-title">Waterjet Cutting</h4>
              <p class="cs-bucket-desc">Fewer Maintenance, Enhanced Versatility</p>
            </a>
          </div>
        </div>
      </section>

      <section class="offer-section">
        <div class="inner-wrap">
          <div class="offer-section-left">
              <div class="os-image">
                <img src="<?php bloginfo('template_url'); ?>/img/resource-img.jpg">
              </div>
              
              <div class="os-rich-text">
                <h4 class="os-title"><a href="http://info.cgrproducts.com/chemical-compatibility-guide"><span class="os-category">Free Resource:</span>The Ultimate Chemical Compatibility Guide</a></h4>
                <p class="os-desc">Get the all-inclusive rubber materials guide to their chemical ratings, resistance, and other material properties.</p>
                <a href="http://info.cgrproducts.com/chemical-compatibility-guide" class="btn-invert" >Download Now</a>

                <a href="http://staging.cgrproducts.com/resource-library" class="os-all-link">View All Resources &raquo</a>
              </div>
          </div>

          <div class="offer-section-right">
              <div class="os-image">
                <img src="<?php bloginfo('template_url'); ?>/img/case-study-img.jpg">
              </div>
              
              <div class="os-rich-text">
                <h4 class="os-title"><a href="http://staging.cgrproducts.com/ensolite-case-study"><span class="os-category">Case Study:</span>Sealing Diesel Exhaust Fluid with Precision-Cut Ensolite Foam</a></h4>
                <p class="os-desc">Learn how CGR saved one client millions in replacement costs with the precision cutting of Ensolite foam.</p>
                <a href="http://staging.cgrproducts.com/ensolite-case-study" class="btn-invert">Learn More</a>
                <a href="http://staging.cgrproducts.com/case-studies/" class="os-all-link">View All Case Studies &raquo</a>
              </div> 
          </div>
        </div>
      </section>

      <section class="services-section">
        <div class="inner-wrap">
          <h2 class="ss-title">Custom Fabrication<span class="ss-title-detail">for gaskets and seals</span></h2>

          <a href="/custom-fabrication/molded-rubber-parts" class="btn">Molding</a>
          <a href="/custom-fabrication/extruded-rubber-parts" class="btn">Extrusion</a>
          <a href="/laminated-materials" class="btn">Laminating</a>
          <a href="/custom-fabrication/beaded-gaskets" class="btn">Beaded</a>
        </div>
      </section>

    </section><!--site-content-wrap END-->

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>