<?php
/*
		Template Name: Portfolio Gallery
*/
 
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap">
					<h1 class="scp-page-title">
						<?php if(get_field('alternative_h1')){
                echo get_field('alternative_h1');
            }
            else {
                the_title();
            }
            ?>
					</h1>

						<?php the_content(); ?>
	        <article class="col-12">  
						<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
						<h3>Select By Industry</h3>
						<section class="portfolio-section">	
						<?php
						$port_categories = get_terms( 'portfolio_category');
						?>
						<a href="#" class="active all-market portfolio-menu">All Markets</a>
						<?php foreach( $port_categories as $port_cat ) :
						?>
						<a class="portfolio-menu portfolio_category-<?php  echo $port_cat->slug;?>" href="#"><?php echo $port_cat->name; ?></a>
						<?php endforeach; wp_reset_query(); 
						$type = 'portfolio';
						$args=array(
						'post_type' => $type,
						'post_status' => 'publish',
						'posts_per_page' => -1
						);
						?>
						<div class="portfolio-item">
						<?php 
						$my_query = null;
						$my_query = new WP_Query($args);
						if( $my_query->have_posts() ) {
						while ($my_query->have_posts()) : $my_query->the_post(); ?>
						<?php foreach(get_the_category() as $category) {
echo $category->slug . ' '; } ?>
						<a href="<?php the_post_thumbnail_url('large'); ?>" <?php post_class(); ?>><?php the_post_thumbnail(array(480,480));?>
						<div class="port-title"><?php the_title(); ?></div>
						<div class="enlarge">Enlarge Image</div>
						</a>
						<?php
						endwhile;
						}
						wp_reset_query();  // Restore global post data stomped by the_post().
						?>

						</div>
						</section>
	        </article>
	       	 <?php // Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>
		</section>

<?php endwhile;?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/recommended-resources' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/cta-banner-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>