<!--Site Footer-->
<!--Site Footer-->
    <footer class="site-footer" role="contentinfo">
      <div class="inner-wrap">
        <div class="f-bucket f-twitter clearfix">
          <img class="f-icon" src="<?php bloginfo('template_url'); ?>/img/twitter-img.svg">
          <div class="f-content-group">
            <p class="f-desc"><div id="rss-feeds"></div></p>
            <div>
              
            </div>
            <a href="https://twitter.com/CGRProducts" class="btn-invert" target="_blank">Follow us on Twitter</a>
          </div>
        </div>
        <div class="f-bucket f-blog clearfix">
          <img class="f-icon" src="<?php bloginfo('template_url'); ?>/img/blog-img.svg">
          <div class="f-content-group">
              <?php
                $latest_post = new WP_Query("showposts=1");
                if($latest_post->have_posts()) :
              ?>
                <?php
                  while($latest_post->have_posts()):
                  $latest_post->the_post();
                ?>
                  <p class="f-desc"><?php the_title() ?></p>
                  <a href="<?php the_permalink(); ?>" class="btn-invert">Latest Blog Post</a> 
                <?php endwhile ?>
              <?php endif ?>
          </div>
        </div>
        <div class="f-bucket f-contact clearfix">
          <img class="f-icon" src="<?php bloginfo('template_url'); ?>/img/pin-img.svg">
          <div class="f-content-group">
            <p class="f-desc">Greensboro, NC<br> Decatur, AL <br>Waukesha, WI</p>
            <a href="//info.cgrproducts.com/contact-us" class="btn-invert">Contact Us</a>
          </div>
        </div>
      </div>
      <div class="sf-small">
        <div class="inner-wrap">
          <p>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> | Website by <a href="//business.thomasnet.com/rpm">ThomasNet RPM</a> | <a href="/sitemap">Sitemap</a> | <a href="/privacy-policy">Privacy Policy</a></p>
        </div>
      </div>
    </footer>
  </div>
  <!--site-wrap END -->