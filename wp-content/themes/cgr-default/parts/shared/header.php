<!--Site Header-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module' ) ); ?>
<header class="site-header" role="banner">
  <!--<div class="site-utility-nav gdd-004-a-site-utility-nav">
    <div class="inner-wrap">
      <div class="social-wrap tablet">
        <a href="https://www.facebook.com/pages/CGR-Products/396319040465923" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg"  alt="Facebook"></a>
        <a href="http://www.linkedin.com/company/cgr-products-inc" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg"  alt="Linked In"></a>
        <a href="https://twitter.com/CGRProducts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" alt="Twitter"></a>
        <a href="https://plus.google.com/u/0/101370404909606206409/about" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" alt="Google Plus"></a>
        <a href="/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" alt="Blog"></a>
        <a href="https://www.youtube.com/channel/UCJ145hQk4B53mWXeiqjsFMg" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-youtube.svg" alt="youtube"></a>
      </div>
      <a href="mailto:info@cgrproducts.com" class="sun-item sh-email"></a>
      <a href="//info.cgrproducts.com/request-quote" class="sun-item sun-rfq">Request Quote</a>
      <a href="tel:877-313-6785" class="sun-item sh-ph">877-313-6785</a>
      <a href="//info.cgrproducts.com/contact-us" class="sun-item" target="_blank">Contact Us</a>
      <!-- <a href="http://gasket-seal-foam-tape.cgrproducts.com/" class="sun-item" >Online Catalog</a> -->
      <!--<a href="/blog" class="sun-item">Blog</a>
      <span class="sh-icons">
        <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
      </span>
    </div>
  </div>-->
  <!-- GDD 004 B Variation -->
  <div class="site-utility-nav gdd-004-b-site-utility-nav">
    <div class="inner-wrap">
      <div class="social-wrap tablet">
        <a href="https://www.facebook.com/pages/CGR-Products/396319040465923" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg"  alt="Facebook"></a>
        <a href="http://www.linkedin.com/company/cgr-products-inc" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg"  alt="Linked In"></a>
        <a href="https://twitter.com/CGRProducts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" alt="Twitter"></a>
        <a href="https://plus.google.com/u/0/101370404909606206409/about" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" alt="Google Plus"></a>
        <a href="/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" alt="Blog"></a>
        <a href="https://www.youtube.com/channel/UCJ145hQk4B53mWXeiqjsFMg" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-youtube.svg" alt="youtube"></a>
      </div>
      <span class="gdd-004-b-utility-bar">
        <a href="https://paymnt.io/6qtYYc" target="_blank" class="sun-rfq sun-pay">Pay Bill</a>
        <a href="//info.cgrproducts.com/request-quote" class="sun-rfq">Request Quote</a>
        <a href="//info.cgrproducts.com/contact-us" class="sh-contact-us" target="_blank">Contact Us</a>
        <a href="/blog" class="sun-item sh-blog" >Blog</a>
        <a href="tel:3366214568" class="sun-item sh-ph">336-621-4568</a>
      </span>
      <span class="sh-icons">
        <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
      </span>
    </div>
  </div>
  <div class="inner-wrap sh-logo-nav-wrap">
    <a href="/" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/site-logo.jpg" alt="Site Logo"></a>
    <!--Site Nav-->
    <div class="site-nav-container">
      <div class="snc-header">
        <a href="" class="close-menu menu-link">Close</a>
      </div>
      <?php wp_nav_menu(array(
        'menu'            => 'Primary Nav',
        'container'       => 'nav',
        'container_class' => 'site-nav',
        'menu_class'      => 'sn-level-1',
        'walker'        => new themeslug_walker_nav_menu
        )); ?>
      <a class="sh-ico-search search-link" target="_blank" href="#"><span>Search</span></a>
    </div>
    <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
    <hr>
    <!--Site Nav END-->
  </div>
  <!--inner-wrap END-->
</header>
<!--Site Content-->
<section class="site-content-wrap">
