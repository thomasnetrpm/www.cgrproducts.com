<!--Secondary Content-->
<aside class="site-content-secondary col-3">

	<?php if(get_field('custom_featured_img') ): ?>
		<div class="featured-img">
			<?php echo get_field('custom_featured_img'); ?>
		</div>
	<?php endif; ?>

	<!-- Aside CTAs -->
	<?php if(get_field('aside_cta') ): ?>
	<div class="cta-aside">
	<?php echo get_field('aside_cta'); ?>
	</div>                 
	<?php elseif(get_field('global_aside_cta','option') ): ?>
	<div class="cta-aside">
	<?php echo get_field('global_aside_cta','option'); ?>
	</div>
	<?php endif; ?>

	<!-- Page Conditionals -->

   <?php if (is_page( '95' ) || '95' == $post->post_parent || '227' == $post->post_parent) : ?>
   	<div class="gdd-cta-006-a">
		<h3 class="scs-nav-title">Materials</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Materials',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
</div>

 




   <?php elseif (is_page( '122' ) || '122' == $post->post_parent || '124' ==  $post->post_parent || '130' ==  $post->post_parent || '127' ==  $post->post_parent) :  ?>
		<h3 class="scs-nav-title">Capabilities</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Capabilities',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav', 
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>


   <?php elseif (is_page( '136' ) || '136' == $post->post_parent) : ?>
		<h3 class="scs-nav-title">About</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'About',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     
   <?php elseif (is_page( '140' ) || '140' == $post->post_parent) : ?>
		<h3 class="scs-nav-title">Case Studies</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Case Studies',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>

	<?php endif; ?>




	<!-- Additional Aside Content -->
	<?php if(get_field('additional_aside_content') ): ?>
		<?php echo get_field('additional_aside_content'); ?>
	</div>                 
	<?php elseif(get_field('global_additional_aside_content','option') ): ?>
		<?php echo get_field('additional_additional_aside_content'); ?>
	<?php endif; ?>
</aside>



