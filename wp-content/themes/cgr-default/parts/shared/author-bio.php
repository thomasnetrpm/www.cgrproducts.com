<div class="author-bio">
	       		<figure><?php echo get_avatar( get_the_author_meta('email') , 90 ); ?></figure>
	       		<div><h3><?php echo the_author_posts_link(); ?>
	       			<?php if ( get_the_author_meta( 'linkedin' ) ) : ?>
	       		 <a target="_blank" href="<?php echo get_the_author_meta('linkedin'); ?>"><img alt="Linked In" src="<?php bloginfo('template_url'); ?>/img/linkedin.png"></a>
	       		<?php endif; ?>
	       		</h3>
	       		<p><?php echo get_the_author_meta('description'); ?></p></div>
	       	</div>