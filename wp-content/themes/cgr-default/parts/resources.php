<div><!-- row -->
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/chemical-resistance-ebook.jpg" alt="General Chemical Resistance Chart">
		<h3 class="resource-title">General Chemical Resistance Chart</h3>
		<p>Ensure your material can really resist any chemicals it encounters.</p>
		<a href="//info.cgrproducts.com/chemical-compatibility-guide" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/specification-fluids-cover.png" alt="Specification Fluids Chart" />
		<h3 class="resource-title">Fluid Specifications Chart</h3>
		<p>Get the fluid resistance rating for your material.</p>
		<a href="//info.cgrproducts.com/specification-fluids-chart" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/costs-and-temperatures-screenshot.png" alt="Approximate Temperature Ranges">
		<h3 class="resource-title">Temperature Ranges and Relative Costs for Polymers</h3>
		<p>Find the cost and temperature range of the polymer for your application.</p>
		<a href="//info.cgrproducts.com/polymer-temperature-ranges-costs" class="btn resource-download">Download</a>
	</div>
</div>

<div><!-- row -->
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/iso-certs.png" alt="">
		<h3 class="resource-title">ISO Certificates</h3>
		<p>Download our ISO certificates for your records.</p>
		<a href="//info.cgrproducts.com/iso-certifications" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/pdf-appliance.jpg" alt="">
		<h3 class="resource-title">Applications for the Appliance Industry</h3>
		<p>Learn how CGR's precision manufacturing capabilities serve the appliance industry.</p>
		<a href="//info.cgrproducts.com/cgr-applications-for-appliance-industry" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/pdf-car.jpg" alt="">
		<h3 class="resource-title">Applications for the Automotive Market </h3>
		<p>Learn how CGR's custom-shaped components serve the automotive industry.</p>
		<a href="//info.cgrproducts.com/cgr-applications-for-automotive-industry" class="btn resource-download">Download</a>
	</div>
</div>

<div><!-- row -->
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/pdf-boat.jpg" alt="">
		<h3 class="resource-title">Applications for the Marine Industry</h3>
		<p>Learn how CGR can meet the challenges of the marine industry.</p>
		<a href="//info.cgrproducts.com/cgr-applications-for-marine-industry" class="btn resource-download">Download</a>
	</div>
  <div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/3M_VHB_tapes.png" alt="">
		<h3 class="resource-title">Get the Facts on 3M VHB Tapes</h3>
		<p>Access performance data and learn how 3M VHB tape can benefit your application.</p>
		<a href="//info.cgrproducts.com/3m-vhb-tapes" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/acrylic-adhesives-covers.jpg" alt="">
		<h3 class="resource-title">Access Data Library of 3M Acrylic Adhesives</h3>
		<p>Get Performance Data, Product Descriptions and More.</p>
		<a href="//info.cgrproducts.com/catalog-library-of-acrylic-adhesives" class="btn resource-download">Download</a>
	</div>
</div>

<div style="clear:left;"><!-- row -->
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/5-design-mistakes.jpg" alt="">
		<h3 class="resource-title">Common Mistakes for Converting Flexible Materials</h3>
		<p>We have come across a number of common design mistakes that can easily be avoided. </p>
		<a href="//info.cgrproducts.com/5-common-design-mistakes-ebook" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="<?php bloginfo('template_url'); ?>/img/ul.jpg" alt="">
		<h3 class="resource-title">Access Our UL Certification</h3>
		<p>CGR Products offers three UL Recognized Components of sponge rubber and pressure sensitive adhesive. </p>
		<a href="//info.cgrproducts.com/ul-certification" class="btn resource-download">Download</a>
	</div>
	<div class="resource-bucket">
		<img class="resource-img" src="//cdn2.hubspot.net/hub/497415/hubfs/rma-tolerances-table.png%3Ft=1450297021179&width=616" alt="">
		<h3 class="resource-title">RMA Tolerances Table for Die-Cut Parts</h3>
		<p>Includes tolerance ranges of rubber materials for material thickness and customer requirements.</p>
		<a href="//info.cgrproducts.com/rma-tolerances-sheet" class="btn resource-download">Download</a>
	</div>
</div>
