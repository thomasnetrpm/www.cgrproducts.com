<div class="featured-resource-library">
	<div class="inner-wrap">
        	<a href="//info.cgrproducts.com/lp-adhesive-materials-checklist"><img src="<?php bloginfo('template_url'); ?>/img/img-3d-checklist.png" class="fr-img"></a>
        	<div>
            <a href="//info.cgrproducts.com/lp-adhesive-materials-checklist"><h2 class="fr-heading">Featured Resource:</h2></a>
            <p class="fr-descp">Be prepared for your next adhesive material project.</p>
            <a href="//info.cgrproducts.com/lp-adhesive-materials-checklist" class="fr-link btn-invert">Get your checklist &raquo;</a>
            </div>
     </div>
</div>