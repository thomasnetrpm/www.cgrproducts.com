<section class="recommended-resources gdd-002-recommended-resources">
	<div class="inner-wrap">
	<h3 class="rr-heading">Recommended Resources for You</h3>
		<div class="rows-of-3">
			

<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-f475b948-709f-429f-b33b-96dba70f2007"><span class="hs-cta-node hs-cta-f475b948-709f-429f-b33b-96dba70f2007" id="hs-cta-f475b948-709f-429f-b33b-96dba70f2007"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/497415/f475b948-709f-429f-b33b-96dba70f2007" ><img class="hs-cta-img" id="hs-cta-img-f475b948-709f-429f-b33b-96dba70f2007" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/497415/f475b948-709f-429f-b33b-96dba70f2007.png"  alt="Chemical Compatibility Guide"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(497415, 'f475b948-709f-429f-b33b-96dba70f2007', {}); </script></span><!-- end HubSpot Call-to-Action Code -->


<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-d42885e2-4dd4-4620-9d9b-059ed6b57ce7"><span class="hs-cta-node hs-cta-d42885e2-4dd4-4620-9d9b-059ed6b57ce7" id="hs-cta-d42885e2-4dd4-4620-9d9b-059ed6b57ce7"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/497415/d42885e2-4dd4-4620-9d9b-059ed6b57ce7" ><img class="hs-cta-img" id="hs-cta-img-d42885e2-4dd4-4620-9d9b-059ed6b57ce7" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/497415/d42885e2-4dd4-4620-9d9b-059ed6b57ce7.png"  alt="Common Design Mistakes"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(497415, 'd42885e2-4dd4-4620-9d9b-059ed6b57ce7', {}); </script></span><!-- end HubSpot Call-to-Action Code -->


<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-22c55591-1650-4d9e-b95f-3dd55051b201"><span class="hs-cta-node hs-cta-22c55591-1650-4d9e-b95f-3dd55051b201" id="hs-cta-22c55591-1650-4d9e-b95f-3dd55051b201"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/497415/22c55591-1650-4d9e-b95f-3dd55051b201" ><img class="hs-cta-img" id="hs-cta-img-22c55591-1650-4d9e-b95f-3dd55051b201" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/497415/22c55591-1650-4d9e-b95f-3dd55051b201.png"  alt="Adhesive Materials Checklist"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(497415, '22c55591-1650-4d9e-b95f-3dd55051b201', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
		</div>
	</div>
</section>