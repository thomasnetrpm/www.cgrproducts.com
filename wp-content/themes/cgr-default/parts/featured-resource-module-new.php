<div class="featured-resource gdd-005-a-resource-library-infographic">
  <div class="inner-wrap-wide">
  <div class="fr-wrap">
         <?php if(get_field('fr_title')):?><h2 class="fr-title"><?php echo get_field('fr_title');?></h2><?php endif;?>
         <?php if(get_field('fr_subtext')):?><p class="fr-subtext"><?php echo get_field('fr_subtext');?></p><?php endif;?>
         <?php if(get_field('fr_link')):?><a class="btn fr-btn" href="<?php echo get_field('fr_link');?>"><?php if(get_field('fr_cta')):?><?php echo get_field('fr_cta');?><?php endif;?></a><?php endif;?>
     </div>
     </div>
</div>


