<div class="gdd-005-b-resource-library-infographic">
<h2 class="rl-header">Infographic</h2>
<div class="rl-featured-resource resource-library-infographic">
  <div class="inner-wrap">
  <div class="rli-wrap">
         <?php if(get_field('rli-title')):?><h2 class="rli-title"><?php echo get_field('rli-title');?></h2><?php endif;?>
         <?php if(get_field('rli_subtext')):?><p class="rli_subtext"><?php echo get_field('rli_subtext');?></p><?php endif;?>
         <?php if(get_field('fr_link')):?><a class="btn  rli-btn" href="<?php echo get_field('rli_link');?>"><?php if(get_field('rli_text')):?><?php echo get_field('rli_text');?><?php endif;?></a><?php endif;?>
     </div>
     </div>
</div>
<hr>
</div>
