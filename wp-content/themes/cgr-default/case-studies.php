<?php

	/*
		Template Name: Case Studies
	*/
?>
   
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
      
	<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap">
					<h1 class="scp-page-title">
						<?php if(get_field('alternative_h1')){
                echo get_field('alternative_h1');
            }
            else {
                the_title();
            }
            ?>
					</h1>
            <article class="site-content-primary col-9"> 
                <p>See some of the projects CGR Products has completed to learn more about our full scale of materials and capabilities.</p>
                <div class="case-study">
               <?php 
				$type = 'page';

$args=array(

 'post_type' => $type,

 'posts_per_page' => -1,
 
 'post_parent' => 140,

// 'caller_get_posts'=> 1

);

$my_query = null;

$my_query = new WP_Query($args);

if( $my_query->have_posts() ) {

 while ($my_query->have_posts()) : $my_query->the_post(); 
					?>
                    <div>
                    <?php 
					the_post_thumbnail() ?>
                    <div>
					<h3><a href="<?php echo the_permalink();?>"><?php the_title(); ?></a>
                    </h3>
					<p><?php  echo excerpt(20); ?></p>
					<p class="learn-more"><a class="btn-invert" href="<?php echo the_permalink();?>">Learn More</a></p>
                    </div>
                   </div>
     
     <?php

 endwhile;

}

wp_reset_query();  // Restore global post data stomped by the_post().

?>                 
                </div>
                
            </article>
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>    
		</div>
	</section>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/recommended-resources' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/cta-banner-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>