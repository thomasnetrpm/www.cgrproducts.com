<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 			<section class="site-intro clearfix">
        <div class="si-video"><video width="100%" autoplay playsinline loop muted>
          <source src="<?php echo get_field('si_video');?>" type="video/mp4">
      </video>
      </div> 
        <div class="si-content-container clearfix">
          <div class="inner-wrap">
          <div class="si-content clearfix">
            <h1 class="si-title">Custom Cutting and Fabricating Services</h1>
            <p class="si-desc">CGR Products’ custom cutting and fabricating capabilities transform materials into high-quality, <br>precision components.</p>
    
            <div class="si-button">
            <a href="https://info.cgrproducts.com/lp-request-quote" class="si-cta si-cta-2" >Request a Quote</a>
          </div>
          </div>
        </div>
        </div> 
      </section>

      <section class="capabilities-section">
        <div class="inner-wrap">
          <h2 class="cs-title">Cutting Capabilities<span class="cs-title-detail">for gaskets and seals</span></h2>
          <div>
            <a href="/cutting/rotary-die" class="cs-bucket">
              <h4 class="cs-bucket-title">Die Cutting</h4>
              <p class="cs-bucket-desc">Rotary Die Cutting & Flatbed Die Cutting Services</p>
            </a>
            <a href="/cutting/kiss" class="cs-bucket">
              <h4 class="cs-bucket-title">Kiss Cutting</h4>
              <p class="cs-bucket-desc">Precision Cutting for Pressure-Sensitive Materials</p>
            </a>
            <a href="/cutting/knife" class="cs-bucket">
              <h4 class="cs-bucket-title">Knife Cutting</h4>
              <p class="cs-bucket-desc">Dieless Cutting for Flexibility</p>
            </a>
            <a href="/cutting/custom-slitting" class="cs-bucket">
              <h4 class="cs-bucket-title">Slitting / Splitting  </h4>
              <p class="cs-bucket-desc">Shearing for Large Rolls and Sheets</p>
            </a>
            <a href="/cutting/waterjet" class="cs-bucket">
              <h4 class="cs-bucket-title">Waterjet Cutting</h4>
              <p class="cs-bucket-desc">Fewer Maintenance, Enhanced Versatility</p>
            </a>
          </div>
        </div>
      </section>


<section class="additional-resource-section gdd-005-b-homepage" style="display: none;">
        <div class="inner-wrap">
        <div class="rows-of-3">
        <div class="ars-image">
<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-f0038eaa-6829-4e01-9494-eaec1924f2db"><span class="hs-cta-node hs-cta-f0038eaa-6829-4e01-9494-eaec1924f2db" id="hs-cta-f0038eaa-6829-4e01-9494-eaec1924f2db"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/497415/f0038eaa-6829-4e01-9494-eaec1924f2db"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-f0038eaa-6829-4e01-9494-eaec1924f2db" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/497415/f0038eaa-6829-4e01-9494-eaec1924f2db.png" /></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(497415, 'f0038eaa-6829-4e01-9494-eaec1924f2db', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
</div>
 <div class="ars-featured-resource">
  <h2 class="fr-title">Featured Resource</h2>
<h3 class="fr-title-info">UL Listed Closed Cell Sponge Rubber with Adhesive</h3>
<p class="fr-intro">Many users of sponge rubber products for sealing are unaware that UL guidelines dictate that when adhesive is applied to sponge rubber the chemistry has been changed....</p>
<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-13ec83d1-3af0-4e20-80f1-cf1bd08379eb"><span class="hs-cta-node hs-cta-13ec83d1-3af0-4e20-80f1-cf1bd08379eb" id="hs-cta-13ec83d1-3af0-4e20-80f1-cf1bd08379eb"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/497415/13ec83d1-3af0-4e20-80f1-cf1bd08379eb"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-13ec83d1-3af0-4e20-80f1-cf1bd08379eb" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/497415/13ec83d1-3af0-4e20-80f1-cf1bd08379eb.png"  alt="Read More"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(497415, '13ec83d1-3af0-4e20-80f1-cf1bd08379eb', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
 </div>
<div class="ars-additional-resources">
  <h3>Additional Resources:</h3>
<a href="/resource-library#white-papers"><span class="btn-important">White Papers & Design Guides</span></a>
<p></p>
<a href="/resource-library#material-data"><span class="btn-important">Material Data</span></a>
<p></p>
<a href="/resource-library#certifications"><span class="btn-important">Certifications</span></a>
<p></p>
<a href="/resource-library#infographics"><span class="btn-important">Infographics</span></a>
</div>
</div>
</div>
</section>



      <section class="view-work-section">
        <div class="inner-wrap">
          <h2>View Our Work</h2>
          <p>Gaskets, die-cut tapes, adhesives and more. See what CGR has created for your industry.</p>
          <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
          <a href="/sample-gallery" class="btn view-gal-btn">Visit Sample Gallery</a>
        </div>
      </section>

      <section class="services-section">
        <div class="inner-wrap">
          <h2 class="ss-title">Material Selection<span class="ss-title-detail">CGR works with a variety of materials to fit your application</span></h2>
          <div>
          <a href="/materials/tapes-and-adhesives/" class="btn">Tapes & Adhesives</a>
          <a href="/materials/fiber-materials/" class="btn">Fiber Materials</a>
          <a href="/materials/open-and-closed-cell-foam-materials/" class="btn">Open/Closed-Cell Foam</a>
          <a href="/materials/solid-rubber/" class="btn">Solid Rubber</a>
          <a href="/materials/miscellaneous-materials/" class="btn">Miscellaneous</a>
        </div>
        </div>
      </section>

      <!--<section class="services-section">
        <div class="inner-wrap">
          <h2 class="ss-title">Custom Fabrication<span class="ss-title-detail">for gaskets and seals</span></h2>

          <a href="/custom-fabrication/molded-rubber-parts" class="btn">Molding</a>
          <a href="/custom-fabrication/extruded-rubber-parts" class="btn">Extrusion</a>
          <a href="/laminated-materials" class="btn">Laminating</a>
          <a href="/custom-fabrication/beaded-gaskets" class="btn">Beaded</a>
        </div>
      </section>-->

    </section><!--site-content-wrap END-->

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox','parts/shared/footer','parts/shared/html-footer' ) ); ?>