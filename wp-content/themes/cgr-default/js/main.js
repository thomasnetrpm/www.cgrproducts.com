//PNG Fallback
if (!Modernizr.svg) {
  var images = $('img[data-png-fallback]');
  images.each(function(i) {
    $(this).attr('src', $(this).data('png-fallback'));
  });
}

//Toggle Boxes
$(document).ready(function() {
  $('body').addClass('js');
  var $activatelink = $('.activate-link');

  $activatelink.click(function() {
    var $this = $(this);
    $this.toggleClass('active').next('div').toggleClass('active');
    return false;
  });

});

//Responsive Navigation
$(document).ready(function() {
  $('body').addClass('js');
  var $menu = $('.site-nav-container'),
    $menulink = $('.menu-link'),
    $menuTrigger = $('.menu-item-has-children > a'),
    $searchLink = $('.search-link'),
    $siteSearch = $('.search-module'),
    $siteWrap = $('.site-wrap');

  $searchLink.click(function(e) {
    e.preventDefault();
    $searchLink.toggleClass('active');
    $siteSearch.toggleClass('active');
  });

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
    $siteWrap.toggleClass('nav-active');
  });

  $menuTrigger.click(function(e) {
    //e.preventDefault();
    var $this = $(this);
    $this.toggleClass('active').next('ul').toggleClass('active');
  });

});

/*********** Portfolio Lightbox addition ************/
$(document).ready(function(){
  $('.portfolio-item a').addClass('lightbox');
});

//Lightbox
$(document).ready(function() {
  $('.lightbox').magnificPopup({
    type: 'image'
  });
});
$(document).ready(function() {
  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });
});



//Show More
$(document).ready(function() {
  $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
  var $showmorelink = $('.showmore-link');
  $showmorelink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');
    $this.toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});

//Show More
$(document).ready(function() {
  var $expandlink = $('.headexpand');
  $expandlink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');


    $this.toggleClass('active').next().toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});
//Click to Expand
$(document).ready(function() {
  var $expandlink = $('.ce-header');
  $expandlink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');


    $this.parent().toggleClass('active'); 
    $showmorecontent.toggleClass('active');
    return false;
  });
});

//Flexslider    
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false
  });
});


//Flexslider    
$(window).load(function() {
  $('.flexslider-automotive-infographic').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: true,
    slideshow: false,
  });
});
//Flexslider    
$(window).load(function() {
  $('.flexslider-automotive-infographic-2').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: true,
    slideshow: false,
  });
});



/*
//Sticky Nav
$(function() {
 
    // grab the initial top offset of the navigation 
    var sticky_navigation_offset_top = $('.nav-wrap').offset().top;
     
    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop(); // our current vertical position from the top
         
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scroll_top > sticky_navigation_offset_top) { 
            $('.nav-wrap').addClass('nav-sticky');
        } else {
            $('.nav-wrap').removeClass('nav-sticky'); 
        }   
    };
     
    // run our function on load
    sticky_navigation();
     
    // and run it again every time you scroll
    $(window).scroll(function() {
         sticky_navigation();
    });
 
});



*/


//Slide in CTA
$(document).ready(function () {  var slidebox = $('#slidebox');
  if (slidebox) {
    /*$(window).scroll(function() {
      var distanceTop = $('#last').offset().top - $(window).height();
      if ($(window).scrollTop() > distanceTop)*/
        slidebox.animate({
          'right': '0px',
        }, 400);
      //else
        //slidebox.stop(true).animate({
         // 'right': '-430px',
        //}, 100);
    //});
    $('#slidebox .close').on('click', function() {
      $(this).parent().remove();
    });
  }
});


// Rss Feed
$(function($) {
  $("#rss-feeds").rss("", {
    limit: 1,
    effect: 'slideFastSynced'
  })
})

// Get URL Params to activate correct tables on /suppliers 
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
var findTarget = getQueryVariable("open");
$('#'+ findTarget).addClass('active');

/************  Portfolio Functionality  **************/
$(document).ready(function(){
  $(".portfolio-menu").click(function(e){
      e.preventDefault();
    var class_name = $(this).attr('class');
    var parts = class_name.split(' ');
    var loc = parts.pop();

     $('.portfolio-item a').fadeOut('slow');
     $('.portfolio-item').find('.' + loc).fadeIn('slow');

     if(loc=='portfolio-menu'){
       $('.portfolio-item a').fadeIn('slow');
   }

   
   $(".portfolio-menu").removeClass('active');
   $(this).addClass('active');
   $(this).removeClass(loc);
   $(this).addClass(loc);
});
});


// Dropdown Menu
var dropdown = document.querySelectorAll('.dropdown');
var dropdownArray = Array.prototype.slice.call(dropdown,0);
dropdownArray.forEach(function(el){
  var button = el.querySelector('a[data-toggle="dropdown"]'),
      menu = el.querySelector('.dropdown-menu'),
      arrow = button.querySelector('i.icon-arrow');

  button.onclick = function(event) {
    if(!menu.hasClass('show')) {
      menu.classList.add('show');
      menu.classList.remove('hide');
      arrow.classList.add('open');
      arrow.classList.remove('close');
      event.preventDefault();
    }
    else {
      menu.classList.remove('show');
      menu.classList.add('hide');
      arrow.classList.remove('open');
      arrow.classList.add('close');
      event.preventDefault();
    }
  };
})

Element.prototype.hasClass = function(className) {
    return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
};










// Materials Dropdown Menu
var dropdown = document.querySelectorAll('.dropdown');
var dropdownArray = Array.prototype.slice.call(dropdown,0);
dropdownArray.forEach(function(el){
  var button = el.querySelector('a[data-toggle="dropdown"]'),
      menu = el.querySelector('.dropdown-menu'),
      arrow = button.querySelector('i.icon-arrow');

  button.onclick = function(event) {
    if(!menu.hasClass('show')) {
      menu.classList.add('show');
      menu.classList.remove('hide');
      arrow.classList.add('open');
      arrow.classList.remove('close');
      event.preventDefault();
    }
    else {
      menu.classList.remove('show');
      menu.classList.add('hide');
      arrow.classList.remove('open');
      arrow.classList.add('close');
      event.preventDefault();
    }
  };
})

Element.prototype.hasClass = function(className) {
    return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
};


var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var sivideo,
    playerDefaults = {autoplay: 1, loop: 1, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};
var vid = [
      {'videoId': 'XpO9tijDYI8', 'startSeconds': 0}
    ],
    randomVid = Math.floor(Math.random() * vid.length),
    currVid = randomVid;

$('.hi em:last-of-type').html(vid.length);

function onYouTubePlayerAPIReady(){
  sivideo = new YT.Player('sivideo', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, playerVars: playerDefaults});
}

function onPlayerReady(){
  sivideo.loadVideoById(vid[0]);
  sivideo.mute();
}

function onPlayerStateChange(e) {
  if (e.data === 1){
    $('#sivideo').addClass('active');
    $('.hi em:nth-of-type(2)').html(currVid + 1);
  } else if (e.data === 2){
    $('#sivideo').removeClass('active');
    if(currVid === vid.length - 1){
      currVid = 0;
    } else {
      currVid++;  
    }
    sivideo.loadVideoById(vid[currVid]);
    sivideo.seekTo(vid[currVid].startSeconds);
  }
}

function vidRescale(){

  var w = $(window).width()+200,
    h = $(window).height()+200;

  if (w/h > 16/9){
    sivideo.setSize(w, w/16*9);
    $('.sivideo .screen').css({'left': '0px'});
  } else {
    sivideo.setSize(h/9*16, h);
    $('.sivideo .screen').css({'left': -($('.sivideo .screen').outerWidth()-w)/2});
  }
}

$(window).on('load resize', function(){
  vidRescale();
});

$('.hi span:first-of-type').on('click', function(){
  $('#sivideo').toggleClass('mute');
  $('.hi em:first-of-type').toggleClass('hidden');
  if($('#sivideo').hasClass('mute')){
    sivideo.mute();
  } else {
    sivideo.unMute();
  }
});

$('.hi span:last-of-type').on('click', function(){
  $('.hi em:nth-of-type(2)').html('~');
  sivideo.pauseVideo();
});
