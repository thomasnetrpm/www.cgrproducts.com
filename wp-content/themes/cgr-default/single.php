<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap">
	    	<h1><?php the_title(); ?></h1>
	        <article class="site-content-primary">
				<div class="post-meta">Posted by <a href="/"><?php the_author(); ?></a> on <time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?></time> | <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> </div>
				
				<?php the_content(); ?> 
				<hr>
				<p><?php the_tags(); ?></p>
				<?php if ( get_the_author_meta( 'description' ) ) : ?>
				<hr>
					<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/author-bio'  ) ); ?>
	    <?php endif; ?>
	       	<hr>
				<?php comments_template( '', true ); ?>

	        </article>
	       	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar-blog','parts/shared/sidebar','parts/shared/flexible-content'  ) ); ?>
	    
	       	

	    </div>
	</section>
<?php endwhile; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/recommended-resources' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>