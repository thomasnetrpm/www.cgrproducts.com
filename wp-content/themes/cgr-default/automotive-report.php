<?php
/*
		Template Name: Automotive Report
*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">

	<script>
		$(document).ready(function() {
			if (screen.width >= 480) {
				$('#fullpage').fullpage({
					scrollHorizontallyKey: 'Y2dycHJvZHVjdHMuY29tX1FyeGMyTnliMnhzU0c5eWFYcHZiblJoYkd4NTl4RQ==',
					scrollHorizontally: true,
					navigation: true,
					slidesNavigation: true,
					slidesNavPosition: 'bottom',
				});
			}
		});
	</script>

	<div id="fullpage">

	  <section class="section auto-1" data-anchor="auto-1">
	  	<div class="inner-wrap">
	  			<h1 class="scp-page-title">
						<?php if(get_field('alternative_h1')){echo get_field('alternative_h1');} else {the_title();} ?>
					</h1>
	  		<div class="col-6">
					<p>As technology evolves, the way we design cars is also evolving; with heightened awareness of manufacturing’s impact on the environment and the increasing interconnectivity of devices through the Internet of Things (IoT), automobiles are becoming both lighter and smarter.</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
				</div>
				<!-- <img class="auto-1-bg" src="http://www.cgrproducts.com/wp-content/themes/cgr-default/img/auto/bg-1.svg" alt="city outline"> -->

<!-- Generator: Adobe Illustrator 18.1.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg class="auto-1-bg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 3300 1650" enable-background="new 0 0 3300 1650" xml:space="preserve">
<g id="Background_x5F_Clouds">
	<g>
		<defs>
			<rect id="SVGID_1_" x="0" y="0" width="3300" height="1650"/>
		</defs>
		<clipPath id="SVGID_2_">
			<use xlink:href="#SVGID_1_"  overflow="visible"/>
		</clipPath>
		<path opacity="0.4" clip-path="url(#SVGID_2_)" fill="#EDF4F2" d="M3524.8,1235.5h206.3c64.2,0,116.3,52.1,116.3,116.3
			c0,64.2-52,116.3-116.3,116.3h-156.4c-12,2.4-21,12.8-21,25.5c0,14.1,11.3,25.6,25.3,26c-0.3,0-0.5,0-0.8,0h-1.9
			c63.5,0.9,114.7,52.5,114.7,116.2c0,64.2-52.1,116.3-116.3,116.3h-378.5c-3.8,3.4-6.2,8.3-6.2,13.8c0,10.2,8.1,18.4,18.2,18.7
			c-0.2,0-0.4,0-0.6,0h-1.4c45.5,0.6,82.3,37.7,82.3,83.4c0,46.1-37.4,83.5-83.5,83.5H1313.2c-46.1,0-83.4-37.4-83.4-83.5
			c0-46.1,37.4-83.5,83.4-83.5h92.4c9.9-0.5,17.8-8.6,17.8-18.6c0-5.5-2.4-10.4-6.2-13.8H975c-64.2,0-116.3-52.1-116.3-116.3
			c0-64.2,52-116.3,116.3-116.3h254c13.8-0.7,24.8-12,24.8-26c0-12.6-9-23.1-21-25.5h-26.3c-64.2,0-116.3-52.1-116.3-116.3
			c0-64.2,52.1-116.3,116.3-116.3h578c11.9-2.4,20.8-12.9,20.8-25.5c0-14.1-11.3-25.6-25.3-26c0.3,0,0.5,0,0.8,0h1.9
			c-63.5-0.9-114.6-52.5-114.6-116.2c0-64.2,52-116.3,116.3-116.3H3527c64.2,0,116.3,52.1,116.3,116.3
			c0,63.7-51.2,115.3-114.7,116.2c0.2,0,0.5,0,0.7,0c-14,0.4-25.3,11.9-25.3,26C3504,1222.6,3513,1233.1,3524.8,1235.5z"/>
		<path opacity="0.2" clip-path="url(#SVGID_2_)" fill="#EDF4F2" d="M1870.1,763.2h-134.4c-42.5,0-76.9-34.4-76.9-76.9
			c0-42.5,34.4-76.9,76.9-76.9h390.4c7.9-1.6,13.9-8.5,13.9-16.9c0-9.4-7.4-17-16.7-17.2c0.2,0,0.3,0,0.5,0h1.3
			c-42-0.6-75.9-34.8-75.9-76.9c0-42.5,34.5-76.9,76.9-76.9H2517c2.5-2.3,4.1-5.5,4.1-9.2c0-6.7-5.3-12.2-12-12.4c0.1,0,0.2,0,0.4,0
			h0.9c-30.1-0.4-54.5-24.9-54.5-55.2c0-30.5,24.7-55.2,55.2-55.2h182.5c30.5,0,55.2,24.7,55.2,55.2c0,30.5-24.7,55.2-55.2,55.2
			h-61.2c-6.6,0.3-11.8,5.7-11.8,12.3c0,3.7,1.6,6.9,4.1,9.2h209.8c42.5,0,76.9,34.5,76.9,76.9c0,42.5-34.4,76.9-76.9,76.9h-85.2
			c-9.1,0.4-16.4,7.9-16.4,17.2c0,8.4,6,15.3,13.9,16.9h413.8c42.5,0,76.9,34.4,76.9,76.9c0,42.5-34.5,76.9-76.9,76.9h-68.7
			c-7.8,1.6-13.8,8.5-13.8,16.8c0,9.4,7.4,17,16.7,17.2c-0.2,0-0.3,0-0.5,0h-1.3c42,0.6,75.9,34.8,75.9,76.9
			c0,42.5-34.4,76.9-76.9,76.9H1868.7c-42.5,0-76.9-34.5-76.9-76.9c0-42.1,33.9-76.3,75.9-76.9c-0.2,0-0.3,0-0.5,0
			c9.3-0.3,16.7-7.9,16.7-17.2C1883.9,771.8,1878,764.8,1870.1,763.2z"/>
	</g>
</g>
<g id="Crane">
	<g>
		<g>
			<g>
				<g>
					<path fill="#EDF1EA" d="M3027.5,828.8h19.1v296.2h-19.1V828.8z M3044.5,830.8h-15v292.1h15V830.8z"/>
				</g>
				<g>
					<rect x="3028.3" y="850.3" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="870.6" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="891" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="911.3" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="931.7" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="952" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3028.3" y="972.4" fill="#EDF1EA" width="17.4" height="2.1"/>
				</g>
				<g>
					<rect x="3029.2" y="992.7" fill="#EDF1EA" width="16.5" height="2.1"/>
				</g>
				<g>
					<rect x="3029.2" y="1013.1" fill="#EDF1EA" width="16.5" height="2.1"/>
				</g>
				<g>
					<rect x="3029.2" y="1033.4" fill="#EDF1EA" width="16.5" height="2.1"/>
				</g>
				<g>
					<rect x="3029.2" y="1053.4" fill="#EDF1EA" width="16.5" height="2.1"/>
				</g>
				<g>
					<rect x="3029.2" y="1093.4" fill="#EDF1EA" width="16.5" height="2.1"/>
				</g>
				<g>
					<polygon fill="#EDF1EA" points="3029.2,1075.5 3046.3,1075.5 3046.4,1073.6 3030.5,1054.6 3045.7,1035.7 3045.6,1032.3 
						3030.5,1014.1 3046.6,993.7 3030.1,973 3046,952.5 3030.2,932 3045.7,913.1 3045.6,910.4 3030.4,891.8 3046.4,871.5 
						3030.4,850.6 3046.1,831.1 3044.5,829.8 3027.7,850.6 3043.8,871.5 3027.7,891.8 3044.1,911.8 3027.6,932 3043.4,952.5 
						3027.4,973.1 3044,993.8 3027.9,1014.1 3044.4,1034.1 3027.9,1054.6 3043.5,1073.4 3029.2,1073.4 					"/>
				</g>
				<g>
					<polygon fill="#EDF1EA" points="3044.6,1124.4 3046.4,1123.4 3030.4,1094.7 3046.5,1075.1 3044.9,1073.8 3027.9,1094.5 					
						"/>
				</g>
			</g>
			<path fill="#EDF1EA" d="M3029,1113.2h16.1c7.1,0,12.9,5.8,12.9,12.9h-41.8C3016.1,1118.9,3021.9,1113.2,3029,1113.2z"/>
		</g>
		<path fill="#EDF1EA" d="M3027.6,813c0-5.2,4.2-9.4,9.4-9.4c5.2,0,9.4,4.2,9.4,9.4v13.1h-18.7V813z"/>
		<g>
			<polygon fill="#EDF1EA" points="2974.8,832.7 3188,830.5 3188,826 2974.8,823.9 			"/>
			<rect x="2935.2" y="816.5" fill="#EDF1EA" width="41.8" height="23.6"/>
			<path fill="#EDF1EA" d="M2992.6,831.3v30h21.7v-30H2992.6z M2995.8,838.4c0-1.1,0.9-2,2-2h11.3c1.1,0,2,0.9,2,2v5.3
				c0,1.1-0.9,2-2,2h-11.3c-1.1,0-2-0.9-2-2V838.4z"/>
		</g>
		<path fill="#EDF1EA" d="M3154.8,828.9v6.6c0,0.9,0.7,1.6,1.6,1.6c0.9,0,1.6-0.7,1.6-1.6v-6.6h1.4v6.6c0,1.7-1.4,3-3,3
			c-1.7,0-3-1.4-3-3v-6.6H3154.8z"/>
		<path fill="#EDF1EA" d="M3035.8,799.8c0-0.7,0.5-1.2,1.2-1.2l0,0c0.7,0,1.2,0.5,1.2,1.2v7c0,0.7-0.5,1.2-1.2,1.2l0,0
			c-0.7,0-1.2-0.5-1.2-1.2V799.8z"/>
		<g>
			
				<rect x="2942.3" y="807.9" transform="matrix(-0.9811 0.1937 -0.1937 -0.9811 6080.3374 1022.3459)" fill="#EDF1EA" width="95.7" height="1.1"/>
		</g>
		<g>
			<rect x="3155.8" y="837.9" fill="#EDF1EA" width="1.1" height="97.1"/>
		</g>
		<g>
			<path fill="#EDF1EA" d="M3156.4,932.4l22.8,39.5h-45.7L3156.4,932.4z M3156.4,934.5l-21,36.3h41.9L3156.4,934.5z"/>
		</g>
		<rect x="3119.4" y="970.4" fill="#EDF1EA" width="73.9" height="14.6"/>
	</g>
</g>
<g id="Buildings">
	<g>
		<polygon fill="#EDECDC" points="2181.4,686.4 2279.1,686.4 2279.1,596.8 2181.4,572.4 		"/>
		<polygon fill="#E3E5D9" points="2293.2,652.3 2167.3,612.6 2167.3,1136 2293.2,1136 		"/>
		<rect x="2174.2" y="698.7" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2198" y="698.7" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="698.7" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="698.7" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="698.7" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2174.2" y="726.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2198" y="726.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="726.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="726.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="726.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2174.2" y="753.9" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2198" y="753.9" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="753.9" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="753.9" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="753.9" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2174.2" y="781.5" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2198" y="781.5" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="781.5" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="781.5" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="781.5" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2174.2" y="809.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2198" y="809.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="809.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="809.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="809.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2174.2" y="836.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2198" y="836.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="836.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="836.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="836.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="864.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="864.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="864.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="891.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="891.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="891.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="919.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="919.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="919.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="947.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="947.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="947.1" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="974.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="974.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="974.7" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="1002.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="1002.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="1002.3" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="1029.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="1029.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="1029.9" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2221.9" y="1057.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2245.7" y="1057.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2269.5" y="1057.5" fill="#E4F3F4" width="16.8" height="16.8"/>
		<rect x="2221.9" y="1085.1" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2245.7" y="1085.1" fill="#F1F8F5" width="16.8" height="16.8"/>
		<rect x="2269.5" y="1085.1" fill="#F1F8F5" width="16.8" height="16.8"/>
	</g>
	<g>
		<g>
			<rect x="2490.8" y="871.7" fill="#FCFDFB" width="135.4" height="264.3"/>
			<path fill="#FCFDFB" d="M2558.5,907.6c-20.5,0-37.1-16.6-37.1-37.1c0-20.5,16.6-37.1,37.1-37.1c20.5,0,37.1,16.6,37.1,37.1
				C2595.6,891,2578.9,907.6,2558.5,907.6z"/>
			<path fill="#C9DCDC" d="M2583.8,870.5c0,14-11.3,25.3-25.3,25.3c-14,0-25.3-11.3-25.3-25.3c0-14,11.3-25.3,25.3-25.3
				C2572.4,845.2,2583.8,856.5,2583.8,870.5z"/>
			<polygon fill="#FCFDFB" points="2574,866.5 2563,866.5 2563,855.5 2554,855.5 2554,866.5 2543,866.5 2543,875.4 2554,875.4 
				2554,886.5 2563,886.5 2563,875.4 2574,875.4 			"/>
		</g>
		<g>
			<rect x="2497.8" y="904.2" fill="#DFE6D7" width="21.6" height="11.2"/>
			<rect x="2531.1" y="904.2" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2564.4" y="904.2" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2597.7" y="904.2" fill="#C8E0E6" width="21.6" height="11.2"/>
			<rect x="2497.8" y="921.3" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2531.1" y="921.3" fill="#C8E0E6" width="21.6" height="11.2"/>
			<rect x="2564.4" y="921.3" fill="#B8CACB" width="21.6" height="11.2"/>
			<rect x="2597.7" y="921.3" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2497.8" y="938.5" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2531.1" y="938.5" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2564.4" y="938.5" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2597.7" y="938.5" fill="#B8CACB" width="21.6" height="11.2"/>
			<rect x="2497.8" y="955.7" fill="#C8E0E6" width="21.6" height="11.2"/>
			<rect x="2531.1" y="955.7" fill="#DFE6D7" width="21.6" height="11.2"/>
			<rect x="2564.4" y="955.7" fill="#C8E0E6" width="21.6" height="11.2"/>
			<rect x="2597.7" y="955.7" fill="#DDE8E7" width="21.6" height="11.2"/>
			<rect x="2497.8" y="972.8" fill="#C5D0D0" width="21.6" height="11.2"/>
			<rect x="2531.1" y="972.8" fill="#B8CACB" width="21.6" height="11.2"/>
			<rect x="2564.4" y="972.8" fill="#C5D0D0" width="21.6" height="11.2"/>
			<rect x="2597.7" y="972.8" fill="#DDE8E7" width="21.6" height="11.2"/>
		</g>
		<rect x="2496.7" y="998.2" fill="#DDE8E7" width="123.6" height="4.3"/>
		<g>
			<rect x="2497" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
			<rect x="2519.1" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
			<rect x="2541.2" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
			<rect x="2563.2" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
			<rect x="2585.3" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
			<rect x="2607.4" y="1014.4" fill="#D0DADA" width="12.6" height="21.7"/>
		</g>
		<g>
			<rect x="2497" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
			<rect x="2519.1" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
			<rect x="2541.2" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
			<rect x="2563.2" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
			<rect x="2585.3" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
			<rect x="2607.4" y="1045" fill="#DDE8E7" width="12.6" height="21.7"/>
		</g>
		<g>
			<rect x="2497" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
			<rect x="2519.1" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
			<rect x="2541.2" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
			<rect x="2563.2" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
			<rect x="2585.3" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
			<rect x="2607.4" y="1075.5" fill="#BEDDE6" width="12.6" height="21.7"/>
		</g>
	</g>
	<g>
		<path fill="#FFFFFF" d="M2333,692.3h-23.2V1136h163.7V759.1C2418.6,754.3,2369.3,729.7,2333,692.3z"/>
		<g>
			<path fill="#B7D6DB" d="M2328.1,704.2h-6.5v90.8h26.3v-73.3C2341,716.2,2334.4,710.4,2328.1,704.2z"/>
			<path fill="#C2DADC" d="M2407.6,755.7v39.2h23.8v-31.4C2423.3,761.3,2415.3,758.7,2407.6,755.7z"/>
			<path fill="#B8D5CF" d="M2351.9,724.7v70.2h23.8v-54.5C2367.5,735.7,2359.6,730.4,2351.9,724.7z"/>
			<path fill="#ACD3DB" d="M2379.8,742.7v52.3h23.8v-40.9C2395.4,750.7,2387.5,746.9,2379.8,742.7z"/>
			<path fill="#B7D6DB" d="M2461.7,769.7c-8.9-1.2-17.7-2.9-26.3-5.1v30.3h26.3V769.7z"/>
		</g>
		<g>
			<rect x="2322.2" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2346.9" y="820.9" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2374.9" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2399.6" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="820.9" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2440" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="820.9" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="862.3" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2334.5" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2346.9" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2374.9" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="862.3" fill="#C1CDBB" width="8.8" height="26"/>
			<rect x="2399.6" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="862.3" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2346.9" y="903.7" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2374.9" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2399.6" y="903.7" fill="#C1CDBB" width="8.8" height="26"/>
			<rect x="2427.6" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="903.7" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2346.9" y="945.1" fill="#C1CDBB" width="8.8" height="26"/>
			<rect x="2374.9" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2399.6" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="945.1" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2346.9" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2374.9" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="986.5" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2399.6" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="986.5" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="1027.8" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2346.9" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2374.9" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2387.3" y="1027.8" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2399.6" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="1027.8" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2322.2" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2334.5" y="1069.2" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2346.9" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2374.9" y="1069.2" fill="#AFD2C8" width="8.8" height="26"/>
			<rect x="2387.3" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2399.6" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2427.6" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2440" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
			<rect x="2452.3" y="1069.2" fill="#D0DDC9" width="8.8" height="26"/>
		</g>
	</g>
	<g>
		<g>
			<path fill="#E0E8DC" d="M1627.3,940.3c-11.2,0-20.2,9.1-20.2,20.2v12.3v49v114h218.7V940.3H1627.3z"/>
			<g>
				<rect x="1740" y="952.2" fill="#74777E" width="37.7" height="31.1"/>
				<rect x="1781.7" y="952.2" fill="#94A4A1" width="32.2" height="31.1"/>
				<rect x="1698.4" y="1020" fill="#97C2B6" width="37.7" height="28.9"/>
				<rect x="1698.4" y="952.2" fill="#87B0C6" width="37.7" height="31.1"/>
				<rect x="1740" y="1020" fill="#87B0C6" width="37.7" height="28.9"/>
				<rect x="1656.7" y="952.2" fill="#94A4A1" width="37.7" height="31.1"/>
				<rect x="1656.7" y="1052.8" fill="#74777E" width="37.7" height="28.9"/>
				<path fill="#74777E" d="M1652.8,952.2h-25.5c-4.6,0-8.4,3.8-8.4,8.4v22.7h33.9V952.2z"/>
				<rect x="1740" y="1052.8" fill="#94A4A1" width="37.7" height="28.9"/>
				<rect x="1656.7" y="1020" fill="#87B0C6" width="37.7" height="28.9"/>
				<rect x="1698.4" y="1052.8" fill="#7BBAB6" width="37.7" height="28.9"/>
				<rect x="1698.4" y="987.2" fill="#74777E" width="37.7" height="28.9"/>
				<rect x="1656.7" y="987.2" fill="#87B0C6" width="37.7" height="28.9"/>
				<rect x="1781.7" y="987.2" fill="#97C2B6" width="32.2" height="28.9"/>
				<rect x="1618.9" y="1052.8" fill="#87B0C6" width="33.9" height="28.9"/>
				<rect x="1740" y="987.2" fill="#7BBAB6" width="37.7" height="28.9"/>
				<rect x="1781.7" y="1052.8" fill="#74777E" width="32.2" height="28.9"/>
				<rect x="1618.9" y="987.2" fill="#7BBAB6" width="33.9" height="28.9"/>
				<rect x="1781.7" y="1020" fill="#94A4A1" width="32.2" height="28.9"/>
				<rect x="1618.9" y="1020" fill="#94A4A1" width="33.9" height="28.9"/>
			</g>
		</g>
		<g>
			<g>
				<polygon fill="#F6F5EE" points="1673.2,870.1 1663.7,906.4 1809.4,906.4 1818,870.1 				"/>
				<polygon fill="#99B7C0" points="1684.3,903.4 1667.9,903.4 1675.7,873.1 1692.1,873.1 				"/>
				<polygon fill="#CEDEDD" points="1704.6,903.4 1688.2,903.4 1696,873.1 1712.4,873.1 				"/>
				<polygon fill="#B7D4CC" points="1724.9,903.4 1708.5,903.4 1716.3,873.1 1732.7,873.1 				"/>
				<polygon fill="#B7D6DB" points="1745.2,903.4 1728.8,903.4 1736.6,873.1 1753,873.1 				"/>
				<polygon fill="#CEDEDD" points="1765.9,903.4 1749.5,903.4 1757.2,873.1 1773.7,873.1 				"/>
				<polygon fill="#B5D3C5" points="1786.2,903.4 1769.8,903.4 1777.5,873.1 1794,873.1 				"/>
				<polygon fill="#B7D6DB" points="1806.5,903.4 1790.1,903.4 1797.8,873.1 1814.3,873.1 				"/>
			</g>
			<g>
				<polygon fill="#F6F5EE" points="1646.2,896.4 1636.7,932.7 1803.1,932.7 1811.7,896.4 				"/>
				<polygon fill="#99B7C0" points="1657.3,929.7 1640.9,929.7 1648.6,899.5 1665.1,899.5 				"/>
				<polygon fill="#CEDEDD" points="1677.6,929.7 1661.2,929.7 1668.9,899.5 1685.4,899.5 				"/>
				<polygon fill="#B7D4CC" points="1697.9,929.7 1681.5,929.7 1689.2,899.5 1705.7,899.5 				"/>
				<polygon fill="#B7D6DB" points="1718.2,929.7 1701.8,929.7 1709.5,899.5 1726,899.5 				"/>
				<polygon fill="#CEDEDD" points="1738.8,929.7 1722.4,929.7 1730.2,899.5 1746.6,899.5 				"/>
				<polygon fill="#B5D3C5" points="1759.2,929.7 1742.7,929.7 1750.5,899.5 1766.9,899.5 				"/>
				<polygon fill="#B7D6DB" points="1779.5,929.7 1763,929.7 1770.8,899.5 1787.2,899.5 				"/>
				<polygon fill="#99B7C0" points="1799.8,929.7 1783.3,929.7 1791.1,899.5 1807.5,899.5 				"/>
			</g>
		</g>
	</g>
	<g>
		<g>
			<rect x="2642" y="714.6" fill="#FFFFFF" width="165.6" height="418.4"/>
			<g>
				<rect x="2647.4" y="719.6" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="719.6" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2710.9" y="719.6" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2742.7" y="719.6" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2774.4" y="719.6" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2647.4" y="760.7" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="760.7" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2710.9" y="760.7" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2742.7" y="760.7" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2774.4" y="760.7" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2647.4" y="801.9" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2679.1" y="801.9" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2710.9" y="801.9" fill="#90B2C7" width="27.8" height="37.4"/>
				<rect x="2742.7" y="801.9" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2774.4" y="801.9" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2647.4" y="843" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="843" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2710.9" y="843" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2742.7" y="843" fill="#90B2C7" width="27.8" height="37.4"/>
				<rect x="2774.4" y="843" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2647.4" y="884.2" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2679.1" y="884.2" fill="#90B2C7" width="27.8" height="37.4"/>
				<rect x="2710.9" y="884.2" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2742.7" y="884.2" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2774.4" y="884.2" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2647.4" y="925.3" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="925.3" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2710.9" y="925.3" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2742.7" y="925.3" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2774.4" y="925.3" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2647.4" y="966.4" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2679.1" y="966.4" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2710.9" y="966.4" fill="#90B2C7" width="27.8" height="37.4"/>
				<rect x="2742.7" y="966.4" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2774.4" y="966.4" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2647.4" y="1007.6" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="1007.6" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2710.9" y="1007.6" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2742.7" y="1007.6" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2774.4" y="1007.6" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2647.4" y="1048.7" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2679.1" y="1048.7" fill="#97C2B6" width="27.8" height="37.4"/>
				<rect x="2710.9" y="1048.7" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2742.7" y="1048.7" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2774.4" y="1048.7" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2647.4" y="1089.8" fill="#94A4A1" width="27.8" height="37.4"/>
				<rect x="2679.1" y="1089.8" fill="#87B0C6" width="27.8" height="37.4"/>
				<rect x="2710.9" y="1089.8" fill="#68AAC1" width="27.8" height="37.4"/>
				<rect x="2742.7" y="1089.8" fill="#74777E" width="27.8" height="37.4"/>
				<rect x="2774.4" y="1089.8" fill="#94A4A1" width="27.8" height="37.4"/>
			</g>
		</g>
	</g>
	<g>
		<rect x="2145.1" y="865.9" fill="#F6F5EE" width="62.7" height="270.1"/>
		<path fill="#D0DDC9" d="M2066.1,829.6c-10.3,0-18.6,8.3-18.6,18.6v2.8V1136h108.9V829.6H2066.1z"/>
		<g>
			<rect x="2131.1" y="959.6" opacity="0.6" fill="#EFEDE6" width="13.5" height="32.2"/>
			<rect x="2131.1" y="919.9" fill="#EFEDE6" width="13.5" height="32.2"/>
			<rect x="2095.8" y="841.4" opacity="0.6" fill="#EFEDE6" width="13.7" height="31.1"/>
			<rect x="2131.1" y="880.1" fill="#EFEDE6" width="13.5" height="32.2"/>
			<rect x="2113.5" y="841.4" opacity="0.6" fill="#EFEDE6" width="13.7" height="31.1"/>
			<rect x="2095.8" y="959.6" opacity="0.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2113.5" y="959.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2113.5" y="919.9" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2113.5" y="880.1" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2095.8" y="919.9" opacity="0.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2078.1" y="919.9" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2078.1" y="959.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2078.1" y="880.1" opacity="0.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2095.8" y="880.1" opacity="0.6" fill="#EFEDE6" width="13.7" height="32.2"/>
			<rect x="2131.1" y="841.4" fill="#EFEDE6" width="13.5" height="31.1"/>
			<path fill="#EFEDE6" d="M2074.1,841.4h-8c-3.7,0-6.8,3.3-6.8,7.3v23.9h14.8V841.4z"/>
			<rect x="2059.3" y="959.6" fill="#EFEDE6" width="14.8" height="32.2"/>
			<rect x="2059.3" y="919.9" opacity="0.6" fill="#EFEDE6" width="14.8" height="32.2"/>
			<rect x="2059.3" y="880.1" fill="#EFEDE6" width="14.8" height="32.2"/>
			<rect x="2078.1" y="841.4" fill="#EFEDE6" width="13.7" height="31.1"/>
		</g>
		<g>
			<rect x="2170" y="889.5" fill="#CEDEDD" width="37.8" height="9.5"/>
			<rect x="2170" y="908.4" fill="#CEDEDD" width="37.8" height="9.5"/>
			<rect x="2170" y="927.3" fill="#CEDEDD" width="37.8" height="9.5"/>
			<rect x="2170" y="946.3" fill="#CEDEDD" width="37.8" height="9.5"/>
		</g>
	</g>
	<g>
		<g>
			<path fill="#96B738" d="M2010.8,996.3c0-7.7-6.2-14-14-14c-7.7,0-14,6.3-14,14v5.7h27.9V996.3z"/>
			<path fill="#8EAE35" d="M2010.8,996.3c0-7.7-6.2-14-14-14v19.6h14V996.3z"/>
		</g>
		<g>
			<path fill="#96B738" d="M2038.7,996.3c0-7.7-6.2-14-14-14c-7.7,0-14,6.3-14,14v5.7h27.9V996.3z"/>
			<path fill="#8EAE35" d="M2038.7,996.3c0-7.7-6.2-14-14-14v19.6h14V996.3z"/>
		</g>
		<g>
			<path fill="#96B738" d="M2066.6,996.3c0-7.7-6.2-14-14-14c-7.7,0-14,6.3-14,14v5.7h27.9V996.3z"/>
			<path fill="#8EAE35" d="M2066.6,996.3c0-7.7-6.2-14-14-14v19.6h14V996.3z"/>
		</g>
		<g>
			<path fill="#96B738" d="M2094.5,996.3c0-7.7-6.2-14-14-14c-7.7,0-14,6.3-14,14v5.7h27.9V996.3z"/>
			<path fill="#8EAE35" d="M2094.5,996.3c0-7.7-6.2-14-14-14v19.6h14V996.3z"/>
		</g>
		<rect x="1976.1" y="1001.1" fill="#E6E8E1" width="123" height="134.8"/>
		<g>
			<rect x="1988.9" y="1016" fill="#97C2B6" width="97.5" height="9"/>
			<rect x="1988.9" y="1037.8" fill="#97C2B6" width="97.5" height="9"/>
			<rect x="1988.9" y="1059.6" fill="#97C2B6" width="97.5" height="9"/>
			<rect x="1988.9" y="1081.4" fill="#97C2B6" width="97.5" height="9"/>
			<rect x="1988.9" y="1103.3" fill="#97C2B6" width="97.5" height="9"/>
		</g>
	</g>
	<g>
		<g>
			<rect x="1843.2" y="890" fill="#F6F5EE" width="134.8" height="246"/>
			<g>
				<g>
					<rect x="1857.4" y="904.8" fill="#9BC3B7" width="19" height="19"/>
					<rect x="1882.1" y="904.8" fill="#9BC3B7" width="19" height="19"/>
				</g>
				<g>
					<rect x="1857.4" y="927.7" fill="#9BC3B7" width="19" height="19"/>
					<rect x="1882.1" y="927.7" fill="#9BC3B7" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1920" y="904.8" fill="#CEDEDD" width="19" height="19"/>
					<rect x="1944.7" y="904.8" fill="#CEDEDD" width="19" height="19"/>
				</g>
				<g>
					<rect x="1920" y="927.7" fill="#CEDEDD" width="19" height="19"/>
					<rect x="1944.7" y="927.7" fill="#CEDEDD" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1857.4" y="959.8" fill="#94A4A1" width="19" height="19"/>
					<rect x="1882.1" y="959.8" fill="#94A4A1" width="19" height="19"/>
				</g>
				<g>
					<rect x="1857.4" y="982.6" fill="#94A4A1" width="19" height="19"/>
					<rect x="1882.1" y="982.6" fill="#94A4A1" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1920" y="959.8" fill="#90B2C7" width="19" height="19"/>
					<rect x="1944.7" y="959.8" fill="#90B2C7" width="19" height="19"/>
				</g>
				<g>
					<rect x="1920" y="982.6" fill="#90B2C7" width="19" height="19"/>
					<rect x="1944.7" y="982.6" fill="#90B2C7" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1857.4" y="1016" fill="#9BC3B7" width="19" height="19"/>
					<rect x="1882.1" y="1016" fill="#9BC3B7" width="19" height="19"/>
				</g>
				<g>
					<rect x="1857.4" y="1038.8" fill="#9BC3B7" width="19" height="19"/>
					<rect x="1882.1" y="1038.8" fill="#9BC3B7" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1920" y="1016" fill="#CEDEDD" width="19" height="19"/>
					<rect x="1944.7" y="1016" fill="#CEDEDD" width="19" height="19"/>
				</g>
				<g>
					<rect x="1920" y="1038.8" fill="#CEDEDD" width="19" height="19"/>
					<rect x="1944.7" y="1038.8" fill="#CEDEDD" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1857.4" y="1071" fill="#94A4A1" width="19" height="19"/>
					<rect x="1882.1" y="1071" fill="#94A4A1" width="19" height="19"/>
				</g>
				<g>
					<rect x="1857.4" y="1093.8" fill="#94A4A1" width="19" height="19"/>
					<rect x="1882.1" y="1093.8" fill="#94A4A1" width="19" height="19"/>
				</g>
			</g>
			<g>
				<g>
					<rect x="1920" y="1071" fill="#90B2C7" width="19" height="19"/>
					<rect x="1944.7" y="1071" fill="#90B2C7" width="19" height="19"/>
				</g>
				<g>
					<rect x="1920" y="1093.8" fill="#90B2C7" width="19" height="19"/>
					<rect x="1944.7" y="1093.8" fill="#90B2C7" width="19" height="19"/>
				</g>
			</g>
		</g>
		<g>
			<polygon fill="#F6F5EE" points="1956.3,883.2 1863.7,883.2 1874,843.6 1965.7,843.6 			"/>
			<g>
				<polygon fill="#CEDEDD" points="1886.2,879.9 1868.3,879.9 1876.7,846.9 1894.7,846.9 				"/>
				<polygon fill="#CEDEDD" points="1908.3,879.9 1890.4,879.9 1898.9,846.9 1916.8,846.9 				"/>
				<polygon fill="#BAD7DC" points="1930.5,879.9 1912.6,879.9 1921.1,846.9 1939,846.9 				"/>
				<polygon fill="#B0BEBE" points="1952.7,879.9 1934.7,879.9 1943.2,846.9 1961.1,846.9 				"/>
			</g>
		</g>
	</g>
	<g>
		<polygon fill="#F6F5EE" points="2143.2,823.1 2072.4,823.1 2082.7,783.5 2152.6,783.5 		"/>
		<g>
			<polygon fill="#ACD3DB" points="2094.9,819.8 2077,819.8 2085.5,786.8 2103.4,786.8 			"/>
			<polygon fill="#CEDEDD" points="2117,819.8 2099.1,819.8 2107.6,786.8 2125.5,786.8 			"/>
			<polygon fill="#ACD3DB" points="2139.2,819.8 2121.3,819.8 2129.8,786.8 2147.7,786.8 			"/>
		</g>
	</g>
	<g>
		<path fill="#E6ECE1" d="M2920.4,930.3c0,1.2-1,2.2-2.2,2.2l0,0c-1.2,0-2.2-1-2.2-2.2v-37.2c0-1.2,1-2.2,2.2-2.2l0,0
			c1.2,0,2.2,1,2.2,2.2V930.3z"/>
		<g>
			<path fill="#D8E2D2" d="M2918.4,873.5c0,0-0.1,0-0.1,0c-2.8,0-5.3,1.2-7.1,3.1l1.7,1.7c1.4-1.5,3.3-2.4,5.5-2.4c0,0,0.1,0,0.1,0
				c2.2,0,4.2,0.9,5.6,2.4l1.7-1.7C2923.8,874.7,2921.2,873.5,2918.4,873.5z"/>
			<path fill="#D8E2D2" d="M2918.4,866.4c0,0-0.1,0-0.1,0c-4.8,0-9.1,2-12.2,5.2l1.7,1.7c2.7-2.8,6.4-4.5,10.5-4.5c0,0,0.1,0,0.1,0
				c4.2,0,7.9,1.7,10.6,4.5l1.7-1.7C2927.5,868.4,2923.2,866.4,2918.4,866.4z"/>
			<path fill="#D8E2D2" d="M2918.4,879.5c0,0-0.1,0-0.1,0c-2.2,0.1-3.9,1.8-3.9,4c0,2.2,1.7,3.9,3.9,4c0,0,0.1,0,0.1,0
				c2.2,0,4-1.8,4-4C2922.4,881.3,2920.6,879.5,2918.4,879.5z"/>
			<path fill="#D8E2D2" d="M2918.4,859.3c0,0-0.1,0-0.1,0c-6.7,0-12.8,2.8-17.2,7.3l1.7,1.7c3.9-4,9.4-6.6,15.5-6.6c0,0,0.1,0,0.1,0
				c6.1,0,11.6,2.5,15.6,6.6l1.7-1.7C2931.3,862.1,2925.1,859.3,2918.4,859.3z"/>
		</g>
		<path fill="#E6ECE1" d="M2928.1,922c0-5.5-4.4-9.9-9.9-9.9c-5.5,0-9.9,4.4-9.9,9.9v12.8h19.8V922z"/>
		<path fill="#D8E2D2" d="M2918.2,929.8c-26.2,0-47.5,21.3-47.5,47.5h95C2965.7,951.1,2944.4,929.8,2918.2,929.8z"/>
		<rect x="2840.3" y="974.1" fill="#D0DDC9" width="155.8" height="50.6"/>
		<rect x="2825.1" y="1021.5" fill="#EAEEED" width="186.1" height="114.4"/>
		<g>
			<path fill="#D0DDC9" d="M2847.5,1047.7c0-2.9-2.3-5.2-5.2-5.2s-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2872.8,1047.7c0-2.9-2.3-5.2-5.2-5.2c-2.9,0-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2898.1,1047.7c0-2.9-2.3-5.2-5.2-5.2c-2.9,0-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2923.4,1047.7c0-2.9-2.3-5.2-5.2-5.2c-2.9,0-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2948.7,1047.7c0-2.9-2.3-5.2-5.2-5.2s-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2974,1047.7c0-2.9-2.3-5.2-5.2-5.2c-2.9,0-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
			<path fill="#D0DDC9" d="M2999.3,1047.7c0-2.9-2.3-5.2-5.2-5.2c-2.9,0-5.2,2.3-5.2,5.2v82.7h10.4V1047.7z"/>
		</g>
		<g>
			<path fill="#EAEEED" d="M2860.6,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2881.2,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2901.8,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2922.4,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2943,990.3c0-2.3-1.9-4.2-4.2-4.2s-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2963.6,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
			<path fill="#EAEEED" d="M2984.2,990.3c0-2.3-1.9-4.2-4.2-4.2c-2.3,0-4.2,1.9-4.2,4.2v26.4h8.5V990.3z"/>
		</g>
	</g>
</g>
<g id="Green_x5F_Elements_x5F_Upper_x5F_Part">
	<g>
		<path fill="#B9D432" d="M1733,1107.2c-0.1-9.3-7.7-16.8-17-16.8c-9.3,0-16.9,7.5-17,16.8c-9.6,0.9-17,9-17,18.8h68.1
			C1750.1,1116.3,1742.6,1108.2,1733,1107.2z"/>
		<path fill="#A8CE38" d="M1733,1107.2c-0.1-9.3-7.7-16.8-17-16.8v35.7h34.1C1750.1,1116.3,1742.6,1108.2,1733,1107.2z"/>
	</g>
	<g>
		<polygon fill="#5A575D" points="2811.7,1126.1 2803.7,1126.1 2805,1098.3 2810.5,1098.3 		"/>
		<g>
			<circle fill="#96B738" cx="2807.7" cy="1076.1" r="26.3"/>
			<path fill="#8EAE35" d="M2807.7,1049.8v52.6c14.5,0,26.3-11.8,26.3-26.3C2834,1061.6,2822.3,1049.8,2807.7,1049.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2212,1126.1 2204.8,1126.1 2205.9,1101 2210.9,1101 		"/>
		<g>
			<path fill="#95B52E" d="M2222.1,1087.6h-1.5c2.1-0.8,3.5-2.9,3.5-5.2c0-3.1-2.5-5.6-5.6-5.6h-2.8c1.5-0.4,2.6-1.8,2.6-3.4
				c0-2-1.6-3.5-3.5-3.5h-12.7c-2,0-3.5,1.6-3.5,3.5c0,1.6,1.1,3,2.6,3.4h-2.8c-3.1,0-5.6,2.5-5.6,5.6c0,2.4,1.4,4.4,3.5,5.2h-1.5
				c-4.2,0-7.6,3.4-7.6,7.6s3.4,7.6,7.6,7.6h27.4c4.2,0,7.6-3.4,7.6-7.6S2226.3,1087.6,2222.1,1087.6z"/>
			<path fill="#89A82A" d="M2222.1,1087.6h-1.5c2.1-0.8,3.5-2.9,3.5-5.2c0-3.1-2.5-5.6-5.6-5.6h-2.8c1.5-0.4,2.6-1.8,2.6-3.4
				c0-2-1.6-3.5-3.5-3.5h-6.4v33h13.7c4.2,0,7.6-3.4,7.6-7.6S2226.3,1087.6,2222.1,1087.6z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2160.7,1126.1 2152,1126.1 2153.4,1095.9 2159.4,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M2185.6,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7h-27.7
				c-4.3,0-7.7,3.5-7.7,7.7c0,3.2,2,6,4.8,7.1h-2c-5.8,0-10.5,4.7-10.5,10.5c0,5.8,4.7,10.5,10.5,10.5h2.3c-3,1.1-5.1,3.9-5.1,7.3
				c0,4.3,3.5,7.7,7.7,7.7h27.7c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C2180.9,1088.1,2185.6,1083.5,2185.6,1077.7z"/>
			<path fill="#A3B120" d="M2185.6,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7h-13.8v50.7
				h13.8c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C2180.9,1088.1,2185.6,1083.5,2185.6,1077.7z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="1781.9,1126.1 1773.2,1126.1 1774.6,1095.9 1780.6,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M1812.2,1073c0-6.8-5.6-12.4-12.4-12.4h-2.4c3.3-1.4,5.7-4.6,5.7-8.5c0-5-4.1-9.1-9.1-9.1h-32.8
				c-5,0-9.1,4.1-9.1,9.1c0,3.8,2.4,7.1,5.7,8.5h-2.4c-6.8,0-12.4,5.6-12.4,12.4s5.5,12.4,12.4,12.4h2.8c-3.5,1.3-6.1,4.6-6.1,8.6
				c0,5,4.1,9.1,9.1,9.1h32.8c5,0,9.1-4.1,9.1-9.1c0-4-2.5-7.3-6.1-8.6h2.8C1806.6,1085.4,1812.2,1079.8,1812.2,1073z"/>
			<path fill="#A3B120" d="M1812.2,1073c0-6.8-5.6-12.4-12.4-12.4h-2.4c3.3-1.4,5.7-4.6,5.7-8.5c0-5-4.1-9.1-9.1-9.1h-16.4v60.1
				h16.4c5,0,9.1-4.1,9.1-9.1c0-4-2.5-7.3-6.1-8.6h2.8C1806.6,1085.4,1812.2,1079.8,1812.2,1073z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="1450.5,1126.1 1441.8,1126.1 1443.1,1095.9 1449.1,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M1475.3,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7h-27.7
				c-4.3,0-7.7,3.5-7.7,7.7c0,3.2,2,6,4.8,7.1h-2c-5.8,0-10.5,4.7-10.5,10.5c0,5.8,4.7,10.5,10.5,10.5h2.3c-3,1.1-5.1,3.9-5.1,7.3
				c0,4.3,3.5,7.7,7.7,7.7h27.7c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C1470.7,1088.1,1475.3,1083.5,1475.3,1077.7z"/>
			<path fill="#A3B120" d="M1475.3,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7h-13.8v50.7
				h13.8c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C1470.7,1088.1,1475.3,1083.5,1475.3,1077.7z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2747.2,1126.1 2738.5,1126.1 2739.9,1095.9 2745.9,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M2772.1,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7H2729
				c-4.3,0-7.7,3.5-7.7,7.7c0,3.2,2,6,4.8,7.1h-2c-5.8,0-10.5,4.7-10.5,10.5c0,5.8,4.7,10.5,10.5,10.5h2.3c-3,1.1-5.1,3.9-5.1,7.3
				c0,4.3,3.5,7.7,7.7,7.7h27.7c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C2767.4,1088.1,2772.1,1083.5,2772.1,1077.7z"/>
			<path fill="#A3B120" d="M2772.1,1077.7c0-5.8-4.7-10.5-10.5-10.5h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.3-3.5-7.7-7.7-7.7h-13.8v50.7
				h13.8c4.3,0,7.7-3.5,7.7-7.7c0-3.3-2.1-6.2-5.1-7.3h2.3C2767.4,1088.1,2772.1,1083.5,2772.1,1077.7z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1547.7,1126.1 1540.5,1126.1 1541.6,1101 1546.6,1101 		"/>
		<g>
			<circle fill="#6B9F33" cx="1544.1" cy="1081" r="23.8"/>
			<path fill="#61922E" d="M1544.1,1057.2v47.5c13.1,0,23.8-10.6,23.8-23.8C1567.9,1067.8,1557.2,1057.2,1544.1,1057.2z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1941.5,1126.1 1934.3,1126.1 1935.4,1101 1940.4,1101 		"/>
		<g>
			<circle fill="#6B9F33" cx="1937.9" cy="1081" r="23.8"/>
			<path fill="#61922E" d="M1937.9,1057.2v47.5c13.1,0,23.8-10.6,23.8-23.8C1961.7,1067.8,1951,1057.2,1937.9,1057.2z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="2023.8,1126.1 2015.9,1126.1 2017.2,1098.8 2022.6,1098.8 		"/>
		<g>
			<circle fill="#96B738" cx="2019.9" cy="1077" r="25.8"/>
			<path fill="#8EAE35" d="M2019.9,1051.2v51.7c14.3,0,25.8-11.6,25.8-25.8C2045.7,1062.8,2034.1,1051.2,2019.9,1051.2z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1983,1126.1 1973.6,1126.1 1975.1,1093.5 1981.5,1093.5 		"/>
		<g>
			<circle fill="#96B738" cx="1978.3" cy="1067.4" r="30.9"/>
			<path fill="#8EAE35" d="M1978.3,1036.5v61.8c17.1,0,30.9-13.8,30.9-30.9C2009.2,1050.3,1995.4,1036.5,1978.3,1036.5z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="2450.4,1125.5 2440.9,1125.5 2442.4,1092.9 2448.9,1092.9 		"/>
		<g>
			<circle fill="#96B738" cx="2445.7" cy="1066.8" r="30.9"/>
			<path fill="#8EAE35" d="M2445.7,1035.9v61.8c17.1,0,30.9-13.8,30.9-30.9C2476.6,1049.7,2462.7,1035.9,2445.7,1035.9z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="2918.3,1126.1 2908.9,1126.1 2910.4,1093.5 2916.8,1093.5 		"/>
		<g>
			<circle fill="#96B738" cx="2913.6" cy="1067.4" r="30.9"/>
			<path fill="#8EAE35" d="M2913.6,1036.5v61.8c17.1,0,30.9-13.8,30.9-30.9C2944.5,1050.3,2930.7,1036.5,2913.6,1036.5z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1611.5,1126.1 1602.1,1126.1 1603.6,1093.5 1610.1,1093.5 		"/>
		<g>
			<circle fill="#96B738" cx="1606.8" cy="1067.4" r="30.9"/>
			<path fill="#8EAE35" d="M1606.8,1036.5v61.8c17.1,0,30.9-13.8,30.9-30.9C1637.8,1050.3,1623.9,1036.5,1606.8,1036.5z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2993.6,1126.1 2984.9,1126.1 2986.2,1095.9 2992.2,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M3013,1082.4c0-4.7-3.8-8.5-8.5-8.5h-1.6c2.3-0.9,3.9-3.2,3.9-5.8c0-3.5-2.8-6.3-6.3-6.3H2978
				c-3.5,0-6.3,2.8-6.3,6.3c0,2.6,1.6,4.9,3.9,5.8h-1.6c-4.7,0-8.5,3.8-8.5,8.5c0,4.7,3.8,8.5,8.5,8.5h1.9c-2.4,0.9-4.2,3.2-4.2,5.9
				c0,3.5,2.8,6.3,6.3,6.3h22.5c3.5,0,6.3-2.8,6.3-6.3c0-2.7-1.7-5-4.2-5.9h1.9C3009.2,1090.9,3013,1087.1,3013,1082.4z"/>
			<path fill="#A3B120" d="M3013,1082.4c0-4.7-3.8-8.5-8.5-8.5h-1.6c2.3-0.9,3.9-3.2,3.9-5.8c0-3.5-2.8-6.3-6.3-6.3h-11.2v41.2h11.2
				c3.5,0,6.3-2.8,6.3-6.3c0-2.7-1.7-5-4.2-5.9h1.9C3009.2,1090.9,3013,1087.1,3013,1082.4z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2276.2,1126.1 2267.5,1126.1 2268.8,1095.9 2274.8,1095.9 		"/>
		<g>
			<path fill="#ADBB23" d="M2296.4,1074.6c0-4.9-3.9-8.8-8.8-8.8h-1.7c2.4-1,4-3.3,4-6c0-3.6-2.9-6.5-6.5-6.5h-23.3
				c-3.6,0-6.5,2.9-6.5,6.5c0,2.7,1.7,5,4,6h-1.7c-4.9,0-8.8,3.9-8.8,8.8c0,4.9,3.9,8.8,8.8,8.8h2c-2.5,0.9-4.3,3.3-4.3,6.1
				c0,3.6,2.9,6.5,6.5,6.5h23.3c3.6,0,6.5-2.9,6.5-6.5c0-2.8-1.8-5.2-4.3-6.1h2C2292.4,1083.4,2296.4,1079.5,2296.4,1074.6z"/>
			<path fill="#A3B120" d="M2296.4,1074.6c0-4.9-3.9-8.8-8.8-8.8h-1.7c2.4-1,4-3.3,4-6c0-3.6-2.9-6.5-6.5-6.5h-11.6v42.7h11.6
				c3.6,0,6.5-2.9,6.5-6.5c0-2.8-1.8-5.2-4.3-6.1h2C2292.4,1083.4,2296.4,1079.5,2296.4,1074.6z"/>
		</g>
	</g>
	<g>
		<path fill="#728E29" d="M2603.2,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7c-9.8,0-17.8,7.9-17.9,17.7c-10.1,1-17.9,9.5-17.9,19.8h71.7
			C2621.2,1115.7,2613.3,1107.2,2603.2,1106.2z"/>
		<path fill="#698426" d="M2603.2,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7v37.6h35.9C2621.2,1115.7,2613.3,1107.2,2603.2,1106.2z"/>
	</g>
	<g>
		<path fill="#728E29" d="M2306.8,1110.5c-0.1-7.4-6.1-13.4-13.5-13.4c-7.4,0-13.5,6-13.5,13.4c-7.6,0.8-13.5,7.2-13.5,15h54.1
			C2320.4,1117.7,2314.4,1111.3,2306.8,1110.5z"/>
		<path fill="#698426" d="M2306.8,1110.5c-0.1-7.4-6.1-13.4-13.5-13.4v28.4h27.1C2320.4,1117.7,2314.4,1111.3,2306.8,1110.5z"/>
	</g>
	<g>
		<path fill="#96B738" d="M1507,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7s-17.8,7.9-17.9,17.7c-10.1,1-17.9,9.5-17.9,19.8h71.7
			C1525,1115.7,1517.1,1107.2,1507,1106.2z"/>
		<path fill="#8EAE35" d="M1507,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7v37.6h35.9C1525,1115.7,1517.1,1107.2,1507,1106.2z"/>
	</g>
	<g>
		<path fill="#96B738" d="M1861.2,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7s-17.8,7.9-17.9,17.7c-10.1,1-17.9,9.5-17.9,19.8h71.7
			C1879.1,1115.7,1871.2,1107.2,1861.2,1106.2z"/>
		<path fill="#8EAE35" d="M1861.2,1106.2c-0.1-9.8-8.1-17.7-17.9-17.7v37.6h35.9C1879.1,1115.7,1871.2,1107.2,1861.2,1106.2z"/>
	</g>
	<g>
		<path fill="#96B738" d="M2347.3,1112.4c-0.1-6.6-5.4-11.9-12.1-11.9c-6.6,0-12,5.3-12.1,11.9c-6.8,0.7-12.1,6.4-12.1,13.3h48.3
			C2359.3,1118.8,2354,1113.1,2347.3,1112.4z"/>
		<path fill="#8EAE35" d="M2347.3,1112.4c-0.1-6.6-5.4-11.9-12.1-11.9v25.3h24.1C2359.3,1118.8,2354,1113.1,2347.3,1112.4z"/>
	</g>
	<g>
		<path fill="#96B738" d="M2863.8,1108.9c-0.1-8.5-7-15.4-15.6-15.4c-8.5,0-15.5,6.9-15.6,15.4c-8.7,0.9-15.6,8.2-15.6,17.2h62.3
			C2879.4,1117.1,2872.5,1109.7,2863.8,1108.9z"/>
		<path fill="#8EAE35" d="M2863.8,1108.9c-0.1-8.5-7-15.4-15.6-15.4v32.6h31.1C2879.4,1117.1,2872.5,1109.7,2863.8,1108.9z"/>
	</g>
	<g>
		<path fill="#B9D432" d="M2694,1109.2c-0.1-8.3-6.9-15.1-15.2-15.1c-8.4,0-15.2,6.7-15.2,15.1c-8.6,0.8-15.3,8.1-15.3,16.9h61
			C2709.2,1117.3,2702.5,1110.1,2694,1109.2z"/>
		<path fill="#A8CE38" d="M2694,1109.2c-0.1-8.3-6.9-15.1-15.2-15.1v32h30.5C2709.2,1117.3,2702.5,1110.1,2694,1109.2z"/>
	</g>
	<g>
		<path fill="#B9D432" d="M2541.4,1109.2c-0.1-8.3-6.9-15.1-15.2-15.1c-8.4,0-15.2,6.7-15.2,15.1c-8.6,0.8-15.3,8.1-15.3,16.9h61
			C2556.7,1117.3,2550,1110.1,2541.4,1109.2z"/>
		<path fill="#A8CE38" d="M2541.4,1109.2c-0.1-8.3-6.9-15.1-15.2-15.1v32h30.5C2556.7,1117.3,2550,1110.1,2541.4,1109.2z"/>
	</g>
	<g>
		<polygon fill="#5A575D" points="2477.1,1125.3 2469.9,1125.3 2471.1,1100.3 2476,1100.3 		"/>
		<g>
			<circle fill="#6B9F33" cx="2473.5" cy="1085.5" r="18.5"/>
			<path fill="#61922E" d="M2473.5,1067v37c10.2,0,18.5-8.3,18.5-18.5C2492,1075.3,2483.7,1067,2473.5,1067z"/>
		</g>
	</g>
</g>
<g id="Ground">
	<path fill="#8EB53A" d="M2082.3,1542.9c-30.9,0-55.9-25-55.9-55.9c0-27.9,20.5-51.2,47.3-55.3h-393.3c-30.9,0-55.9,25.1-55.9,56
		c0,30.9,25,55.9,55.9,55.9L2082.3,1542.9z"/>
	<path fill="#8EB53A" d="M2903.4,1431.7h-440.8c26.8,4.2,47.3,27.4,47.3,55.3c0,30.9-25,55.9-55.9,55.9l449.4,0.7
		c30.9,0,55.9-25,55.9-55.9C2959.3,1456.9,2934.3,1431.7,2903.4,1431.7z"/>
	<path fill="#8FAA29" d="M3000.8,1236.8h-666.4c30.7,0,55.7,24.9,55.7,55.7c0,30.7-24.9,55.9-55.7,55.9h666.4
		c30.8,0,55.7-25.1,55.7-55.9C3056.5,1261.8,3031.5,1236.8,3000.8,1236.8z"/>
	<path fill="#7AA32F" d="M2719.3,1236.7H1474.6c30.8,0,55.7,25,55.7,55.7s-24.9,55.9-55.7,55.9h1244.7c30.8,0,55.7-25.1,55.7-55.9
		S2750.1,1236.7,2719.3,1236.7z"/>
	<path fill="#6B9F33" d="M1620.4,1181c0-31,25.1-56.1,56.1-56.1h-265.9c-30.9,0-55.9,25-55.9,55.9s25,55.9,55.9,55.9h258.8
		C1641.8,1233.1,1620.4,1209.6,1620.4,1181z"/>
	<path fill="#7EA42E" d="M3094.8,1124.9H2777c31,0,56.1,25.1,56.1,56.1c0,29.2-22.3,53.2-50.9,55.9h312.5c30.9,0,56-25.1,56-56
		C3150.7,1149.9,3125.7,1124.9,3094.8,1124.9z"/>
	<g>
		<path fill="#89A82A" d="M2026.4,1487c0-30.9,25-56,55.9-56h-223.4c-30.9,0-55.9,25.1-55.9,56c0,30.9,25,55.9,55.9,55.9h223.4
			C2051.4,1542.9,2026.4,1517.9,2026.4,1487z"/>
	</g>
	<path fill="#7AA32F" d="M2810.7,1389.7c0-22.8,18.5-41.4,41.3-41.4H1795.6c-22.8,0-41.3,18.6-41.3,41.4c0,22.8,18.5,41.3,41.3,41.3
		H2852C2829.2,1431.1,2810.7,1412.5,2810.7,1389.7z"/>
	<path fill="#DDE8C7" d="M2131.4,1348.4h381.4c-31,0-56.2-25.1-56.2-56.2c0-31,25.1-56.4,56.2-56.4h-381.4
		c-31,0-56.2,25.3-56.2,56.4C2075.2,1323.2,2100.3,1348.4,2131.4,1348.4z"/>
	<path fill="#C9DEBD" d="M2337.7,1348.4h221.9c-31,0-56.2-25.1-56.2-56.2c0-31,25.1-56.4,56.2-56.4h-221.9
		c-31,0-56.2,25.3-56.2,56.4C2281.5,1323.2,2306.7,1348.4,2337.7,1348.4z"/>
	<path fill="#E0E8DC" d="M2675.5,1348.3l-531.9,0.1c22.8,0,41.3,18.6,41.3,41.4c0,22.8-18.5,41.3-41.3,41.3h531.9
		c22.8,0,41.3-18.6,41.3-41.4C2716.9,1366.8,2698.4,1348.3,2675.5,1348.3z"/>
	<path fill="#CBDCA6" d="M2509.9,1487c0,30.9-25,55.9-55.9,55.9h-371.7c-30.9,0-55.9-25-55.9-55.9l0,0c0-30.9,25-56,55.9-56H2454
		C2484.9,1431.1,2509.9,1456.2,2509.9,1487L2509.9,1487z"/>
	<path fill="#A3B120" d="M2143.6,1348.4h-715.9c-22.8,0-41.3,18.6-41.3,41.4c0,22.8,18.5,41.3,41.3,41.3h715.9
		c22.8,0,41.3-18.5,41.3-41.3C2184.9,1367,2166.4,1348.4,2143.6,1348.4z"/>
	<path fill="#CBDCA6" d="M2833.1,1181c0,31-25.1,56.1-56.1,56.1H1676.5c-31,0-56.1-25.1-56.1-56.1l0,0c0-31,25.1-56.1,56.1-56.1
		H2777C2808,1124.9,2833.1,1150,2833.1,1181L2833.1,1181z"/>
	<path fill="#DDE8C7" d="M2198.8,1181c0-31,25.1-56.1,56.1-56.1h-578.4c-31,0-56.1,25.1-56.1,56.1s25.1,56.1,56.1,56.1H2255
		C2224,1237.1,2198.8,1212,2198.8,1181z"/>
</g>
<g id="Lake">
	<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="1386.4795" y1="1389.6699" x2="1692.5334" y2="1389.6699">
		<stop  offset="0.1774" style="stop-color:#A3D9D3"/>
		<stop  offset="0.3775" style="stop-color:#9DD5D2"/>
		<stop  offset="0.6451" style="stop-color:#8CCBD0"/>
		<stop  offset="0.9496" style="stop-color:#70B9CD"/>
		<stop  offset="1" style="stop-color:#6BB6CC"/>
	</linearGradient>
	<path fill="url(#SVGID_3_)" d="M1612.8,1389.7c0-22.8,18.5-41.4,41.3-41.4h-226.3c-22.8,0-41.3,18.6-41.3,41.4
		c0,22.8,18.5,41.3,41.3,41.3H1654C1631.3,1431,1612.8,1412.5,1612.8,1389.7z"/>
	<linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="1458.8525" y1="1292.3843" x2="1750.278" y2="1292.3843">
		<stop  offset="0.1774" style="stop-color:#A3D9D3"/>
		<stop  offset="0.3775" style="stop-color:#9DD5D2"/>
		<stop  offset="0.6451" style="stop-color:#8CCBD0"/>
		<stop  offset="0.9496" style="stop-color:#70B9CD"/>
		<stop  offset="1" style="stop-color:#6BB6CC"/>
	</linearGradient>
	<path fill="url(#SVGID_4_)" d="M1694.4,1236.4h-235.5c30.9,0,55.9,25,55.9,55.9c0,30.9-25,56-55.9,56h235.5
		c30.9,0,55.9-25.2,55.9-56C1750.3,1261.4,1725.3,1236.4,1694.4,1236.4z"/>
</g>
<g id="Transport">
	<g>
		<rect x="1923.4" y="1169.6" fill="#829143" width="11.5" height="35.2"/>
		<g>
			<path fill="#8EAE35" d="M1759.3,1164.4c-25,0-46.7,13.8-58.1,34.1c-0.3,0.4-0.5,0.9-0.8,1.4c-0.4,0.9-0.6,1.9-0.6,2.9
				c0,4.2,4.7,8.3,9.4,8.3c0.5,0,2.5,0.1,3,0h11.7l4.7-4.1h46v4.1h83.4v-4.1h46v4.1h22.1v-46.7H1759.3z M1765.9,1188.7h-15.1
				c-9.4,0-18.5,1.6-26.8,4.7l-3.1-7.1c10.7-7.6,24.2-11.9,38.4-11.9h6.5V1188.7z M1791.9,1188.7h-24v-14.4h24V1188.7z
				 M1854.7,1188.7h-31.3v-14.4h31.3V1188.7z M1888.2,1188.7h-31.3v-14.4h31.3V1188.7z M1921.8,1188.7h-31.3v-14.4h31.3V1188.7z"/>
		</g>
		<g>
			<path fill="#8EAE35" d="M2144.3,1164.4H2131h-199.6v46.7h28.3v-4.1h46v4.1h83.4v-4.1h46l4.7,4.1h23.6v-27.5
				C2163.5,1173,2154.9,1164.4,2144.3,1164.4z M1967.5,1188.7h-31.3v-14.4h31.3V1188.7z M2001.1,1188.7h-31.3v-14.4h31.3V1188.7z
				 M2034.6,1188.7h-31.3v-14.4h31.3V1188.7z M2068.1,1188.7h-31.3v-14.4h31.3V1188.7z M2101.7,1188.7h-31.3v-14.4h31.3V1188.7z
				 M2135.2,1188.7h-31.3v-14.4h31.3V1188.7z"/>
		</g>
		<path fill="#5A575D" d="M1762.6,1204.8c-2.4,0-4.4,1.7-4.9,4h-2.3v-0.6h-9.2v0.6h-2.3c-0.4-2.3-2.5-4-4.9-4c-2.7,0-5,2.2-5,5
			c0,2.7,2.2,5,5,5c2.4,0,4.4-1.7,4.9-4h2.3v0.6h9.2v-0.6h2.3c0.4,2.3,2.5,4,4.9,4c2.7,0,5-2.2,5-5
			C1767.5,1207,1765.3,1204.8,1762.6,1204.8z"/>
		<path fill="#5A575D" d="M1893.1,1204.8c-2.4,0-4.4,1.7-4.9,4h-2.3v-0.6h-9.2v0.6h-2.3c-0.4-2.3-2.5-4-4.9-4c-2.7,0-5,2.2-5,5
			c0,2.7,2.2,5,5,5c2.4,0,4.4-1.7,4.9-4h2.3v0.6h9.2v-0.6h2.3c0.4,2.3,2.5,4,4.9,4c2.7,0,5-2.2,5-5
			C1898.1,1207,1895.9,1204.8,1893.1,1204.8z"/>
		<path fill="#829143" d="M1699.9,1202.8c0,4.2,4.7,8.3,9.4,8.3c0.5,0,2.5,0.1,3,0h11.7l4.7-4.1h46v4.1h83.4v-4.1h46v4.1h22.1v-9.2
			H1700C1699.9,1202.2,1699.9,1202.5,1699.9,1202.8z"/>
		<g>
			<path fill="#829143" d="M1801.5,1159.6l-3.6,5.4h72.2l-3.2-5.4H1801.5z M1814.9,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C1815.9,1162.3,1815.5,1162.8,1814.9,1162.8z M1831.5,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C1832.5,1162.3,1832.1,1162.8,1831.5,1162.8z M1848.1,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C1849.1,1162.3,1848.6,1162.8,1848.1,1162.8z M1864.6,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C1865.6,1162.3,1865.2,1162.8,1864.6,1162.8z"/>
			<path fill="#829143" d="M2074.7,1159.6h-65.4l-3.6,5.4h72.2L2074.7,1159.6z M2022.8,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1
				h12c0.6,0,1,0.5,1,1C2023.8,1162.3,2023.4,1162.8,2022.8,1162.8z M2039.4,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C2040.4,1162.3,2040,1162.8,2039.4,1162.8z M2056,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C2057,1162.3,2056.5,1162.8,2056,1162.8z M2072.6,1162.8h-12c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h12
				c0.6,0,1,0.5,1,1C2073.6,1162.3,2073.1,1162.8,2072.6,1162.8z"/>
		</g>
		<path fill="#5A575D" d="M1993.4,1204.8c-2.4,0-4.4,1.7-4.9,4h-2.3v-0.6h-9.2v0.6h-2.3c-0.4-2.3-2.5-4-4.9-4c-2.7,0-5,2.2-5,5
			c0,2.7,2.2,5,5,5c2.4,0,4.4-1.7,4.9-4h2.3v0.6h9.2v-0.6h2.3c0.4,2.3,2.5,4,4.9,4c2.7,0,5-2.2,5-5
			C1998.4,1207,1996.2,1204.8,1993.4,1204.8z"/>
		<path fill="#5A575D" d="M2124,1204.8c-2.4,0-4.4,1.7-4.9,4h-2.3v-0.6h-9.2v0.6h-2.3c-0.4-2.3-2.5-4-4.9-4c-2.7,0-5,2.2-5,5
			c0,2.7,2.2,5,5,5c2.4,0,4.4-1.7,4.9-4h2.3v0.6h9.2v-0.6h2.3c0.4,2.3,2.5,4,4.9,4c2.7,0,5-2.2,5-5
			C2129,1207,2126.7,1204.8,2124,1204.8z"/>
		<polygon fill="#829143" points="1931.4,1201.8 1931.4,1211.1 1959.7,1211.1 1959.7,1207 2005.7,1207 2005.7,1211.1 2089.2,1211.1 
			2089.2,1207 2135.2,1207 2139.9,1211.1 2163.5,1211.1 2163.5,1201.8 		"/>
	</g>
	<g>
		<g>
			<g>
				<path fill="#5A575D" d="M2172,1507.3c-6.4,0-11.7,5.2-11.7,11.7c0,6.4,5.2,11.7,11.7,11.7c6.4,0,11.7-5.2,11.7-11.7
					C2183.7,1512.5,2178.5,1507.3,2172,1507.3z M2172,1525.8c-3.8,0-6.9-3.1-6.9-6.9c0-3.8,3.1-6.9,6.9-6.9c3.8,0,6.9,3.1,6.9,6.9
					C2178.9,1522.7,2175.8,1525.8,2172,1525.8z"/>
				<path fill="#5A575D" d="M2172,1516.1c-1.5,0-2.8,1.3-2.8,2.8c0,1.5,1.3,2.8,2.8,2.8c1.5,0,2.8-1.3,2.8-2.8
					C2174.8,1517.4,2173.6,1516.1,2172,1516.1z"/>
				<path fill="#5A575D" d="M2266.9,1507.3c-6.4,0-11.7,5.2-11.7,11.7c0,6.4,5.2,11.7,11.7,11.7c6.4,0,11.7-5.2,11.7-11.7
					C2278.5,1512.5,2273.3,1507.3,2266.9,1507.3z M2266.9,1525.8c-3.8,0-6.9-3.1-6.9-6.9c0-3.8,3.1-6.9,6.9-6.9
					c3.8,0,6.9,3.1,6.9,6.9C2273.7,1522.7,2270.7,1525.8,2266.9,1525.8z"/>
				<path fill="#5A575D" d="M2266.9,1516.1c-1.5,0-2.8,1.3-2.8,2.8c0,1.5,1.3,2.8,2.8,2.8s2.8-1.3,2.8-2.8
					C2269.7,1517.4,2268.4,1516.1,2266.9,1516.1z"/>
			</g>
			<g>
				<g>
					<g>
						<path fill="#89A82A" d="M2242.1,1457.9H2190l-2.4,5.1h56.9L2242.1,1457.9z M2205.3,1462H2192c-0.7,0-1.3-0.6-1.3-1.3
							c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3C2206.6,1461.4,2206,1462,2205.3,1462z M2222.7,1461.9h-13.3
							c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3C2224,1461.3,2223.4,1461.9,2222.7,1461.9z
							 M2240.1,1461.8h-13.3c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3
							C2241.4,1461.2,2240.8,1461.8,2240.1,1461.8z"/>
					</g>
					<g>
						<path fill="#89A82A" d="M2296.1,1462.9h-152.5c-2.7,0-4.8,2.4-4.8,5.1v46.2c0,6.1,4.7,7,7.4,7h11.9c-0.1-0.8-0.2-1.6-0.2-2.4
							c0-7.9,6.4-14.2,14.2-14.2s14.2,6.4,14.2,14.2c0,0.8-0.1,1.6-0.2,2.4h66.8c-0.1-0.8-0.2-1.6-0.2-2.4c0-7.9,6.4-14.2,14.2-14.2
							s14.2,6.4,14.2,14.2c0,0.8-0.1,1.6-0.2,2.4h9.8c14.2,0,19.3,0.9,19.3-14.9v-20.6C2310,1472.2,2304.6,1462.9,2296.1,1462.9z
							 M2279.8,1497c-8-5.8-14.9-11.5-29.4-11.5c-5.7,0-98.4,0-98.4,0c-7.8,0-10.4-0.9-10.4-9.2v-2.2v-6c0-1.1,0.9-2.1,1.9-2.1
							h136.2V1497z M2307.1,1508.5c-0.6,0-6-0.5-13.2-3.3c-4.7-1.9-8.5-4.2-11.9-6.6v-32.7h14c0.2,0,0.4,0,0.6,0c0.1,0,0.1,0,0.2,0
							c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.2,0,0.4,0.1,0.5,0.1c0,0,0,0,0,0c0.2,0,0.3,0.1,0.5,0.1c0.1,0,0.1,0,0.2,0
							c0.1,0,0.3,0.1,0.4,0.1c0,0,0.1,0,0.1,0.1c0.2,0.1,0.3,0.1,0.5,0.2c0,0,0,0,0,0c0.1,0.1,0.3,0.1,0.4,0.2c0,0,0.1,0.1,0.1,0.1
							c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.3,0.2,0.4,0.3c0,0,0,0,0.1,0c0.1,0.1,0.2,0.2,0.3,0.3
							c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.2,0.3,0.2c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.2,0.3,0.4c0,0,0,0,0.1,0.1
							c0.1,0.1,0.2,0.2,0.3,0.3c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.3,0.3,0.4
							c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0,0.1,0.1,0.1,0.1,0.2c0.1,0.1,0.1,0.2,0.2,0.3c0,0.1,0.1,0.1,0.1,0.2
							c0.1,0.1,0.1,0.3,0.2,0.4c0,0.1,0.1,0.1,0.1,0.2c0,0.1,0.1,0.2,0.1,0.2c0,0.1,0.1,0.1,0.1,0.2c0,0.1,0.1,0.1,0.1,0.2
							c0,0.1,0.1,0.1,0.1,0.2c0,0,0,0.1,0.1,0.1c0.1,0.2,0.2,0.3,0.2,0.5c0,0,0,0.1,0,0.1c0,0.1,0.1,0.2,0.1,0.3c0,0,0,0,0,0.1
							c0.6,1.4,0.9,2.6,1.1,3.3c0.3,1.2,0.7,3.9,0.7,7.5C2307.1,1488.6,2307.1,1508.5,2307.1,1508.5z"/>
					</g>
				</g>
			</g>
		</g>
		<g>
			<g>
				<g>
					<path fill="#6C9229" d="M2214.1,1501.9l-8.9-1.4v14.7l8.9-1.3c0.8,0,1.5-0.7,1.5-1.5v-9
						C2215.6,1502.6,2215,1501.9,2214.1,1501.9z"/>
				</g>
				<g>
					<path fill="#6C9229" d="M2205,1516.7c-1,0-1.8-0.8-1.8-1.8v-13.9c0-1,0.8-1.8,1.8-1.8l0,0c1,0,1.8,0.8,1.8,1.8v13.9
						C2206.9,1515.8,2206,1516.7,2205,1516.7L2205,1516.7z"/>
				</g>
				<g>
					<g>
						<path fill="#6C9229" d="M2197.8,1503.2c-0.6,0-1,0.5-1,1c0,0.6,0.5,1,1,1h7.5v-2.1H2197.8z"/>
					</g>
					<g>
						<path fill="#6C9229" d="M2197.8,1510.5c-0.6,0-1,0.5-1,1s0.5,1,1,1h7.5v-2.1H2197.8z"/>
					</g>
				</g>
				<g>
					<path fill="#6C9229" d="M2217.9,1509.2c0.3,0,0.5,0.2,0.5,0.5v1.3l1.9-0.4v-1.1c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5
						v0.9l1.4-0.3v-4.5l-1.4-0.3v0.9c0,0.3-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5v-1.1l-1.9-0.4v1.3c0,0.3-0.2,0.5-0.5,0.5
						c-0.3,0-0.5-0.2-0.5-0.5v-1.6l-1.9-0.4v7.6l1.9-0.4v-1.6C2217.4,1509.4,2217.6,1509.2,2217.9,1509.2z"/>
				</g>
			</g>
			<path fill="#89A82A" d="M2242.1,1457.9H2190l-2.3,5h56.8L2242.1,1457.9z M2205.3,1462H2192c-0.7,0-1.3-0.6-1.3-1.3
				c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3C2206.6,1461.4,2206,1462,2205.3,1462z M2222.7,1461.9h-13.3
				c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3C2224,1461.3,2223.4,1461.9,2222.7,1461.9z
				 M2240.1,1461.8h-13.3c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3h13.3c0.7,0,1.3,0.6,1.3,1.3
				C2241.4,1461.2,2240.8,1461.8,2240.1,1461.8z"/>
			<polygon fill="#6C9229" points="2244.5,1463 2244.4,1462.9 2187.7,1462.9 2187.6,1463 			"/>
			<path fill="#6C9229" d="M2248.6,1485.5c-2.5,12.2-13.3,21.4-26.3,21.4v1.9c14,0,25.7-10.1,28.2-23.3c-0.1,0-0.1,0-0.2,0
				C2250.1,1485.5,2249.5,1485.5,2248.6,1485.5z"/>
		</g>
	</g>
	<g>
		<g>
			<g>
				<g>
					<path fill="#728C45" d="M2466.6,1272.6l-1.6-10h16.5l-1.4,10c0,0.9-0.8,1.7-1.7,1.7h-10.1
						C2467.4,1274.3,2466.6,1273.6,2466.6,1272.6z"/>
				</g>
				<g>
					<path fill="#728C45" d="M2483.2,1262.4c0-1.1-0.9-2.1-2.1-2.1h-15.6c-1.1,0-2.1,0.9-2.1,2.1l0,0c0,1.1,0.9,2.1,2.1,2.1h15.6
						C2482.3,1264.5,2483.2,1263.6,2483.2,1262.4L2483.2,1262.4z"/>
				</g>
				<g>
					<g>
						<path fill="#728C45" d="M2468,1254.3c0-0.6,0.5-1.2,1.2-1.2c0.6,0,1.2,0.5,1.2,1.2v8.4h-2.3V1254.3z"/>
					</g>
					<g>
						<path fill="#728C45" d="M2476.3,1254.3c0-0.6,0.5-1.2,1.2-1.2s1.2,0.5,1.2,1.2v8.4h-2.3V1254.3z"/>
					</g>
				</g>
				<g>
					<path fill="#728C45" d="M2474.8,1276.9c0,0.3,0.3,0.6,0.6,0.6h1.5l-0.5,2.2h-1.2c-0.3,0-0.6,0.3-0.6,0.6c0,0.3,0.3,0.6,0.6,0.6
						h1l-0.3,1.6h-5.1l-0.3-1.6h1c0.3,0,0.6-0.3,0.6-0.6c0-0.3-0.3-0.6-0.6-0.6h-1.2l-0.5-2.2h1.5c0.3,0,0.6-0.3,0.6-0.6
						c0-0.3-0.3-0.6-0.6-0.6h-1.8l-0.4-2.1h8.6l-0.4,2.1h-1.8C2475,1276.3,2474.8,1276.6,2474.8,1276.9z"/>
				</g>
			</g>
			<g>
				<path fill="#728C45" d="M2446.2,1310c15.5,0,28-12.6,28-28h-1.8c0,14.4-11.8,26.2-26.2,26.2V1310z"/>
			</g>
		</g>
		<g>
			<rect x="2404.5" y="1270.5" fill="#92A34C" width="2" height="23.8"/>
			<path fill="#92A34C" d="M2445.2,1293c-1.2-4.6-5.8-15.6-10.7-20.3l1.8,0.1c0.5,0,1-0.4,1-0.9c0-0.5-0.4-1-0.9-1l-6.7-0.4
				c0,0-0.1,0-0.1,0c-8.3-0.5-32.1-1.8-38.9-1.8c-7,0-13.3,2.2-28.8,14c-15.5,11.8-27.3,21.2-27.3,28.8v12.7c0,0-0.6,4.7,4.4,5
				c-0.3-1.1-0.5-2.3-0.5-3.5c0-6.8,5.6-12.4,12.4-12.4c6.8,0,12.4,5.6,12.4,12.4c0,1.2-0.2,2.4-0.5,3.5c17.2,0,40.3,0,56.7,0
				c-0.4-1.3-0.7-2.6-0.7-4c0-6.8,5.6-12.4,12.4-12.4c6.8,0,12.4,5.6,12.4,12.4c0,0.4,0,0.7,0,1.1c2.2-0.1,3.7-0.5,4.1-2.9
				C2448.9,1316.2,2445.9,1295.7,2445.2,1293z M2345.5,1306.2c-2.6,2.1-5.4,3-6.3,1.9c-0.9-1.1,0.4-3.7,3-5.9c2.6-2.1,5.4-3,6.3-1.9
				C2349.4,1301.5,2348.1,1304.1,2345.5,1306.2z M2352.6,1292.4c3.3-2.7,6.9-5.4,10.5-8.2c15.1-11.5,21.1-13.6,27.6-13.6
				c6.9,0,31.6,1.3,39.6,1.8c4.2,0.3,10.6,12.8,12.7,19.9H2352.6z"/>
		</g>
		<g>
			<path fill="#5A575D" d="M2350.8,1315.7c-5.5,0-9.9,4.4-9.9,9.9c0,5.5,4.4,9.9,9.9,9.9c5.5,0,9.9-4.4,9.9-9.9
				C2360.7,1320.2,2356.3,1315.7,2350.8,1315.7z M2350.8,1332.5c-3.8,0-6.9-3.1-6.9-6.9s3.1-6.9,6.9-6.9c3.8,0,6.9,3.1,6.9,6.9
				S2354.6,1332.5,2350.8,1332.5z"/>
			<circle fill="#5A575D" cx="2350.8" cy="1325.6" r="2.3"/>
		</g>
		<g>
			<path fill="#5A575D" d="M2431.1,1315.2c-5.5,0-9.9,4.4-9.9,9.9c0,5.5,4.4,9.9,9.9,9.9c5.5,0,9.9-4.4,9.9-9.9
				C2441,1319.7,2436.5,1315.2,2431.1,1315.2z M2431.1,1332c-3.8,0-6.9-3.1-6.9-6.9c0-3.8,3.1-6.9,6.9-6.9c3.8,0,6.9,3.1,6.9,6.9
				C2438,1328.9,2434.9,1332,2431.1,1332z"/>
			<circle fill="#5A575D" cx="2431.1" cy="1325.1" r="2.3"/>
		</g>
	</g>
	<g>
		<g>
			<path fill="#5B6C4C" d="M2620.2,1299.3c0-3.9-3.1-7-7-7h-23.4c-3.9,0-7,3.1-7,7v44.3h37.4V1299.3z"/>
			<path fill="#5B6C4C" d="M2624.2,1345.4c0-1.6-1.3-2.9-2.9-2.9h-39.4c-1.6,0-2.9,1.3-2.9,2.9l0,0c0,1.6,1.3,2.9,2.9,2.9h39.4
				C2622.9,1348.3,2624.2,1347,2624.2,1345.4L2624.2,1345.4z"/>
			<path fill="#DDE8C7" d="M2616.9,1307.1c0,2.7-2.2,4.8-4.8,4.8h-21c-2.7,0-4.8-2.2-4.8-4.8v-6.5c0-2.7,2.2-4.8,4.8-4.8h21
				c2.7,0,4.8,2.2,4.8,4.8V1307.1z"/>
			<g>
				<path fill="#546546" d="M2621.3,1342.5h-19.7v5.9h19.7c1.6,0,2.9-1.3,2.9-2.9C2624.2,1343.8,2622.9,1342.5,2621.3,1342.5z"/>
				<path fill="#546546" d="M2620.2,1299.3c0-3.9-3.1-7-7-7h-11.7v3.5h10.5c2.7,0,4.8,2.2,4.8,4.8v6.5c0,2.7-2.2,4.8-4.8,4.8h-10.5
					v31.7h18.7V1299.3z"/>
				<path fill="#D8E2BD" d="M2612,1311.9c2.7,0,4.8-2.2,4.8-4.8v-6.5c0-2.7-2.2-4.8-4.8-4.8h-10.5v16.2H2612z"/>
			</g>
		</g>
		<g>
			<g>
				<g>
					<path fill="#546546" d="M2635.8,1320.6l-1.1-7.2h11.9l-1,7.2c0,0.7-0.6,1.2-1.2,1.2h-7.3
						C2636.4,1321.8,2635.8,1321.3,2635.8,1320.6z"/>
				</g>
				<g>
					<path fill="#546546" d="M2647.8,1313.2c0-0.8-0.7-1.5-1.5-1.5H2635c-0.8,0-1.5,0.7-1.5,1.5l0,0c0,0.8,0.7,1.5,1.5,1.5h11.3
						C2647.1,1314.7,2647.8,1314.1,2647.8,1313.2L2647.8,1313.2z"/>
				</g>
				<g>
					<g>
						<path fill="#546546" d="M2636.8,1307.4c0-0.5,0.4-0.8,0.8-0.8s0.8,0.4,0.8,0.8v6.1h-1.7V1307.4z"/>
					</g>
					<g>
						<path fill="#546546" d="M2642.8,1307.4c0-0.5,0.4-0.8,0.8-0.8c0.5,0,0.8,0.4,0.8,0.8v6.1h-1.7V1307.4z"/>
					</g>
				</g>
				<g>
					<path fill="#546546" d="M2641.7,1323.7c0,0.2,0.2,0.4,0.4,0.4h1.1l-0.3,1.6h-0.9c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4
						h0.7l-0.2,1.1h-3.7l-0.2-1.1h0.7c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.2-0.4-0.4-0.4h-0.9l-0.3-1.6h1.1c0.2,0,0.4-0.2,0.4-0.4
						c0-0.2-0.2-0.4-0.4-0.4h-1.3l-0.3-1.5h6.2l-0.3,1.5h-1.3C2641.9,1323.3,2641.7,1323.5,2641.7,1323.7z"/>
				</g>
			</g>
			<path fill="#546546" d="M2639.6,1327.3v11.8c0,1.9-1.5,3.4-3.4,3.4c-1.9,0-3.4-1.5-3.4-3.4v-3.9c0-3.1-2.5-5.6-5.5-5.6h-6.3v2.2
				h6.3c1.9,0,3.3,1.5,3.3,3.4v3.9c0,3.1,2.5,5.6,5.6,5.6s5.6-2.5,5.6-5.6v-11.8H2639.6z"/>
		</g>
	</g>
	<g>
		<g>
			<g>
				<path fill="#8EAE35" d="M2651.1,1231.3c-6,0-10.9-4.9-10.9-10.9c0-6,4.9-10.9,10.9-10.9s10.9,4.9,10.9,10.9
					C2662,1226.3,2657.1,1231.3,2651.1,1231.3z M2651.1,1210.8c-5.2,0-9.5,4.3-9.5,9.5c0,5.2,4.3,9.5,9.5,9.5c5.2,0,9.5-4.3,9.5-9.5
					C2660.6,1215.1,2656.3,1210.8,2651.1,1210.8z"/>
			</g>
			<g>
				<path fill="#8EAE35" d="M2685.6,1231.3c-6,0-10.9-4.9-10.9-10.9c0-6,4.9-10.9,10.9-10.9c6,0,10.9,4.9,10.9,10.9
					C2696.5,1226.3,2691.6,1231.3,2685.6,1231.3z M2685.6,1210.8c-5.2,0-9.5,4.3-9.5,9.5c0,5.2,4.3,9.5,9.5,9.5
					c5.2,0,9.5-4.3,9.5-9.5C2695.1,1215.1,2690.8,1210.8,2685.6,1210.8z"/>
			</g>
			<g>
				<path fill="#8EAE35" d="M2686.6,1221.2h-16.8l-11.8-15.6h18.6L2686.6,1221.2z M2670.5,1219.8h13.5l-8.2-12.8h-15L2670.5,1219.8z
					"/>
			</g>
			<g>
				<polygon fill="#8EAE35" points="2651.9,1219.8 2650.5,1219.3 2655.5,1204.1 2653.7,1201.6 2651.3,1203.3 2650.5,1202.2 
					2654.1,1199.6 2657,1203.9 				"/>
			</g>
			<g>
				<circle fill="#8EAE35" cx="2670.3" cy="1220.7" r="3.9"/>
			</g>
		</g>
		<g>
			<path fill="#8EAE35" d="M2664.3,1187.7c0,2.3-1.9,4.1-4.1,4.1c-2.3,0-4.1-1.9-4.1-4.1s1.8-4.1,4.1-4.1
				C2662.5,1183.6,2664.3,1185.4,2664.3,1187.7z"/>
			<g>
				<path fill="#8EAE35" d="M2678.7,1197.5c0.7,1.1,0.3,2.6-0.8,3.2l-9.3,5.4c-1.1,0.7-2.6,0.3-3.2-0.8l0,0
					c-0.7-1.1-0.3-2.6,0.8-3.2l9.3-5.4C2676.6,1196,2678,1196.4,2678.7,1197.5L2678.7,1197.5z"/>
				<path fill="#8EAE35" d="M2673,1214.5c-0.9,0.5-2.1,0.2-2.6-0.7l-5.1-8.6c-0.5-0.9-0.2-2.1,0.7-2.6l0,0c0.9-0.5,2.1-0.2,2.6,0.7
					l5.1,8.6C2674.2,1212.8,2673.9,1214,2673,1214.5L2673,1214.5z"/>
			</g>
			<g>
				<path fill="#8EAE35" d="M2681.4,1196.8c1,0.8,1.3,2.3,0.5,3.3l-6.5,8.6c-0.8,1-2.3,1.3-3.3,0.5l0,0c-1-0.8-1.3-2.3-0.5-3.3
					l6.5-8.6C2678.9,1196.2,2680.4,1196,2681.4,1196.8L2681.4,1196.8z"/>
				<path fill="#8EAE35" d="M2671,1221.3c-1.2-0.2-2.1-1.4-1.9-2.6l1.9-11.7c0.2-1.2,1.4-2.1,2.6-1.9l0,0c1.2,0.2,2.1,1.4,1.9,2.6
					l-1.9,11.7C2673.4,1220.7,2672.2,1221.5,2671,1221.3L2671,1221.3z"/>
			</g>
			<g>
				<path fill="#8EAE35" d="M2679.9,1192.8l-12.4-4.3l-2.5,8.8l11.9,4.1c2.4,0.8,5-0.5,5.8-2.8
					C2683.6,1196.2,2682.3,1193.6,2679.9,1192.8z"/>
				<g>
					<path fill="#8EAE35" d="M2664.9,1201.3c0.4,1,0,2.1-1,2.5l-8,3.5c-1,0.4-2.1,0-2.5-1l0,0c-0.4-1,0-2.1,1-2.5l8-3.5
						C2663.4,1199.9,2664.5,1200.4,2664.9,1201.3L2664.9,1201.3z"/>
					<path fill="#8EAE35" d="M2667.5,1188.5c1,0.4,1.5,1.5,1.1,2.5l-3.7,11.8c-0.4,1-1.5,1.5-2.5,1.1l0,0c-1-0.4-1.5-1.5-1.1-2.5
						l3.7-11.8C2665.4,1188.6,2666.5,1188.1,2667.5,1188.5L2667.5,1188.5z"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g id="Gren_x5F_Elements_x5F_Ground">
	<g>
		<polygon fill="#607150" points="2649.4,1542.8 2637.6,1542.8 2639.5,1501.9 2647.6,1501.9 		"/>
		<g>
			<path fill="#B9D432" d="M2687.9,1469.6c0-8.8-7.1-15.9-15.9-15.9h-3.1c4.3-1.7,7.3-5.9,7.3-10.8c0-6.5-5.2-11.7-11.7-11.7h-42
				c-6.5,0-11.7,5.2-11.7,11.7c0,4.9,3,9.1,7.3,10.8h-3.1c-8.8,0-15.9,7.1-15.9,15.9c0,8.8,7.1,15.9,15.9,15.9h3.5
				c-4.5,1.6-7.8,5.9-7.8,11c0,6.5,5.2,11.7,11.7,11.7h42c6.5,0,11.7-5.2,11.7-11.7c0-5.1-3.2-9.4-7.8-11h3.5
				C2680.8,1485.4,2687.9,1478.3,2687.9,1469.6z"/>
			<path fill="#A8CE38" d="M2687.9,1469.6c0-8.8-7.1-15.9-15.9-15.9h-3.1c4.3-1.7,7.3-5.9,7.3-10.8c0-6.5-5.2-11.7-11.7-11.7h-21
				v77.1h21c6.5,0,11.7-5.2,11.7-11.7c0-5.1-3.2-9.4-7.8-11h3.5C2680.8,1485.4,2687.9,1478.3,2687.9,1469.6z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2214,1348.4 2202.5,1348.4 2204.3,1308.6 2212.2,1308.6 		"/>
		<g>
			<path fill="#B9D432" d="M2251.5,1277c0-8.5-6.9-15.5-15.5-15.5h-3c4.2-1.7,7.1-5.8,7.1-10.6c0-6.3-5.1-11.4-11.4-11.4h-41
				c-6.3,0-11.4,5.1-11.4,11.4c0,4.8,2.9,8.9,7.1,10.6h-3c-8.5,0-15.5,6.9-15.5,15.5c0,8.5,6.9,15.5,15.5,15.5h3.4
				c-4.4,1.6-7.6,5.8-7.6,10.7c0,6.3,5.1,11.4,11.4,11.4h41c6.3,0,11.4-5.1,11.4-11.4c0-5-3.2-9.2-7.6-10.7h3.4
				C2244.5,1292.5,2251.5,1285.6,2251.5,1277z"/>
			<path fill="#A8CE38" d="M2251.5,1277c0-8.5-6.9-15.5-15.5-15.5h-3c4.2-1.7,7.1-5.8,7.1-10.6c0-6.3-5.1-11.4-11.4-11.4h-20.5v75.1
				h20.5c6.3,0,11.4-5.1,11.4-11.4c0-5-3.2-9.2-7.6-10.7h3.4C2244.5,1292.5,2251.5,1285.6,2251.5,1277z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2300.6,1237.1 2291.4,1237.1 2292.8,1205.2 2299.1,1205.2 		"/>
		<g>
			<path fill="#B9D432" d="M2330.6,1180c0-6.8-5.5-12.4-12.4-12.4h-2.4c3.3-1.4,5.7-4.6,5.7-8.5c0-5-4.1-9.1-9.1-9.1h-32.8
				c-5,0-9.1,4.1-9.1,9.1c0,3.8,2.4,7.1,5.7,8.5h-2.4c-6.8,0-12.4,5.5-12.4,12.4c0,6.8,5.5,12.4,12.4,12.4h2.8
				c-3.5,1.3-6.1,4.6-6.1,8.6c0,5,4.1,9.1,9.1,9.1h32.8c5,0,9.1-4.1,9.1-9.1c0-4-2.5-7.3-6.1-8.6h2.8
				C2325,1192.3,2330.6,1186.8,2330.6,1180z"/>
			<path fill="#A8CE38" d="M2330.6,1180c0-6.8-5.5-12.4-12.4-12.4h-2.4c3.3-1.4,5.7-4.6,5.7-8.5c0-5-4.1-9.1-9.1-9.1H2296v60.1h16.4
				c5,0,9.1-4.1,9.1-9.1c0-4-2.5-7.3-6.1-8.6h2.8C2325,1192.3,2330.6,1186.8,2330.6,1180z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2405.2,1542.9 2394.8,1542.9 2396.5,1507.1 2403.6,1507.1 		"/>
		<g>
			<path fill="#B9D432" d="M2438.9,1478.7c0-7.7-6.2-13.9-13.9-13.9h-2.7c3.7-1.5,6.4-5.2,6.4-9.5c0-5.7-4.6-10.3-10.3-10.3h-36.8
				c-5.7,0-10.3,4.6-10.3,10.3c0,4.3,2.6,8,6.4,9.5h-2.7c-7.7,0-13.9,6.2-13.9,13.9c0,7.7,6.2,13.9,13.9,13.9h3.1
				c-4,1.4-6.8,5.2-6.8,9.7c0,5.7,4.6,10.3,10.3,10.3h36.8c5.7,0,10.3-4.6,10.3-10.3c0-4.5-2.8-8.2-6.8-9.7h3.1
				C2432.6,1492.6,2438.9,1486.4,2438.9,1478.7z"/>
			<path fill="#A8CE38" d="M2438.9,1478.7c0-7.7-6.2-13.9-13.9-13.9h-2.7c3.7-1.5,6.4-5.2,6.4-9.5c0-5.7-4.6-10.3-10.3-10.3H2400
				v67.5h18.4c5.7,0,10.3-4.6,10.3-10.3c0-4.5-2.8-8.2-6.8-9.7h3.1C2432.6,1492.6,2438.9,1486.4,2438.9,1478.7z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="1465.5,1236.5 1455.2,1236.5 1456.8,1200.8 1463.9,1200.8 		"/>
		<g>
			<path fill="#B9D432" d="M1499.1,1172.5c0-7.7-6.2-13.9-13.9-13.9h-2.7c3.7-1.5,6.4-5.2,6.4-9.5c0-5.7-4.6-10.2-10.2-10.2H1442
				c-5.7,0-10.2,4.6-10.2,10.2c0,4.3,2.6,8,6.4,9.5h-2.7c-7.7,0-13.9,6.2-13.9,13.9c0,7.7,6.2,13.9,13.9,13.9h3.1
				c-4,1.4-6.8,5.2-6.8,9.6c0,5.7,4.6,10.2,10.2,10.2h36.7c5.7,0,10.2-4.6,10.2-10.2c0-4.4-2.8-8.2-6.8-9.6h3.1
				C1492.9,1186.4,1499.1,1180.2,1499.1,1172.5z"/>
			<path fill="#A8CE38" d="M1499.1,1172.5c0-7.7-6.2-13.9-13.9-13.9h-2.7c3.7-1.5,6.4-5.2,6.4-9.5c0-5.7-4.6-10.2-10.2-10.2h-18.4
				v67.3h18.4c5.7,0,10.2-4.6,10.2-10.2c0-4.4-2.8-8.2-6.8-9.6h3.1C1492.9,1186.4,1499.1,1180.2,1499.1,1172.5z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1669.8,1431 1661.7,1431 1663,1403 1668.5,1403 		"/>
		<g>
			<circle fill="#6B9F33" cx="1665.7" cy="1375.9" r="31.3"/>
			<path fill="#61922E" d="M1665.7,1344.6v62.5c17.3,0,31.3-14,31.3-31.3C1697,1358.6,1683,1344.6,1665.7,1344.6z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2587.7,1237.1 2578.7,1237.1 2580.1,1206.1 2586.3,1206.1 		"/>
		<g>
			<path fill="#6B9F33" d="M2612.1,1188.3c0-5.7-4.6-10.4-10.4-10.4h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.2-3.4-7.6-7.6-7.6h-27.4
				c-4.2,0-7.6,3.4-7.6,7.6c0,3.2,2,5.9,4.8,7.1h-2c-5.7,0-10.4,4.6-10.4,10.4c0,5.7,4.6,10.4,10.4,10.4h2.3c-3,1.1-5.1,3.9-5.1,7.2
				c0,4.2,3.4,7.6,7.6,7.6h27.4c4.2,0,7.6-3.4,7.6-7.6c0-3.3-2.1-6.1-5.1-7.2h2.3C2607.5,1198.7,2612.1,1194,2612.1,1188.3z"/>
			<path fill="#61922E" d="M2612.1,1188.3c0-5.7-4.6-10.4-10.4-10.4h-2c2.8-1.1,4.8-3.9,4.8-7.1c0-4.2-3.4-7.6-7.6-7.6h-13.7v50.3
				h13.7c4.2,0,7.6-3.4,7.6-7.6c0-3.3-2.1-6.1-5.1-7.2h2.3C2607.5,1198.7,2612.1,1194,2612.1,1188.3z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2163,1431.1 2154.1,1431.1 2155.5,1400.3 2161.6,1400.3 		"/>
		<g>
			<path fill="#6B9F33" d="M2200.2,1371.4c0-8.2-6.7-14.9-14.9-14.9h-2.9c4-1.6,6.8-5.6,6.8-10.2c0-6.1-4.9-11-11-11h-39.4
				c-6.1,0-11,4.9-11,11c0,4.6,2.8,8.5,6.8,10.2h-2.9c-8.2,0-14.9,6.7-14.9,14.9c0,8.2,6.7,14.9,14.9,14.9h3.3
				c-4.2,1.5-7.3,5.6-7.3,10.3c0,6.1,4.9,11,11,11h39.4c6.1,0,11-4.9,11-11c0-4.8-3-8.8-7.3-10.3h3.3
				C2193.5,1386.3,2200.2,1379.6,2200.2,1371.4z"/>
			<path fill="#61922E" d="M2200.2,1371.4c0-8.2-6.7-14.9-14.9-14.9h-2.9c4-1.6,6.8-5.6,6.8-10.2c0-6.1-4.9-11-11-11h-19.7v72.3
				h19.7c6.1,0,11-4.9,11-11c0-4.8-3-8.8-7.3-10.3h3.3C2193.5,1386.3,2200.2,1379.6,2200.2,1371.4z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1618.9,1431 1608.9,1431 1610.4,1396.3 1617.3,1396.3 		"/>
		<g>
			<circle fill="#6B9F33" cx="1613.9" cy="1362.6" r="38.8"/>
			<path fill="#61922E" d="M1613.9,1323.8v77.5c21.4,0,38.8-17.4,38.8-38.8S1635.3,1323.8,1613.9,1323.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1747.6,1542.9 1739.2,1542.9 1740.5,1514 1746.2,1514 		"/>
		<g>
			<circle fill="#6B9F33" cx="1743.4" cy="1486" r="32.3"/>
			<path fill="#61922E" d="M1743.4,1453.8v64.5c17.8,0,32.3-14.4,32.3-32.3C1775.6,1468.2,1761.2,1453.8,1743.4,1453.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1702.8,1542.9 1694.5,1542.9 1695.8,1514 1701.5,1514 		"/>
		<g>
			<circle fill="#6B9F33" cx="1698.7" cy="1486" r="32.3"/>
			<path fill="#61922E" d="M1698.7,1453.8v64.5c17.8,0,32.3-14.4,32.3-32.3C1730.9,1468.2,1716.5,1453.8,1698.7,1453.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="2756.6,1542.9 2748.3,1542.9 2749.6,1514 2755.3,1514 		"/>
		<g>
			<circle fill="#6B9F33" cx="2752.4" cy="1486" r="32.3"/>
			<path fill="#61922E" d="M2752.4,1453.8v64.5c17.8,0,32.3-14.4,32.3-32.3S2770.3,1453.8,2752.4,1453.8z"/>
		</g>
	</g>
	<g>
		<path fill="#728E29" d="M2986.3,1218.5c-0.1-9-7.4-16.3-16.5-16.3c-9,0-16.4,7.3-16.5,16.3c-9.3,0.9-16.5,8.7-16.5,18.2h65.9
			C3002.8,1227.3,2995.6,1219.5,2986.3,1218.5z"/>
		<path fill="#698426" d="M2986.3,1218.5c-0.1-9-7.4-16.3-16.5-16.3v34.5h33C3002.8,1227.3,2995.6,1219.5,2986.3,1218.5z"/>
	</g>
	<g>
		<path fill="#B9D432" d="M2771.4,1410.3c-0.1-10.3-8.4-18.5-18.7-18.5c-10.3,0-18.6,8.3-18.7,18.5c-10.5,1-18.7,9.9-18.7,20.7h74.9
			C2790.1,1420.2,2781.9,1411.4,2771.4,1410.3z"/>
		<path fill="#A8CE38" d="M2771.4,1410.3c-0.1-10.3-8.4-18.5-18.7-18.5v39.2h37.4C2790.1,1420.2,2781.9,1411.4,2771.4,1410.3z"/>
	</g>
	<g>
		<path fill="#B9D432" d="M2568.9,1410.4c-0.1-10.2-8.4-18.5-18.7-18.5c-10.3,0-18.6,8.3-18.7,18.5c-10.5,1-18.7,9.9-18.7,20.7h74.9
			C2587.6,1420.3,2579.4,1411.4,2568.9,1410.4z"/>
		<path fill="#A8CE38" d="M2568.9,1410.4c-0.1-10.2-8.4-18.5-18.7-18.5v39.2h37.4C2587.6,1420.3,2579.4,1411.4,2568.9,1410.4z"/>
	</g>
	<g>
		<path fill="#728E29" d="M2914.6,1218.5c-0.1-9-7.4-16.3-16.5-16.3c-9,0-16.4,7.3-16.5,16.3c-9.3,0.9-16.5,8.7-16.5,18.2h65.9
			C2931.1,1227.3,2923.9,1219.5,2914.6,1218.5z"/>
		<path fill="#698426" d="M2914.6,1218.5c-0.1-9-7.4-16.3-16.5-16.3v34.5h33C2931.1,1227.3,2923.9,1219.5,2914.6,1218.5z"/>
	</g>
	<g>
		<path fill="#728E29" d="M2080.8,1412.8c-0.1-9-7.4-16.3-16.5-16.3c-9,0-16.4,7.3-16.5,16.3c-9.3,0.9-16.5,8.7-16.5,18.2h65.9
			C2097.2,1421.5,2090,1413.7,2080.8,1412.8z"/>
		<path fill="#698426" d="M2080.8,1412.8c-0.1-9-7.4-16.3-16.5-16.3v34.5h33C2097.2,1421.5,2090,1413.7,2080.8,1412.8z"/>
	</g>
	<g>
		<polygon fill="#607150" points="1565.6,1431 1553.4,1431 1555.3,1388.8 1563.6,1388.8 		"/>
		<g>
			<path fill="#B9D432" d="M1605.3,1355.3c0-9.1-7.3-16.4-16.4-16.4h-3.2c4.4-1.8,7.5-6.1,7.5-11.2c0-6.7-5.4-12.1-12.1-12.1h-43.4
				c-6.7,0-12.1,5.4-12.1,12.1c0,5.1,3.1,9.4,7.5,11.2h-3.2c-9.1,0-16.4,7.3-16.4,16.4c0,9.1,7.3,16.4,16.4,16.4h3.6
				c-4.7,1.7-8,6.1-8,11.4c0,6.7,5.4,12.1,12.1,12.1h43.4c6.7,0,12.1-5.4,12.1-12.1c0-5.3-3.4-9.7-8-11.4h3.6
				C1597.9,1371.7,1605.3,1364.4,1605.3,1355.3z"/>
			<path fill="#A8CE38" d="M1605.3,1355.3c0-9.1-7.3-16.4-16.4-16.4h-3.2c4.4-1.8,7.5-6.1,7.5-11.2c0-6.7-5.4-12.1-12.1-12.1h-21.7
				v79.6h21.7c6.7,0,12.1-5.4,12.1-12.1c0-5.3-3.4-9.7-8-11.4h3.6C1597.9,1371.7,1605.3,1364.4,1605.3,1355.3z"/>
		</g>
	</g>
	<g>
		<polygon fill="#607150" points="2827.2,1348.4 2815.4,1348.4 2817.3,1307.7 2825.3,1307.7 		"/>
		<g>
			<path fill="#B9D432" d="M2865.5,1275.5c0-8.7-7.1-15.8-15.8-15.8h-3c4.3-1.7,7.3-5.9,7.3-10.8c0-6.4-5.2-11.7-11.7-11.7h-41.8
				c-6.4,0-11.7,5.2-11.7,11.7c0,4.9,3,9.1,7.3,10.8h-3c-8.7,0-15.8,7.1-15.8,15.8c0,8.7,7.1,15.8,15.8,15.8h3.5
				c-4.5,1.6-7.7,5.9-7.7,11c0,6.4,5.2,11.7,11.7,11.7h41.8c6.4,0,11.7-5.2,11.7-11.7c0-5.1-3.2-9.4-7.7-11h3.5
				C2858.4,1291.3,2865.5,1284.2,2865.5,1275.5z"/>
			<path fill="#A8CE38" d="M2865.5,1275.5c0-8.7-7.1-15.8-15.8-15.8h-3c4.3-1.7,7.3-5.9,7.3-10.8c0-6.4-5.2-11.7-11.7-11.7h-20.9
				v76.7h20.9c6.4,0,11.7-5.2,11.7-11.7c0-5.1-3.2-9.4-7.7-11h3.5C2858.4,1291.3,2865.5,1284.2,2865.5,1275.5z"/>
		</g>
	</g>
	<g>
		<path fill="#96B738" d="M2411.1,1227.4c0-13-10.5-23.5-23.5-23.5c-13,0-23.5,10.5-23.5,23.5v9.5h47V1227.4z"/>
		<path fill="#8EAE35" d="M2411.1,1227.4c0-13-10.5-23.5-23.5-23.5v33h23.5V1227.4z"/>
	</g>
	<g>
		<path fill="#96B738" d="M2460,1227.4c0-13-10.5-23.5-23.5-23.5c-13,0-23.5,10.5-23.5,23.5v9.5h47V1227.4z"/>
		<path fill="#8EAE35" d="M2460,1227.4c0-13-10.5-23.5-23.5-23.5v33h23.5V1227.4z"/>
	</g>
	<g>
		<path fill="#96B738" d="M1736.3,1423c0-11-8.9-20-20-20c-11,0-20,8.9-20,20v8.1h39.9V1423z"/>
		<path fill="#8EAE35" d="M1736.3,1423c0-11-8.9-20-20-20v28.1h20V1423z"/>
	</g>
	<g>
		<path fill="#96B738" d="M1616.7,1228c0-11.4-9.3-20.7-20.7-20.7c-11.4,0-20.7,9.3-20.7,20.7v8.4h41.4V1228z"/>
		<path fill="#8EAE35" d="M1616.7,1228c0-11.4-9.3-20.7-20.7-20.7v29.1h20.7V1228z"/>
	</g>
	<g>
		<path fill="#96B738" d="M3068.9,1227.5c0-12.7-10.3-23.1-23.1-23.1c-12.8,0-23.1,10.3-23.1,23.1v9.4h46.2V1227.5z"/>
		<path fill="#8EAE35" d="M3068.9,1227.5c0-12.7-10.3-23.1-23.1-23.1v32.4h23.1V1227.5z"/>
	</g>
	<g>
		<path fill="#96B738" d="M1517.7,1421.7c0-12.7-10.3-23.1-23.1-23.1c-12.7,0-23.1,10.3-23.1,23.1v9.4h46.2V1421.7z"/>
		<path fill="#8EAE35" d="M1517.7,1421.7c0-12.7-10.3-23.1-23.1-23.1v32.4h23.1V1421.7z"/>
	</g>
	<g>
		<polygon fill="#5A575D" points="1551.5,1236.4 1542.1,1236.4 1543.6,1203.8 1550,1203.8 		"/>
		<g>
			<circle fill="#96B738" cx="1546.8" cy="1177.7" r="30.9"/>
			<path fill="#8EAE35" d="M1546.8,1146.8v61.8c17.1,0,30.9-13.8,30.9-30.9S1563.9,1146.8,1546.8,1146.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="1700.4,1348.4 1691,1348.4 1692.5,1315.8 1698.9,1315.8 		"/>
		<g>
			<circle fill="#96B738" cx="1695.7" cy="1289.7" r="30.9"/>
			<path fill="#8EAE35" d="M1695.7,1258.8v61.8c17.1,0,30.9-13.8,30.9-30.9C1726.6,1272.6,1712.8,1258.8,1695.7,1258.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="2691.8,1348.4 2682.4,1348.4 2683.9,1315.8 2690.4,1315.8 		"/>
		<g>
			<circle fill="#96B738" cx="2687.1" cy="1289.7" r="30.9"/>
			<path fill="#8EAE35" d="M2687.1,1258.8v61.8c17.1,0,30.9-13.8,30.9-30.9C2718.1,1272.6,2704.2,1258.8,2687.1,1258.8z"/>
		</g>
	</g>
	<g>
		<polygon fill="#5A575D" points="3094.9,1236.8 3085.5,1236.8 3087,1204.3 3093.4,1204.3 		"/>
		<g>
			<circle fill="#96B738" cx="3090.2" cy="1178.2" r="30.9"/>
			<path fill="#8EAE35" d="M3090.2,1147.2v61.8c17.1,0,30.9-13.8,30.9-30.9S3107.3,1147.2,3090.2,1147.2z"/>
		</g>
	</g>
</g>
<g id="Satellite">
	<g class="animate-up-n-down">
		<g>
			
				<rect x="2468" y="232.2" transform="matrix(0.7071 0.7071 -0.7071 0.7071 910.505 -1696.9191)" fill="#F1F8F5" width="71.2" height="36.8"/>
			
				<rect x="2470.8" y="225.8" transform="matrix(0.7071 -0.7071 0.7071 0.7071 563.6324 1825.6951)" fill="#8FD3D0" width="29.4" height="13.3"/>
			
				<rect x="2494.6" y="249.1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 553.687 1849.371)" fill="#7FC6C6" width="29.4" height="14.4"/>
			
				<rect x="2482.5" y="237.1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 558.6721 1837.2808)" fill="#8FD3D0" width="29.4" height="14.4"/>
			
				<rect x="2514.3" y="253.5" transform="matrix(0.7071 0.7071 -0.7071 0.7071 928.1205 -1704.2197)" fill="#7FC6C6" width="13.9" height="29.4"/>
		</g>
		<g>
			
				<rect x="2547.2" y="311.4" transform="matrix(0.7071 0.7071 -0.7071 0.7071 989.6613 -1729.7036)" fill="#F1F8F5" width="71.2" height="36.8"/>
			
				<rect x="2550" y="305" transform="matrix(0.7071 -0.7071 0.7071 0.7071 530.8026 1904.8167)" fill="#7FC6C6" width="29.4" height="13.3"/>
			
				<rect x="2573.8" y="328.3" transform="matrix(0.7071 -0.7071 0.7071 0.7071 520.9911 1928.6591)" fill="#7FC6C6" width="29.4" height="14.4"/>
			
				<rect x="2561.7" y="316.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 525.88 1916.4441)" fill="#7FC6C6" width="29.4" height="14.4"/>
			
				<rect x="2593.4" y="332.7" transform="matrix(0.7072 0.7071 -0.7071 0.7072 1007.1287 -1736.8907)" fill="#7FC6C6" width="13.9" height="29.4"/>
		</g>
		<g>
			<path fill="#EDECDC" d="M2540.7,315.6c-2.8,2.8-7.5,2.8-10.3,0l-13-13c-2.8-2.8-2.8-7.4,0-10.3l27.9-27.9c2.8-2.8,7.5-2.8,10.3,0
				l13,13c2.8,2.8,2.8,7.4,0,10.3L2540.7,315.6z"/>
			<path fill="#DFDFD0" d="M2540.7,315.6l27.9-27.9c2.8-2.8,2.8-7.4,0-10.3l-6.5-6.5l-38.2,38.2l6.5,6.5
				C2533.2,318.4,2537.9,318.4,2540.7,315.6z"/>
		</g>
		<g>
			<path fill="#EDECDC" d="M2528.2,324.8c-1.4,1.4-3.6,1.4-5,0l-16.1-16.1c-1.4-1.4-1.4-3.6,0-5l0,0c1.4-1.4,3.6-1.4,5,0l16.1,16.1
				C2529.6,321.2,2529.6,323.4,2528.2,324.8L2528.2,324.8z"/>
			<path fill="#EDECDC" d="M2516.6,315.3c-12-12-31.6-12-43.6,0l43.6,43.6C2528.6,346.9,2528.6,327.4,2516.6,315.3z"/>
			<path fill="#EDECDC" d="M2488.1,347.3c-1,1-2.5,1-3.5,0l0,0c-1-1-1-2.5,0-3.5l21.4-21.4c1-1,2.5-1,3.5,0l0,0c1,1,1,2.5,0,3.5
				L2488.1,347.3z"/>
			<path fill="#DFDFD0" d="M2522.8,324.4l0.4,0.4c1.4,1.4,3.6,1.4,5,0c1.4-1.4,1.4-3.6,0-5l-8-8l-35.6,35.6c1,1,2.5,1,3.5,0l8.4-8.4
				l20,20C2525.9,349.6,2528,335.8,2522.8,324.4z"/>
		</g>
	</g>
</g>
<g id="Solar_x5F_Pannels">
	<g>
		<g>
			<g>
				<rect x="1937.7" y="1506.5" fill="#E3E5D9" width="9.9" height="36.4"/>
				<rect x="1942.7" y="1506.5" fill="#D9DBD0" width="5" height="36.4"/>
			</g>
			<g>
				<rect x="1870.1" y="1431" fill="#E3E5D9" width="145.1" height="77.9"/>
				<rect x="1942.7" y="1431" fill="#D9DBD0" width="72.6" height="77.9"/>
			</g>
			<g>
				<rect x="1873.4" y="1434.3" fill="#E3E5D9" width="138.6" height="71.4"/>
				<rect x="1942.7" y="1434.3" fill="#E3E5D9" width="69.3" height="71.4"/>
			</g>
		</g>
		<g>
			<g>
				<rect x="1873.4" y="1434.3" fill="#8FD3D0" width="43.4" height="20.7"/>
				<rect x="1873.4" y="1458.6" fill="#8FD3D0" width="43.4" height="23.1"/>
				<rect x="1873.4" y="1485.2" fill="#8FD3D0" width="43.4" height="20.4"/>
				<rect x="1969.2" y="1434.3" fill="#8FD3D0" width="42.8" height="20.7"/>
				<rect x="1920.3" y="1434.3" fill="#8FD3D0" width="45.3" height="20.7"/>
				<rect x="1969.2" y="1485.2" fill="#8FD3D0" width="42.8" height="20.4"/>
				<rect x="1969.2" y="1458.6" fill="#8FD3D0" width="42.8" height="23.1"/>
				<rect x="1920.3" y="1485.2" fill="#8FD3D0" width="45.3" height="20.4"/>
				<rect x="1920.3" y="1458.6" fill="#8FD3D0" width="45.3" height="23.1"/>
			</g>
			<g>
				<rect x="1969.2" y="1434.3" fill="#7FC6C6" width="42.8" height="20.7"/>
				<rect x="1942.7" y="1434.3" fill="#7FC6C6" width="23" height="20.7"/>
				<rect x="1969.2" y="1485.2" fill="#7FC6C6" width="42.8" height="20.4"/>
				<rect x="1969.2" y="1458.6" fill="#7FC6C6" width="42.8" height="23.1"/>
				<rect x="1942.7" y="1485.2" fill="#7FC6C6" width="23" height="20.4"/>
				<rect x="1942.7" y="1458.6" fill="#7FC6C6" width="23" height="23.1"/>
			</g>
		</g>
	</g>
	<g>
		<g>
			<g>
				<rect x="1974.6" y="1312" fill="#E3E5D9" width="9.9" height="36.4"/>
				<rect x="1979.5" y="1312" fill="#D9DBD0" width="5" height="36.4"/>
			</g>
			<g>
				<rect x="1907" y="1236.5" fill="#E3E5D9" width="145" height="77.8"/>
				<rect x="1979.5" y="1236.5" fill="#D9DBD0" width="72.5" height="77.8"/>
			</g>
			<g>
				<rect x="1910.3" y="1239.8" fill="#E3E5D9" width="138.5" height="71.3"/>
				<rect x="1979.5" y="1239.8" fill="#E3E5D9" width="69.3" height="71.3"/>
			</g>
		</g>
		<g>
			<g>
				<rect x="1910.3" y="1239.8" fill="#8FD3D0" width="43.4" height="20.7"/>
				<rect x="1910.3" y="1264.1" fill="#8FD3D0" width="43.4" height="23.1"/>
				<rect x="1910.3" y="1290.7" fill="#8FD3D0" width="43.4" height="20.4"/>
				<rect x="2006" y="1239.8" fill="#8FD3D0" width="42.8" height="20.7"/>
				<rect x="1957.2" y="1239.8" fill="#8FD3D0" width="45.3" height="20.7"/>
				<rect x="2006" y="1290.7" fill="#8FD3D0" width="42.8" height="20.4"/>
				<rect x="2006" y="1264.1" fill="#8FD3D0" width="42.8" height="23.1"/>
				<rect x="1957.2" y="1290.7" fill="#8FD3D0" width="45.3" height="20.4"/>
				<rect x="1957.2" y="1264.1" fill="#8FD3D0" width="45.3" height="23.1"/>
			</g>
			<g>
				<rect x="2006" y="1239.8" fill="#7FC6C6" width="42.8" height="20.7"/>
				<rect x="1979.5" y="1239.8" fill="#7FC6C6" width="22.9" height="20.7"/>
				<rect x="2006" y="1290.7" fill="#7FC6C6" width="42.8" height="20.4"/>
				<rect x="2006" y="1264.1" fill="#7FC6C6" width="42.8" height="23.1"/>
				<rect x="1979.5" y="1290.7" fill="#7FC6C6" width="22.9" height="20.4"/>
				<rect x="1979.5" y="1264.1" fill="#7FC6C6" width="22.9" height="23.1"/>
			</g>
		</g>
	</g>
	<g>
		<g>
			<g>
				<rect x="1820.9" y="1396.2" fill="#E3E5D9" width="9.5" height="34.9"/>
				<rect x="1825.6" y="1396.2" fill="#D9DBD0" width="4.7" height="34.9"/>
			</g>
			<g>
				<rect x="1756.1" y="1323.9" fill="#E3E5D9" width="139" height="74.6"/>
				<rect x="1825.6" y="1323.9" fill="#D9DBD0" width="69.5" height="74.6"/>
			</g>
			<g>
				<rect x="1759.3" y="1327.1" fill="#E3E5D9" width="132.8" height="68.4"/>
				<rect x="1825.6" y="1327.1" fill="#E3E5D9" width="66.4" height="68.4"/>
			</g>
		</g>
		<g>
			<g>
				<rect x="1759.3" y="1327.1" fill="#8FD3D0" width="41.6" height="19.9"/>
				<rect x="1759.3" y="1350.3" fill="#8FD3D0" width="41.6" height="22.1"/>
				<rect x="1759.3" y="1375.9" fill="#8FD3D0" width="41.6" height="19.6"/>
				<rect x="1851" y="1327.1" fill="#8FD3D0" width="41" height="19.9"/>
				<rect x="1804.2" y="1327.1" fill="#8FD3D0" width="43.4" height="19.9"/>
				<rect x="1851" y="1375.9" fill="#8FD3D0" width="41" height="19.6"/>
				<rect x="1851" y="1350.3" fill="#8FD3D0" width="41" height="22.1"/>
				<rect x="1804.2" y="1375.9" fill="#8FD3D0" width="43.4" height="19.6"/>
				<rect x="1804.2" y="1350.3" fill="#8FD3D0" width="43.4" height="22.1"/>
			</g>
			<g>
				<rect x="1851" y="1327.1" fill="#7FC6C6" width="41" height="19.9"/>
				<rect x="1825.6" y="1327.1" fill="#7FC6C6" width="22" height="19.9"/>
				<rect x="1851" y="1375.9" fill="#7FC6C6" width="41" height="19.6"/>
				<rect x="1851" y="1350.3" fill="#7FC6C6" width="41" height="22.1"/>
				<rect x="1825.6" y="1375.9" fill="#7FC6C6" width="22" height="19.6"/>
				<rect x="1825.6" y="1350.3" fill="#7FC6C6" width="22" height="22.1"/>
			</g>
		</g>
	</g>
</g>
<g id="Clouds_x5F_With_x5F_Shadows">
	<g class="auto-1-cloud">
		<g>
			<path fill="#FFFDF2" d="M2039.8,575.5c-3.4,0-6.1,2.7-6.1,6.1c0,3.4,2.7,6.1,6,6.1c0,0,0,0,0,0c0.7,0,1.3,0.6,1.3,1.4
				c0,0.7-0.5,1.2-1.1,1.3h-24.3c-3.4,0-6.1,2.7-6.1,6.1c0,3.4,2.7,6.1,6.1,6.1h6.8c0.6,0.1,1.1,0.7,1.1,1.3c0,0.7-0.6,1.4-1.3,1.4
				c0,0,0,0,0,0h0.1c-3.3,0-6,2.8-6,6.1c0,3.4,2.7,6.1,6.1,6.1h29.9v-42.2H2039.8z"/>
			<path fill="#F6F5EE" d="M2085.8,590.5h-9.8c-0.6-0.1-1.1-0.7-1.1-1.3c0-0.7,0.6-1.4,1.3-1.4c0,0,0,0,0,0h-0.1c3.3,0,6-2.8,6-6.1
				c0-3.4-2.7-6.1-6.1-6.1h-23.7v42.2h14.7c3.4,0,6.1-2.7,6.1-6.1c0-3.4-2.7-6.1-6.1-6.1h-6.8c-0.7,0-1.3-0.6-1.3-1.4
				c0-0.7,0.5-1.2,1.1-1.3h25.8c3.4,0,6.1-2.7,6.1-6.1C2091.9,593.2,2089.2,590.5,2085.8,590.5z"/>
		</g>
	</g>
	<g class="auto-1-cloud">
		<g>
			<path fill="#FFFDF2" d="M2864,566.5c-3.4,0-6.1,2.7-6.1,6.1c0,3.4,2.7,6.1,6,6.1c0,0,0,0,0,0c0.7,0,1.3,0.6,1.3,1.4
				c0,0.7-0.5,1.2-1.1,1.3h-24.3c-3.4,0-6.1,2.7-6.1,6.1c0,3.4,2.7,6.1,6.1,6.1h6.8c0.6,0.1,1.1,0.7,1.1,1.3c0,0.7-0.6,1.4-1.3,1.4
				c0,0,0,0,0,0h0.1c-3.3,0-6,2.8-6,6.1c0,3.4,2.7,6.1,6.1,6.1h29.9v-42.2H2864z"/>
			<path fill="#F6F5EE" d="M2910,581.5h-9.8c-0.6-0.1-1.1-0.7-1.1-1.3c0-0.7,0.6-1.4,1.3-1.4c0,0,0,0,0,0h-0.1c3.3,0,6-2.8,6-6.1
				c0-3.4-2.7-6.1-6.1-6.1h-23.7v42.2h14.7c3.4,0,6.1-2.7,6.1-6.1c0-3.4-2.7-6.1-6.1-6.1h-6.8c-0.7,0-1.3-0.6-1.3-1.4
				c0-0.7,0.5-1.2,1.1-1.3h25.8c3.4,0,6.1-2.7,6.1-6.1C2916.2,584.3,2913.4,581.5,2910,581.5z"/>
		</g>
	</g>
	<g class="auto-1-cloud">
		<g>
			<path fill="#FFFDF2" d="M1824.7,383.2c-6.3,0-11.4,5.1-11.4,11.4c0,6.2,5,11.3,11.2,11.4c1.4,0,2.5,1.2,2.5,2.5
				c0,1.2-0.9,2.2-2,2.5h-42.3c-6.3,0-11.4,5.1-11.4,11.4c0,6.3,5.1,11.4,11.4,11.4h64.4v-50.6H1824.7z"/>
			<path fill="#F6F5EE" d="M1886,411h-18.3c-1.2-0.2-2-1.3-2-2.5c0-1.4,1.1-2.5,2.5-2.6c0,0-0.1,0-0.1,0h0
				c6.1-0.2,11.1-5.2,11.1-11.4c0-6.3-5.1-11.4-11.4-11.4H1847v50.6h39c6.3,0,11.4-5.1,11.4-11.4C1897.4,416.1,1892.3,411,1886,411z
				"/>
		</g>
	</g>
	<g class="auto-1-cloud">
		<g>
			<path fill="#FFFDF2" d="M2464.2,473.3c-6.3-0.1-11.4,5-11.5,11.3c-0.1,6.2,4.9,11.3,11,11.5c1.4,0.1,2.5,1.2,2.4,2.6
				c0,1.2-0.9,2.2-2,2.5l-42.3-0.5c-6.3-0.1-11.4,5-11.5,11.3c-0.1,6.3,5,11.4,11.3,11.5l64.4,0.7l0.6-50.6L2464.2,473.3z"/>
			<path fill="#F6F5EE" d="M2525.2,501.8l-18.3-0.2c-1.2-0.3-2-1.3-2-2.5c0-1.4,1.1-2.5,2.5-2.5c0,0,0,0-0.1,0h0
				c6.1-0.1,11.1-5.1,11.2-11.3c0.1-6.3-5-11.4-11.3-11.5l-20.8-0.2l-0.6,50.6l39,0.4c6.3,0.1,11.4-5,11.5-11.3
				C2536.5,507,2531.5,501.8,2525.2,501.8z"/>
		</g>
	</g>
	<g class="auto-1-cloud">
		<g>
			<path fill="#F6F5EE" d="M1689.7,530.5c5.9,0,10.7,4.8,10.7,10.7c0,5.9-4.7,10.6-10.5,10.7c0,0,0,0,0.1,0c-1.3,0-2.3,1.1-2.3,2.4
				c0,1.2,0.8,2.1,1.9,2.3h44.4c5.9,0,10.7,4.8,10.7,10.7c0,5.9-4.8,10.7-10.7,10.7h-22.1c-1.1,0.2-1.9,1.2-1.9,2.3
				c0,1.3,1,2.4,2.3,2.4c0,0,0,0-0.1,0h-0.2c5.8,0.1,10.5,4.8,10.5,10.7c0,5.9-4.8,10.7-10.7,10.7h-51.1v-73.6H1689.7z"/>
			<path fill="#FFFDF2" d="M1611.6,556.6h17.1c1.1-0.2,1.9-1.2,1.9-2.3c0-1.3-1-2.4-2.3-2.4c0,0,0,0,0.1,0h0.2
				c-5.8-0.1-10.5-4.8-10.5-10.7c0-5.9,4.8-10.7,10.7-10.7h32V604h-36.4c-5.9,0-10.7-4.8-10.7-10.7c0-5.9,4.8-10.7,10.7-10.7h11.8
				c1.3-0.1,2.3-1.1,2.3-2.4c0-1.2-0.8-2.1-1.9-2.3h-24.9c-5.9,0-10.7-4.8-10.7-10.7C1600.9,561.3,1605.7,556.6,1611.6,556.6z"/>
		</g>
	</g>
	<g class="auto-1-cloud">
		<g>
			<path fill="#FFFDF2" d="M2142.1,213.7c-5.9,0-10.7,4.8-10.7,10.7c0,5.9,4.7,10.6,10.5,10.7c0,0,0,0-0.1,0c1.3,0,2.3,1.1,2.3,2.4
				c0,1.2-0.8,2.1-1.9,2.3H2098c-5.9,0-10.7,4.8-10.7,10.7c0,5.9,4.8,10.7,10.7,10.7h22.1c1.1,0.2,1.9,1.2,1.9,2.3
				c0,1.3-1,2.4-2.3,2.4c0,0,0,0,0.1,0h0.2c-5.8,0.1-10.5,4.8-10.5,10.7c0,5.9,4.8,10.7,10.7,10.7h51.1v-73.6H2142.1z"/>
			<path fill="#F6F5EE" d="M2220.3,239.8h-17.1c-1.1-0.2-1.9-1.2-1.9-2.3c0-1.3,1-2.4,2.3-2.4c0,0,0,0-0.1,0h-0.2
				c5.8-0.1,10.5-4.8,10.5-10.7c0-5.9-4.8-10.7-10.7-10.7h-32v73.6h36.4c5.9,0,10.7-4.8,10.7-10.7c0-5.9-4.8-10.7-10.7-10.7h-11.8
				c-1.3-0.1-2.3-1.1-2.3-2.4c0-1.2,0.8-2.1,1.9-2.3h24.9c5.9,0,10.7-4.8,10.7-10.7C2231,244.6,2226.2,239.8,2220.3,239.8z"/>
		</g>
	</g>
</g>
<g id="Wind_x5F_Turbines">
	<g>
		<polygon fill="#EEEBD0" points="1584.8,787.5 1586.1,787.5 1586.6,787.5 1587.9,787.5 1591.9,1124.9 1586.6,1124.9 1586.1,1124.9 
			1580.8,1124.9 		"/>
		<g class="auto-1-turbine-3">
			<path fill="#EEEBD0" d="M1589.2,786.7l79.6,5.8c0,0,0.3,3.2-2.8,3.1c-5.6,0-69,0.4-69,0.4l-4-5.9l-4.3-0.7L1589.2,786.7z"/>
			<path fill="#EEEBD0" d="M1583.5,793.1l-45.4,65.6c0,0-2.9-1.3-1.3-4c2.9-4.8,34.7-59.6,34.7-59.6l7.1-0.4l2.8-3.3L1583.5,793.1z"
				/>
			<path fill="#EEEBD0" d="M1581.1,785.3l-35-71.7c0,0,2.6-1.9,4.1,0.8c2.8,4.9,35,59.5,35,59.5l-3.1,6.4l1.5,4.1L1581.1,785.3z"/>
			<path fill="#EEEBD0" d="M1591.1,788.2c0-3.4-2.7-6.2-6.1-6.2c-3.4,0-6.2,2.7-6.2,6.1c0,3.4,2.7,6.2,6.1,6.2
				C1588.3,794.3,1591.1,791.6,1591.1,788.2z"/>
		</g>
	</g>
	<g>
		<polygon fill="#EEEBD0" points="1452.3,848 1451,848 1450.6,848 1449.3,848 1445.3,1124.9 1450.6,1124.9 1451,1124.9 
			1456.3,1124.9 		"/>
		<g class="auto-1-turbine-1">
			<path fill="#EEEBD0" d="M1448,847.2l-78.1,5.6c0,0-0.3,3.1,2.7,3.1c5.5,0,67.7,0.4,67.7,0.4l3.9-5.8l4.2-0.7L1448,847.2z"/>
			<path fill="#EEEBD0" d="M1453.6,853.4l44.5,64.4c0,0,2.8-1.3,1.3-3.9c-2.8-4.7-34.1-58.5-34.1-58.5l-7-0.4l-2.8-3.3L1453.6,853.4
				z"/>
			<path fill="#EEEBD0" d="M1455.9,845.8l34.3-70.4c0,0-2.5-1.8-4,0.8c-2.7,4.8-34.3,58.4-34.3,58.4l3.1,6.3l-1.5,4L1455.9,845.8z"
				/>
			<path fill="#EEEBD0" d="M1446.1,848.7c0-3.3,2.6-6,6-6.1c3.3,0,6,2.7,6.1,6c0,3.3-2.7,6-6,6.1
				C1448.8,854.7,1446.1,852,1446.1,848.7z"/>
		</g>
	</g>
	<g>
		<polygon fill="#EDECDC" points="1522.7,679.1 1520.9,679.1 1520.3,679.1 1518.6,679.1 1513.3,1124.9 1520.3,1124.9 1520.9,1124.9 
			1527.9,1124.9 		"/>
		<g class="auto-1-turbine-2">
			<path fill="#EDECDC" d="M1525.8,675.8l104.8-6.8c0,0,0.5-4.2-3.6-4.2c-7.4,0-90.8-1.1-90.8-1.1l-5.3,7.8l-5.7,0.9L1525.8,675.8z"
				/>
			<path fill="#EDECDC" d="M1518.4,667.4l-59.1-86.8c0,0-3.8,1.7-1.8,5.2c3.8,6.4,45.2,78.8,45.2,78.8l9.4,0.6l3.7,4.4L1518.4,667.4
				z"/>
			<path fill="#EDECDC" d="M1515.2,677.6l-46.7,94.1c0,0,3.4,2.5,5.4-1c3.7-6.4,46.6-78,46.6-78l-4-8.5l2-5.4L1515.2,677.6z"/>
			<circle fill="#EDECDC" cx="1520.3" cy="673.9" r="8.1"/>
		</g>
	</g>
	<g class="">
		<g>
			<path fill="#8EAE35" d="M2694.5,644.3l11.2-11.3l0,0c0.2-0.2,0.4-0.3,0.7-0.3h6.1c0.5,0,0.9,0.4,0.9,0.9c0,0.1,0,0.3-0.1,0.4
				l-5.3,10.4H2694.5z"/>
			<g>
				<g>
					<path fill="#8EAE35" d="M2634.9,633c0,1,0.8,1.8,1.8,1.8h58.6c1,0,1.8-0.8,1.8-1.8l0,0c0-1-0.8-1.7-1.8-1.7h-58.6
						C2635.7,631.2,2634.9,632,2634.9,633L2634.9,633z"/>
					<path fill="#8EAE35" d="M2664.7,638.8c0,0.7,0.6,1.3,1.3,1.3l0,0c0.7,0,1.3-0.6,1.3-1.3v-9c0-0.7-0.6-1.3-1.3-1.3l0,0
						c-0.7,0-1.3,0.6-1.3,1.3V638.8z"/>
				</g>
				<path fill="#8EAE35" d="M2657.5,642.8c0-2.8,2.3-5.1,5.1-5.1h6.7c2.8,0,5.1,2.3,5.1,5.1c0,0.1,0,0.2,0,0.4h-16.8
					C2657.5,643,2657.5,642.9,2657.5,642.8z"/>
			</g>
			<g>
				<polygon fill="#8EAE35" points="2667.6,649.8 2670.3,649.8 2670.3,653.1 2673.5,653.1 2673.5,655.7 2670.3,655.7 2670.3,659 
					2667.6,659 2667.6,655.7 2664.4,655.7 2664.4,653.1 2667.6,653.1 				"/>
				<path fill="#8EAE35" d="M2630.4,659.3c0,8.7,13.6,9.6,13.6,9.6s16.1,0,24.8,0c8.7,0,16.5-13.4,19.1-15.1
					c2.6-1.6,16.4-3.1,16.4-3.1l0,0h0.4c2.3,0,4.3-1.9,4.3-4.3c0-2.4-1.9-4.3-4.3-4.3h-47.6C2642.2,642.2,2630.4,650.6,2630.4,659.3
					z M2703,646.4c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6c0,0.9-0.7,1.6-1.6,1.6C2703.7,648,2703,647.3,2703,646.4z
					 M2661.5,654.2c0-4.1,3.3-7.5,7.5-7.5c4.1,0,7.5,3.3,7.5,7.5c0,4.1-3.3,7.5-7.5,7.5C2664.8,661.7,2661.5,658.4,2661.5,654.2z
					 M2633.7,658.8c0.5-6.2,9.8-12.9,22.2-13.3v13.3H2633.7z"/>
			</g>
			<g>
				
					<rect x="2659.5" y="665.1" transform="matrix(-0.7337 0.6795 -0.6795 -0.7337 5068.1548 -645.889)" fill="#8EAE35" width="2.3" height="10.1"/>
				
					<rect x="2649.7" y="665.1" transform="matrix(-0.7339 0.6792 -0.6792 -0.7339 5051.4883 -638.4811)" fill="#8EAE35" width="2.3" height="10.1"/>
				<path fill="#8EAE35" d="M2635,674.4c0,0.8,0.7,1.5,1.5,1.5h43.9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-43.9
					C2635.7,672.9,2635,673.6,2635,674.4L2635,674.4z"/>
			</g>
		</g>
	</g>
</g>
</svg>

	  	</div>
	  </section>



	  <section class="section auto-2">
        

				<div class="slide">
					<div class="inner-wrap">
						<h3>Lightweight Materials</h3>
			  		<p>Efforts to enhance energy efficiency and sustainability have impelled auto manufacturers to begin using lightweight materials such as aluminum, magnesium, and carbon fiber in lieu of traditionally heavier steel panels. According to the Department of Energy, making the switch to lighter alternatives can reduce a car’s weight by up to 10% and boost the fuel economy by 6-8%.</p>
			  		<div class="blue-truck-wrap">
				  		<img src="<?php bloginfo('template_url'); ?>/img/auto/blue-truck.svg" alt="blue truck">
				  		<div class="car-exhaust">
								  <span class="s0"></span>
								  <span class="s1"></span>
								  <span class="s2"></span>
								  <span class="s3"></span>
								  <span class="s4"></span>
								  <span class="s5"></span>
								  <span class="s6"></span>
								  <span class="s7"></span>
								  <span class="s8"></span>
								  <span class="s9"></span>
								</div>
							</div>
			  	</div>
		  	</div>
		  
				<div class="slide">
					<div class="inner-wrap">
						<h3>Wireless Connectivity</h3>
						<p>As for connectivity, more and more vehicles are being equipped with wireless access for integration with users’ other devices. In the past year alone, about 20 million connected cars shipped to dealerships — and about 380 million are projected to be on the road by 2021.</p> 
			  		<p>These shifting design trends are not only changing cars’ exteriors but also the types of parts used in interiors and bodies. To capture the potential of this business opportunity, automakers must make adjustments to their regulations, part designs, and manufacturing processes, as well as develop better relationships with suppliers.</p>
						  		
						<div class="auto-2b">

						<svg version="1.1" id="_x2014_ÎÓÈ_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
							 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 410.1 227.8"
							 style="enable-background:new 0 0 410.1 227.8;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#A9F2BB;}
							.st1{fill:#FFF500;}
							.st2{fill:#EBFBFF;}
							.st3{fill:#2CC921;}
							.st4{fill:#FFFFFF;}
							.st5{fill:#7FC652;}
							.st6{fill:#CAE4EB;}
							.st7{fill:#E71300;}
							.st8{fill:#5F953E;}
							.st9{fill:#CAEAF8;}
							.st10{fill:#333333;}
							.st11{fill:#B3B3B3;}
							.st12{fill:#6939B5;}
							.st13{fill:#2C75BA;}
							.st14{fill:#F1F8F5;}
							.st15{fill:#8FD3D0;}
							.st16{fill:#7FC6C6;}
							.st17{fill:#EDECDC;}
							.st18{fill:#DFDFD0;}
							.st19{fill:#E4E7E7;}
							.st20{fill:#324D5B;}
							.st21{fill:#576E78;}
							.st22{fill:#B6B9B9;}
						</style>
						<switch>
							<foreignObject requiredExtensions="&ns_ai;" x="0" y="0" width="1" height="1">
								<i:pgfRef  xlink:href="#adobe_illustrator_pgf">
								</i:pgfRef>
							</foreignObject>
							<g i:extraneous="self">
								<g>
									<path d="M51.1,55.7h0.8c0.5,0,0.8-0.4,0.8-0.8v-2c0-0.5-0.4-0.8-0.8-0.8h-0.8c-0.5,0-0.8,0.4-0.8,0.8v2
										C50.3,55.4,50.7,55.7,51.1,55.7z"/>
									<path class="st0" d="M52,193.6h43.4V45.7c0-4.9-4-8.9-8.9-8.9H60.9c-4.9,0-8.9,4-8.9,8.9V193.6z"/>
									<polygon class="st1" points="65.1,91 76.3,96 66.6,73.7 78.2,73.8 82.3,102.9 72.5,99.9 75.9,120.1 75.9,120.1 78,116.5 
										76.9,123.7 71.3,119.1 75,119.9 			"/>
									<rect x="57.5" y="49.5" class="st2" width="33.3" height="15"/>
									<g>
										<rect x="61.2" y="53.9" class="st3" width="27.3" height="6.2"/>
										<rect x="68.1" y="54.7" class="st4" width="19.4" height="4.6"/>
										<rect x="59.8" y="55.2" class="st3" width="1.4" height="3.6"/>
									</g>
									<g>
										<path class="st5" d="M344.2,162.5c12.5,9.2,10.3,39.3-2.1,39.5c-78.2,0-224.5-0.4-276.5,0c-18-0.2-31.1-15.9-31.9-23.4
											c0.4,3.5,4.8-30,7-39.8c0.8-3.4,25.8-1.8,29.3-3.8c2.6-1.5,12.9-7.2,32.6-21.2c16.6-11.8,96.7-9,114.1-0.7
											c24.7,13.5,31.1,18.9,55.2,30.2C283.2,145.3,320.6,146.4,344.2,162.5z"/>
										<path class="st6" d="M71.5,133.9c0.3-0.1,8.6-5.6,28.7-18.7c0,0-0.4,1.1-1.2,1.8l-26.7,18.3L71.5,133.9z"/>
										<path class="st7" d="M39,188.8c-3.1-3.6-5-7.2-5.3-9.9c0.2,0.8,0.9-3.2,1.7-9h7.2v18.9H39z"/>
										<path class="st8" d="M93.3,181.8v20c-10.9,0-20.3,0.1-27.7,0.1c-15.6-0.2-27.5-12-30.9-20.1H93.3z"/>
										<path class="st8" d="M297.1,188.3h55c-1.1,7.5-4.6,13.5-10.1,13.6c-13.3,0-28.5,0-45,0V188.3z"/>
										<path class="st8" d="M262.7,200.4v-41.9c0-7.8-19.7-23.5-33.4-32.4c-9.4-5.9-30.9-13.6-54-13.8c0,0-4.6-0.1-4.6,4.6v83.5H262.7z
											"/>
										<path class="st5" d="M171.6,199.6l90.2-0.1c0,0,0-30.6,0-41.1c0-6.3-16.2-21-32.4-31.6c-9.7-5.4-27.4-13.1-53.1-13.9
											c-3.8,0-4.8,1.6-4.8,3.7L171.6,199.6z"/>
										<path class="st8" d="M165.4,200.4c0,0,0-83.4,0-83.6c0-1,0.2-2.1-0.6-2.9c-0.6-0.7-1.6-0.9-3.3-0.9c-0.4,0-0.9,0-1.3,0
											c-19.6,0-37.9,4.8-51.6,13.5c-11.5,7.3-19.1,16.8-19.4,24.9c0,0.4-0.2,22.4-0.2,22.4s11.1-0.2,13.3-0.1
											c5.8,0.1,12.5,3.7,17.1,8.5c7.3,7.6,7.4,17.4,7.4,17.5l0,0.8h0.7H165.4z"/>
										<path class="st5" d="M164.6,199.6h-37.4c0,0-0.1-9.9-7.6-17.8c-4.9-5.1-11.7-8.4-17.6-8.6c-5.9-0.2-8.8,0.1-12.2,0.1
											c0-14.7,0.1-20,0.1-21.8c0.5-14.7,26.7-38.1,71.3-37.7c3.9,0,3.4,1.7,3.4,3.8V199.6z"/>
										<path class="st9" d="M158.7,157.8v-38.6c-39,0.3-64,22.1-64,38.5C94.7,158.3,158.7,157.8,158.7,157.8z"/>
										<path class="st9" d="M177.4,157.8v-38.6l8.1-0.1c19.4,2.4,38.1,11.7,40.4,13c1.5,1,3.2,2.1,5,3.3c7.7,5,23.4,15.2,24.7,22.4
											H177.4z"/>
										<path class="st9" d="M269.3,142l-1.6,1.3c-1.6,0.7-49.1-25.9-47.8-28.5C224.5,116.9,248.9,131.3,269.3,142z"/>
										<path class="st8" d="M314,201.7c-0.5-11.8-10.2-21.3-22.2-21.3c-11.9,0-21.7,9.5-22.2,21.3h-5.5c0.5-14.9,12.7-26.8,27.7-26.8
											s27.2,11.9,27.7,26.8H314z"/>
										<path class="st8" d="M119.8,201.7c-0.5-11.8-10.2-21.3-22.2-21.3c-11.9,0-21.7,9.5-22.2,21.3H70c0.5-14.9,12.7-26.8,27.7-26.8
											c15,0,27.2,11.9,27.7,26.8H119.8z"/>
										<circle class="st10" cx="291.8" cy="202.4" r="22.7"/>
										<circle cx="291.8" cy="202.4" r="14.8"/>
										<circle class="st10" cx="97.7" cy="202.4" r="22.7"/>
										<circle cx="97.7" cy="202.4" r="14.8"/>
										<circle class="st11" cx="97.7" cy="202.4" r="9.7"/>
										<circle class="st11" cx="291.8" cy="202.4" r="9.7"/>
										<path class="st9" d="M334.9,172.9c5.4,3.1,14.3,10.6,15.3,7.2c1.1-3.3-5.1-10.9-8.5-12.9c-6.8-4.1-15.7-5.9-16.8-2.6
											C324,168,328.1,168.9,334.9,172.9z"/>
										<path class="st8" d="M178.5,172.1h7.5c0.6,0,1.1-0.5,1.1-1.1v-2.3c0-0.6-0.5-1.1-1.1-1.1h-7.5c-0.6,0-1.1,0.5-1.1,1.1v2.3
											C177.4,171.6,177.9,172.1,178.5,172.1z"/>
										<path class="st8" d="M108.7,172.1h7.5c0.6,0,1.1-0.5,1.1-1.1v-2.3c0-0.6-0.5-1.1-1.1-1.1h-7.5c-0.6,0-1.1,0.5-1.1,1.1v2.3
											C107.6,171.6,108.1,172.1,108.7,172.1z"/>
										<g>
											<g>
												<path class="st8" d="M59.1,170.1h1.9c1,0,1.9-0.8,1.9-1.9v-2.4h-5.6v2.4C57.2,169.3,58,170.1,59.1,170.1z"/>
												<g>
													<rect x="61" y="164.2" class="st8" width="0.7" height="2.3"/>
													<rect x="58.2" y="164.2" class="st8" width="0.7" height="2.4"/>
												</g>
											</g>
											<g>
												<path class="st8" d="M57.2,176.4c0.2,0,1.2-0.2,2.4-1.7c1.3-1.6,0.8-5.1,0.8-5.2c0-0.3-0.3-0.5-0.6-0.5
													c-0.3,0-0.5,0.3-0.4,0.5c0.1,0.9,0.3,3.3-0.6,4.4c-1,1.2-1.7,1.2-1.7,1.2c-0.3,0-0.5,0.3-0.5,0.6
													C56.7,176,56.9,176.4,57.2,176.4C57.2,176.4,57.2,176.4,57.2,176.4z"/>
											</g>
										</g>
									</g>
									<g>
										<circle cx="69.1" cy="169.2" r="3"/>
										<polygon points="68.1,179.8 69.4,179.3 70.1,168.2 67.5,168.2 				"/>
										<g>
											<path d="M36.5,216.2c1.7,0,3.3-0.3,4.7-0.9c2.3-1,4.1-2.9,5.7-4.6l15.6-16.3c7.2-7.5,7.1-14.4,7.1-23.4c0-1,0-1.6,0-3.6H68
												c0,2,0,2.6,0,3.6c0.1,8.9,0.1,15-6.6,22.1l-15.6,16.4c-1.5,1.6-3.2,3.4-5.3,4.3c-2.6,1.1-5.8,1.1-9.9-0.2
												c-10.6-3.4-22.3-13.3-24.1-25.8c-1-6.5,1-11.3,3.9-17.5c1.1-2.4,4.1-4,7.4-5.8c4.8-2.6,10.7-5.7,14.6-12.9
												c8.2-15.1,8.8-32,9.3-48.3c0.1-3.7,0-7.5,0.2-11.3c0.1-1-0.3-35.5,8.7-37.1v-1.6c-10,1.3-9.9,37.7-10,38.6
												c-0.2,3.8-0.3,7.6-0.4,11.3c-0.5,16.9-1,32.9-9.1,47.7c-3.7,6.8-9.2,9.7-14,12.3c-3.6,1.9-6.7,3.6-8.1,6.5
												c-2.9,6.2-5.1,11.5-4,18.3C7,201.1,19.2,211.5,30.2,215C32.5,215.8,34.6,216.2,36.5,216.2z"/>
										</g>
									</g>
								</g>
								<path class="st12 animate-floating" d="M168,96.4c0,0-9.7-15.1-9.7-23.5c0-5.3,4.3-9.7,9.7-9.7c5.3,0,9.7,4.3,9.7,9.7C177.7,81.2,168,96.4,168,96.4
									z M168,76.2c1.8,0,3.2-1.4,3.2-3.2s-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2S166.3,76.2,168,76.2z"/>
								<g>
									<path class="st13 animate-3d-print-3" d="M288.1,91.1l0.6-2.8c11.3,0.8,21.8,11.5,20.9,24.6l-2.9,0.1C306.3,100.7,300.2,93.5,288.1,91.1z"/>
									<path class="st13 animate-3d-print-2" d="M301.9,113.2l-3,0.1c1-7.8-5-14.3-12.4-14.6l0.6-3C295.8,96.1,303,104.2,301.9,113.2z"/>
									<path class="st13 animate-3d-print-1" d="M285,106.1l0.5-2.6c5.2-0.6,10,4.4,8.6,10.1l-2.7,0.1c0.4-2,0-3.9-1.3-5.4
										C288.8,106.7,287.1,106.1,285,106.1z"/>
								</g>
								<g>
									<path class="st13 animate-3d-print-3" d="M335,86.3l-0.4,2.1c-8.5-0.6-16.3-8.6-15.7-18.4l2.2-0.1C321.3,79.1,325.9,84.5,335,86.3z"/>
									<path class="st13 animate-3d-print-2" d="M324.6,69.7l2.3-0.1c-0.7,5.8,3.7,10.7,9.3,10.9l-0.5,2.2C329.2,82.6,323.8,76.4,324.6,69.7z"/>
									<path class="st13 animate-3d-print-1" d="M337.3,75l-0.4,2c-3.9,0.5-7.5-3.3-6.4-7.6l2-0.1c-0.3,1.5,0,2.9,1,4C334.5,74.6,335.8,75.1,337.3,75z"/>
								</g>
								<g>
									<g>
										
											<rect x="329.4" y="12.5" transform="matrix(0.7071 0.7071 -0.7071 0.7071 116.935 -239.0431)" class="st14" width="35.3" height="18.3"/>
										
											<rect x="330.7" y="9.3" transform="matrix(0.7071 -0.7071 0.7071 0.7071 90.0734 242.7384)" class="st15" width="14.6" height="6.6"/>
										
											<rect x="342.5" y="20.9" transform="matrix(0.7071 -0.7071 0.7071 0.7071 85.1706 254.5276)" class="st16" width="14.6" height="7.1"/>
										
											<rect x="336.6" y="14.9" transform="matrix(0.7071 -0.7071 0.7071 0.7071 87.6497 248.535)" class="st15" width="14.6" height="7.1"/>
										
											<rect x="352.3" y="23.1" transform="matrix(0.7071 0.7071 -0.7071 0.7071 125.6726 -242.663)" class="st16" width="6.9" height="14.6"/>
									</g>
									<g>
										
											<rect x="368.6" y="51.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 156.2172 -255.3141)" class="st14" width="35.3" height="18.3"/>
										
											<rect x="370" y="48.6" transform="matrix(0.7071 -0.7071 0.7071 0.7071 73.7961 282.0169)" class="st16" width="14.6" height="6.6"/>
										
											<rect x="381.8" y="60.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 68.9127 293.8292)" class="st16" width="14.6" height="7.1"/>
										
											<rect x="375.8" y="54.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 71.3777 287.8172)" class="st16" width="14.6" height="7.1"/>
										
											<rect x="391.6" y="62.4" transform="matrix(0.7072 0.7071 -0.7071 0.7072 164.9315 -258.9175)" class="st16" width="6.9" height="14.6"/>
									</g>
									<g>
										<path class="st17" d="M365.4,53.9c-1.4,1.4-3.7,1.4-5.1,0l-6.5-6.5c-1.4-1.4-1.4-3.7,0-5.1l13.8-13.8c1.4-1.4,3.7-1.4,5.1,0
											l6.5,6.5c1.4,1.4,1.4,3.7,0,5.1L365.4,53.9z"/>
										<path class="st18" d="M365.4,53.9L379.3,40c1.4-1.4,1.4-3.7,0-5.1l-3.2-3.2l-19,19l3.2,3.2C361.7,55.3,364,55.3,365.4,53.9z"/>
									</g>
									<g>
										<path class="st17" d="M359.2,58.4c-0.7,0.7-1.8,0.7-2.5,0l-8-8c-0.7-0.7-0.7-1.8,0-2.5l0,0c0.7-0.7,1.8-0.7,2.5,0l8,8
											C359.9,56.6,359.9,57.7,359.2,58.4L359.2,58.4z"/>
										<path class="st17" d="M353.4,53.7c-6-6-15.7-6-21.6,0l21.6,21.6C359.4,69.4,359.4,59.7,353.4,53.7z"/>
										<path class="st17" d="M339.3,69.6c-0.5,0.5-1.3,0.5-1.7,0l0,0c-0.5-0.5-0.5-1.3,0-1.7l10.6-10.6c0.5-0.5,1.3-0.5,1.7,0l0,0
											c0.5,0.5,0.5,1.3,0,1.7L339.3,69.6z"/>
										<path class="st18" d="M356.5,58.2l0.2,0.2c0.7,0.7,1.8,0.7,2.5,0c0.7-0.7,0.7-1.8,0-2.5l-4-4l-17.6,17.6c0.5,0.5,1.3,0.5,1.7,0
											l4.2-4.2l9.9,9.9C358.1,70.7,359.1,63.9,356.5,58.2z"/>
									</g>
								</g>
								<g id="htc_one">
									<g>
										<path class="st19" d="M272.7,134.8c0,0.8,0.7,1.5,1.5,1.5H285c0.8,0,1.5-0.7,1.5-1.5v-0.8h-13.8V134.8z M275.8,134.8h7.7v0.8
											h-7.7V134.8z"/>
										<path class="st19" d="M285,111.8h-10.7c-0.8,0-1.5,0.7-1.5,1.5v0.8h13.8v-0.8C286.5,112.5,285.8,111.8,285,111.8z M283.4,113.4
											h-7.7v-0.8h7.7V113.4z"/>
										<path class="st20" d="M272.7,134.1h13.8v-19.9h-13.8V134.1z M274.2,115.7H285v16.9h-10.7V115.7z"/>
										<rect x="274.2" y="115.7" class="st21" width="10.7" height="16.9"/>
										<rect x="275.8" y="112.6" class="st22" width="7.7" height="0.8"/>
										<rect x="275.8" y="134.8" class="st22" width="7.7" height="0.8"/>
									</g>
								</g>
							</g>
						</switch>
						</svg>
					</div>
				</div>
	  	</div>
	  </section>



	  <section class="section auto-3">
	  	<div class="inner-wrap">
	  		<h3>Creating New Designs to Meet New Regulations</h3>
	  		<p>Experts project steady growth in light vehicle sales over the next 10 years, as manufacturers worldwide are increasingly focused on building more fuel-efficient vehicles. In fact, as fuel economy regulations continue to tighten, U.S. and European automakers will be expected to average more than 60 miles per gallon by 2025.</p>  
	  		<div class="col-6">
	  		<p>American auto manufacturers have begun using <a href="/materials/open-and-closed-cell-foam-materials/">lightweight materials</a> as the first step toward achieving this goal, and it’s already paying off. When Ford reduced the weight of its F-Series trucks in 2014 by swapping out steel for aluminum, the following year’s F-150 had the best mileage of any gasoline pickup in the United States — all while maintaining its position as the best-selling domestic vehicle of any kind (by a large margin).</p>
		  	</div>
		  	<div class="col-6 col-last">
		  		<img class="animate-floating" src="<?php bloginfo('template_url'); ?>/img/auto/floating-blue-truck.svg" alt="blue truck">
		  	</div>
	  	</div>
	  </section>


	  <section class="section auto-4">
	  	<div class="inner-wrap">
	  		<p>The same principles of fuel efficiency also apply to all types of smaller components used in these vehicles. For example, CGR recently developed a solution for Ford diesel truck engines that were experiencing shutdowns during snow and ice conditions; any time the air filter intake area was clogged with snow, the engine would use excess fuel and eventually shut down from a lack of air flow.</p>  
	  		<div class="col-7">
	  			<p>To resolve this issue, we supplied a <a href="/cutting/waterjet">waterjet-cut</a> urethane airflow solution, adding an 80-PPI (pores per inch) filter foam to allow the passage of air, regardless of environmental conditions. Our team provided five different prototype parts in various shapes and porosity levels. The final version, selected by Ford, is now in full production for all their diesel truck models.</p>
	  		</div>
	  		<div class="auto-4-right">


					<!-- Generator: Adobe Illustrator 18.1.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
					<svg class="air-vent" version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 130.2 69.1" enable-background="new 0 0 130.2 69.1" xml:space="preserve">
					<g>
						<g>
							<ellipse fill="#5A5A5A" cx="67.9" cy="50.9" rx="31.1" ry="4.9"/>
							<polygon fill="#DF8176" points="97.7,56.4 38.2,56.4 46.8,7.1 89.1,7.1 		"/>
							<polygon fill="#993D3D" points="43.6,56.4 40.9,56.4 48.7,7.1 50.6,7.1 		"/>
							<polygon fill="#993D3D" points="53.3,56.4 50.6,56.4 56.3,7.1 58.3,7.1 		"/>
							<polygon fill="#993D3D" points="64.2,56.4 61,56.4 63.9,7.1 65.7,7.1 		"/>
							<polygon fill="#993D3D" points="75.1,56.4 71.9,56.4 70.3,7.1 72.2,7.4 		"/>
							<polygon fill="#993D3D" points="85.4,56.4 82.7,56.4 77.7,7.1 79.7,7.1 		"/>
							<polygon fill="#993D3D" points="95.2,56.4 92.5,56.4 85.4,7.1 87.3,7.1 		"/>
							<path fill="#E6E6E6" d="M89.4,3.7c0.3,1.6-9.3,2.9-21.5,2.9c-12.2,0-21.8-1.3-21.5-2.9C46.7,2.2,56.4,1,67.9,1
								C79.5,1,89.2,2.2,89.4,3.7z"/>
							<path fill="#B0B0B0" d="M89.9,6c0-0.2-0.1-0.5-0.1-0.7c-0.1-0.4-0.1-0.7-0.1-0.7l0,0c-0.1-0.5-0.2-0.8-0.2-0.8
								c0.3,1.6-9.3,2.9-21.5,2.9c-12.2,0-21.8-1.3-21.5-2.9c0,0-0.1,0.4-0.2,0.8v0c0,0-0.3,1.5-0.5,2.5c0,0,0,0,0,0l0,0
								c-0.1,0.4-0.1,0.8-0.2,0.9c-0.3,1.7,9.7,3.1,22.3,3.1c12.6,0,22.6-1.4,22.3-3.1C90.2,7.8,90,6.9,89.9,6z"/>
							<path fill="#5A5A5A" d="M48.4,57c0,0,0,0.5,0,1.2c0,0,0,2.3,0,3.8c0,0.6,0,1,0,1.2c0,2.7,8.7,4.9,19.5,4.9
								c10.8,0,19.5-2.2,19.5-4.9c0-0.1,0-0.6,0-1.2c0-1.5,0-3.8,0-3.8c0-0.7,0-1.2,0-1.2H48.4z"/>
							<path fill="#A3A3A3" d="M98.9,51.3c0,0.1-0.1,0.1-0.1,0.2C97,54,83.9,55.9,67.9,55.9S38.9,54,37.1,51.5c0-0.1-0.1-0.1-0.1-0.2
								c-0.1-0.1-0.1-0.3-0.1-0.4c0,0,0,0.5,0,1.2c0,0,0,2.3,0,3.8c0,0.6,0,1,0,1.2c0,2.7,13.9,4.9,31.1,4.9c17.2,0,31.1-2.2,31.1-4.9
								c0-0.1,0-0.6,0-1.2c0-1.5,0-3.8,0-3.8c0-0.7,0-1.2,0-1.2C99.1,51.1,99,51.2,98.9,51.3z"/>
						</g>
						<g>
							<path fill="#FFFFFF" d="M47.4,3.6c0,0-2.1-1.7-2.4,0.7c-0.3,2.3-0.4,3.4-0.1,4.2c0.3,0.7,0.5,0.9,0.5,1.8c0,0.8,0,3.8,0.4,5.5
								c0.4,1.8-0.8,3.7-0.2,6.3c0.6,2.5,1.1,5.8,1,7.3c0,1.5,1.5-4.2,1.6-4.5c0.1-0.3,1.5-7.3,2.1-8.4c0.6-1.1,0.6-2.7,0.6-4.2
								c0-1.4,1.5-3.5,1.5-3.5s0.3,1.6,0.9,2.3c0,0,0.1,1.2-0.6,2.8c-0.7,1.5,0.8,2.4,1,3.1c0.2,0.8-0.4,2.1-0.1,3.2
								c0.3,1.2,0.6,1.1,1.1-0.2c0.6-1.3,0.8-4.1,1.2-4.8c0.3-0.6,0.6-2.2,0.6-3.3c0-1.1,0.8-4.2,1.6-2.9c0.8,1.3,1.6,2.1,1.6,3.7
								s0.1,1.1,0.5,2.2c0.4,1.1-0.2,3.8-0.2,5.4c0,1.6,0.7,2.6,0.8,4.5c0.1,1.9,0.2,4.8,0.6,5.5s0.8-3,0.9-3.7c0.1-0.7,1.2-3.9,1.1-5.1
								c-0.2-1.2,0.9-4.4,1.2-6.1c0.3-1.7,0.2-5.8,0.2-5.8s1.5,2.5,1.7,3.2c0,0,0.8-0.6,1-2.1c0.2-1.5,1-2.2,1.3-3.1
								c0.3-0.9-0.2-2.9-1.1-3.1c-1-0.2-2.9-1.6-3.3-1.1c-0.5,0.5-4.1,2.1-5.2,1.1c-1.1-0.9-3.9-2.4-5.3-1.6c-1.4,0.8-1.8,1.2-3.2,0.7
								c-1.4-0.6-1.8-0.6-2.3-0.4c-0.5,0.2-0.5,0.2-0.5,0.2L47.4,3.6z"/>
							<path opacity="0.15" fill="#FFFFFF" d="M48.3,4.3c0,0-2.1-1.7-2.4,0.7c-0.3,2.3-0.4,3.4-0.1,4.2c0.3,0.7,0.5,0.9,0.5,1.8
								c0,0.8,0,3.8,0.4,5.5c0.4,1.8-0.8,3.7-0.2,6.3c0.6,2.5,1.1,5.8,1,7.3c0,1.5,1.5-4.2,1.6-4.5c0.1-0.3,1.5-7.3,2.1-8.4
								s0.6-2.7,0.6-4.2c0-1.4,1.5-3.5,1.5-3.5s0.3,1.6,0.9,2.3c0,0,0.1,1.2-0.6,2.8c-0.7,1.5,0.8,2.4,1,3.1c0.2,0.8-0.4,2.1-0.1,3.2
								c0.3,1.2,0.6,1.1,1.1-0.2c0.6-1.3,0.8-4.1,1.2-4.8c0.3-0.6,0.6-2.2,0.6-3.3c0-1.1,0.8-4.2,1.6-2.9c0.8,1.3,1.6,2.1,1.6,3.7
								c0,1.7,0.1,1.1,0.5,2.2c0.4,1.1-0.2,3.8-0.2,5.4c0,1.6,0.7,2.6,0.8,4.5c0.1,1.9,0.2,4.8,0.6,5.5c0.4,0.7,0.8-3,0.9-3.7
								c0.1-0.7,1.2-3.9,1.1-5.1c-0.2-1.2,0.9-4.4,1.2-6.1c0.3-1.7,0.2-5.8,0.2-5.8s1.5,2.5,1.7,3.2c0,0,0.8-0.6,1-2.1
								c0.2-1.5,1-2.2,1.3-3.1c0.3-0.9-0.2-2.9-1.1-3.1c-1-0.2-2.9-1.6-3.3-1.1c-0.5,0.5-4.1,2.1-5.2,1.1C59,4.5,56.2,3,54.8,3.8
								c-1.4,0.8-1.8,1.2-3.2,0.7c-1.4-0.6-1.8-0.6-2.3-0.4c-0.5,0.2-0.5,0.2-0.5,0.2L48.3,4.3z"/>
							<path fill="#FFFFFF" d="M61.2,32.2c0,0-0.1,0-0.3,0.4c-0.3,0.4-0.3,0.7-0.2,1.1c0.1,0.4,0.7,0.2,0.8,0s0.4-0.5,0.2-0.8
								C61.5,32.6,61.3,32.4,61.2,32.2z"/>
							<path fill="#DCE0E9" d="M46.6,29.5c0,0,0.9-2.2,1.4-4.2c0.5-2,1.6-6.6,1.8-7.4c0.2-0.8,1.3-2.1,1-3.2c-0.2-1.1,0.2-3.3,0.6-4
								c0.4-0.7,1.1-1.9,1.1-1.9s-1.4-0.7-2.7,0.6s-1.9,4.4-1.6,6.4c0.3,2-0.1,4.6-0.4,6.6C47.6,24.3,46.6,29.5,46.6,29.5z"/>
							<path fill="#DCE0E9" d="M61.5,30.4c0,0,0.6-0.6,0.8-2.9c0.2-2.3,0.6-2.8,0.7-3.2c0.1-0.4,0.5-1.6,0.4-3.4
								c-0.1-1.7,1.1-4.1,1.2-5.6c0.1-1.5,0.1-5.6,0.1-5.6s0,0.1-1,0.3c-0.9,0.2-1.7,1.9-1.5,2.6c0.1,0.6,1,2,1,3.5c0,1.5-0.6,1.6-1,2.9
								c-0.3,1.3-0.3,2.5-0.2,3.9C62.2,24.5,61.5,30.4,61.5,30.4z"/>
						</g>
						<g>
							<path fill="#DCE0E9" d="M99.1,49.6c0,0,0.8,0.1,1.3,2.5c0.5,2.4,0.5,3.2,0.8,4.3c0.3,1.2,0.4,0,0,2.3c-0.4,2.3-0.8,2.9-1.4,1.1
								c-0.6-1.8,0.1-3.4-2-3.9c-2.1-0.5-5.5-2.6-6.7-0.7c-1.2,2-1.4,4.5-2.3,3.2c-0.9-1.3-1.4-2.1-1.7-0.8c-0.4,1.3-0.1,2.5-1.3,0.5
								c-1.2-2-4.8-1.3-5.3-0.8c-0.5,0.4-2.5,1.2-4.7,0.6c-2.2-0.5-3.5-1.4-4.5-1.8c-0.9-0.4-0.1,1.5-1.9,1.4c-1.8-0.1-2.4,0.3-2.6-1.6
								c-0.2-1.9,1.3-4.2,2.3-3.9c1.1,0.3,1.7-0.3,2.3-0.8c0.7-0.5,1.2,0.5,2.6,0.8c1.4,0.3,2.2-1.7,3.4-2.4c1.2-0.7,5.4-0.7,5.9,0
								c0.5,0.7,4.5,2.2,5.1,1.4c0.5-0.8,1.9-1.3,2.3-1c0.4,0.3,0.8-0.7,2.6-1.5c1.8-0.7,2,1.2,3.2,1.3C97.5,50,99.1,49.6,99.1,49.6z"/>
							<path opacity="0.15" fill="#DCE0E9" d="M99.7,50.4c0,0,0.8,0.1,1.3,2.5c0.5,2.4,0.5,3.2,0.8,4.3c0.3,1.2,0.4,0,0,2.3
								c-0.4,2.3-0.8,2.9-1.4,1.1c-0.6-1.8,0.1-3.4-2-3.9c-2.1-0.5-5.5-2.6-6.7-0.7c-1.2,2-1.4,4.5-2.3,3.2c-0.9-1.3-1.4-2.1-1.7-0.8
								c-0.4,1.3-0.1,2.5-1.3,0.5c-1.2-2-4.8-1.3-5.3-0.8c-0.5,0.4-2.5,1.2-4.7,0.6c-2.2-0.5-3.5-1.4-4.5-1.8c-0.9-0.4-0.1,1.5-1.9,1.4
								c-1.8-0.1-2.4,0.3-2.6-1.6c-0.2-1.9,1.3-4.2,2.3-3.9c1.1,0.3,1.7-0.3,2.3-0.8c0.7-0.5,1.2,0.5,2.6,0.8c1.4,0.3,2.2-1.7,3.4-2.4
								c1.2-0.7,5.4-0.7,5.9,0c0.5,0.7,4.5,2.2,5.1,1.4c0.5-0.8,1.9-1.3,2.3-1c0.4,0.3,0.8-0.7,2.6-1.5c1.8-0.7,2,1.2,3.2,1.3
								C98.2,50.7,99.7,50.4,99.7,50.4z"/>
							<path fill="#FFFFFF" d="M86.5,60.5c0,0-0.1,0-0.5,0.7c-0.4,0.7-0.5,1.2-0.3,1.8c0.2,0.7,1.1,0.4,1.3,0c0.2-0.4,0.6-0.8,0.3-1.3
								C86.9,61.2,86.6,60.8,86.5,60.5z"/>
							<path fill="#FFFFFF" d="M68.4,52.2l-1,1.7c0,0,2.2,1.6,3.9,0.8c1.7-0.8,2.6-0.3,3.4,0c0.8,0.3,3.2,0.4,5.2,0
								c2-0.4,2.6-0.8,4.6-0.5c2,0.3,4.3,0.1,5.6-0.6c1.3-0.8,2.9-1.7,5-0.8c2,0.8,5.6,1.1,5.4-0.1c-0.3-1.2-1.2-2.9-2.4-2.8
								c-1.1,0.1-2.3-0.3-2.8-0.7s-2-0.6-3.2,0.4c-1.2,1-1.8,0.8-2,0.8c-0.2-0.1-1,0.4-1.7,0.8c-0.7,0.4-1.5,1.1-2.4,0.4
								c-0.9-0.7-5.2-2.3-5.7-2.3s-3.2,0.4-3.8,1.4c-0.6,1-1.1,1.9-2,1.5c-1-0.4-2.3-1.9-2.8-1.2c-0.5,0.7-1.2,0.9-1.7,1
								C69.5,52,68.4,52.2,68.4,52.2z"/>
						</g>
					</g>
					<g class="side-to-side">
						<path fill="#5A5A5A" d="M2.5,31.1h27.1c0.4,0,0.7-0.3,0.7-0.7c0-0.3-0.3-0.7-0.7-0.7H2.5c-0.4,0-0.7,0.3-0.7,0.7
							C1.8,30.8,2.1,31.1,2.5,31.1z"/>
						<path fill="#5A5A5A" d="M11.2,25.6c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h13.3c0.4,0,0.7-0.3,0.7-0.7
							c0-0.4-0.3-0.7-0.7-0.7"/>
					</g>
					<path class="side-to-side" fill="#5A5A5A" d="M20.3,21.8h11.7c0.2,0,0.3-0.3,0.3-0.7c0-0.3-0.1-0.7-0.3-0.7H20.3c-0.2,0-0.3,0.3-0.3,0.7
						C20,21.5,20.1,21.8,20.3,21.8z"/>
					<path class="side-to-side" fill="#5A5A5A" d="M24,16.3c-0.2,0-0.3,0.3-0.3,0.7c0,0.4,0.1,0.7,0.3,0.7h5.7c0.2,0,0.3-0.3,0.3-0.7c0-0.4-0.1-0.7-0.3-0.7"/>
					<path class="side-to-side" fill="#5A5A5A" d="M110.8,16.3c-0.2,0-0.3,0.3-0.3,0.7c0,0.4,0.1,0.7,0.3,0.7h5.7c0.2,0,0.3-0.3,0.3-0.7c0-0.4-0.1-0.7-0.3-0.7
						"/>
					<g class="side-to-side">
						<path fill="#5A5A5A" d="M114.3,25.6c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h13.3c0.4,0,0.7-0.3,0.7-0.7
							c0-0.4-0.3-0.7-0.7-0.7H114.3z"/>
					</g>
					<g class="side-to-side">
						<path fill="#5A5A5A" d="M104.5,19.4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h13.3c0.4,0,0.7-0.3,0.7-0.7
							c0-0.4-0.3-0.7-0.7-0.7H104.5z"/>
					</g>
					<g class="side-to-side">
						<path fill="#5A5A5A" d="M105,31.8c-0.7,0-1.2,0.3-1.2,0.7c0,0.4,0.5,0.7,1.2,0.7h22.2c0.7,0,1.2-0.3,1.2-0.7c0-0.4-0.5-0.7-1.2-0.7
							H105z"/>
					</g>
					</svg>


		  		<img class="auto-4-bg" src="<?php bloginfo('template_url'); ?>/img/auto/truck-w-hood.png" alt="blue truck">
		  	</div>
			</div>
		</section>	




		<section class="section auto-5">
	  	<div class="inner-wrap">
	  		<h3>Even small design tweaks can have major environmental impacts</h3>
		  	<p>As another example, our <a href="/using-3m-thinsulate-manufacture-lighter-quieter-cars/">3M Thinsulate</a> — an alternative material choice for acoustic automotive applications — greatly enhances sound insulation and acoustic absorption without adding unnecessary weight to a vehicle. Simply adhering a few square centimeters of this polyester and polypropylene non-woven fiber blend to a car’s interior panels, seatbacks, doors, luggage compartments, HVAC equipment, and engine undercovers allows for improved functioning without added weight.</p>
	  		<div class="col-7">
		  		<p>As the push for sustainability continues, you’ll notice that modifying even the most basic parts of a car can improve its fuel efficiency. To meet changing regulations, forward-thinking automakers and car component contract manufacturers have already begun adjusting their designs accordingly.</p>	
		  	</div>
		  	<div class="auto-5-left">
		  		<img class="" src="<?php bloginfo('template_url'); ?>/img/auto/3m-thinsulate.png" alt="3M Thinsulate">
		  	</div>
		  	<div class="auto-5-right">	
		  		<img class="" src="<?php bloginfo('template_url'); ?>/img/auto/car-door.svg" alt="Car Door">
		  	</div> 
			</div>
		</section>	




		<section class="section auto-6">
	  	<div class="inner-wrap">
	  		<div class="auto-6-content">
	  			<h3>Establishing New Processes for Shifting Demands</h3>
		  		<p>The rapid assimilation of technology into smart cars means automakers are working more closely with tech companies — and being forced to reconcile their distinct design processes. While automotive manufacturers typically like to take their time developing one product before moving forward with high-volume production, software companies prefer to “fail and fix” through a rapid prototyping process. To successfully integrate with the latest technologies, car makers will need to quicken their design processes to match.</p>
				
	  		</div>


				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 674.5 319.5" enable-background="new 0 0 674.5 319.5" xml:space="preserve">
				<g class="side-to-side" id="Layer_2">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M553.2,50.5c0-2.8,2.3-5.1,5.1-5.1c2.8,0,5.1,2.3,5.1,5.1
						c0,2.8-2.3,5.1-5.1,5.1C555.5,55.6,553.2,53.3,553.2,50.5z M550.4,61.5h-3.2c0.8-1.4,1.3-3.1,1.3-4.9c0-5.4-4.4-9.9-9.9-9.9
						c-4.6,0-8.5,3.2-9.6,7.4c-0.9-0.5-2-0.8-3.1-0.8c-3.5,0-6.3,2.8-6.3,6.3c0,0.6,0.1,1.2,0.3,1.8h-10.4c1.3-2.2,2-4.7,2-7.4
						c0-8.2-6.7-14.8-14.8-14.8c-6.9,0-12.7,4.8-14.4,11.2c-1.7-1.2-3.7-1.9-5.9-1.9c-5.6,0-10.2,4.6-10.2,10.2c0,1,0.1,1.9,0.4,2.8h-8
						c-3.3,0-5.9,2.7-5.9,5.9c0,3.3,2.7,5.9,5.9,5.9h91.7c3.3,0,5.9-2.7,5.9-5.9C556.3,64.2,553.7,61.5,550.4,61.5z"/>
					<path fill="#FFFFFF" d="M201.2,78.6H164c1.3-2.2,2-4.7,2-7.4c0-8.2-6.7-14.8-14.8-14.8c-6.9,0-12.7,4.8-14.4,11.2
						c-1.7-1.2-3.7-1.9-5.9-1.9c-5.6,0-10.2,4.6-10.2,10.2c0,1,0.1,1.9,0.4,2.8h-10.8c-3.3,0-5.9,2.7-5.9,5.9c0,3.3,2.7,5.9,5.9,5.9
						h90.9c3.3,0,5.9-2.7,5.9-5.9C207.2,81.3,204.5,78.6,201.2,78.6z"/>
				</g>
					 
				<g id="Layer_1">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7D391D" d="M72.7,153.9v10.4c0,1.3-1.1,2.4-2.4,2.4h-1.2
						c-1.3,0-2.4-1.1-2.4-2.4v-10.4c1,0.2,1.9,0.2,3,0.2C70.7,154.2,71.7,154.1,72.7,153.9z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7BB933" d="M69.7,154.2c9.8,0,17.8-8,17.8-17.8c0-9.8-6.5-21.7-17.8-31.2
						V154.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#A2C627" d="M69.7,154.2c-9.8,0-17.8-8-17.8-17.8c0-9.8,6.5-21.7,17.8-31.2
						V154.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7BB933" d="M69.7,142.6c-5.2,0-9.4-4.2-9.4-9.4c0-5.2,3.4-11.5,9.4-16.5
						V142.6z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#1D8136" d="M69.7,142.6c5.2,0,9.4-4.2,9.4-9.4c0-5.2-3.4-11.5-9.4-16.5V142.6
						z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7BB933" d="M64.3,186.2h10.8c2,0,3.7,1.6,3.7,3.7v3.7H60.7v-3.7
						C60.7,187.8,62.3,186.2,64.3,186.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M46.6,164.4h566.6c14.5,0,26.4,20.9,26.4,46.5v0
						c0,25.6-11.9,46.5-26.4,46.5H46.6c-14.5,0-26.4-20.9-26.4-46.5v0C20.2,185.3,32.1,164.4,46.6,164.4z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#666666" d="M83.3,279.2V70.3c0,0-0.1-10.5,15.8-8.6c0,0,1.9,139.9,30.9,177.5
						L83.3,279.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7FC652" d="M83.3,279.2V70.3c1.8,41.7,8.2,132.3,30.2,165.8l-25.9,37.8
						L83.3,279.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#666666" d="M591.2,279.2V70.3c0,0,0.1-10.5-15.8-8.6
						c0,0-1.9,139.9-30.9,177.5L591.2,279.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7FC652" d="M591.2,279.2V70.3c-1.8,41.7-8.2,132.3-30.2,165.8l25.9,37.8
						L591.2,279.2z"/>
					<rect x="83.3" y="279.2" fill-rule="evenodd" clip-rule="evenodd" fill="#383838" width="507.9" height="40.2"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M591.2,319.5v-69c-59.9-23.2-151.4-38-253.9-38
						s-194.1,14.8-253.9,38v69H591.2z"/>

					<g class="animate-road-dash">
						<rect x="331.6" y="183.5" fill-rule="evenodd" clip-rule="evenodd" fill="#F1F1F1" width="12.5" height="21.8"/>
					</g>
					<g class="animate-road-dash">
						<rect x="331.6" y="156.3" fill-rule="evenodd" clip-rule="evenodd" fill="#F1F1F1" width="12.5" height="21.8"/>
					</g>
					<g class="animate-road-dash">
						<rect x="331.7" y="129" fill-rule="evenodd" clip-rule="evenodd" fill="#F1F1F1" width="12.5" height="21.8"/>
					</g>
						
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#666666" d="M391.1,211.1c80.5,4.2,151,17.6,200.1,36.6v0.8v0.3v0.8v0.3v0.8
						v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3v0.8v0.3
						v0.8v0.3v0.8c-59.9,23.2-151.4,38-253.9,38s-194.1-14.8-253.9-38v-0.8V269v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8
						v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3V258v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8v-0.3v-0.8V252v-0.8v-0.3V250v-0.3v-0.8v-0.3
						v-0.8c49.2-19,119.7-32.4,200.1-36.6c13.6-9,32.7-14.6,53.8-14.6C358.4,196.5,377.5,202.1,391.1,211.1z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" d="M391.1,211.1c68.6,3.6,130,13.8,177.1,28.6
						c-33,3.5-68.4,5.4-105.2,5.4c-90.2,0-171.8-11.5-230.9-30c16.6-1.8,33.7-3.1,51.3-4c13.6-9,32.7-14.6,53.8-14.6
						C358.4,196.5,377.5,202.1,391.1,211.1z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#333333" d="M413.6,319.5v-50.1c0-15.9-13-28.9-28.9-28.9h-94.9
						c-15.9,0-28.9,13-28.9,28.9v50.1H413.6z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#49A6BD" d="M402.5,310.7v-41.4c0-9.8-8-17.8-17.8-17.8h-94.9
						c-9.8,0-17.8,8-17.8,17.8v41.4H402.5z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" d="M402.5,273.9v-4.6c0-3.3-0.9-6.5-2.6-9.2H274.6
						c-1.6,2.7-2.6,5.8-2.6,9.2v4.6H402.5z"/>
					<rect x="272.1" y="317.7" fill-rule="evenodd" clip-rule="evenodd" fill="#666666" width="130.4" height="1.7"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M264.4,246.3c-6.6,6.5-11.2,18.3-11.9,27.5
						c-0.6,7.3-9.8,9.4-14.1,1.3c-3.6-8.2-11.8-17-19.7-21.5c-2.9-1.6-2.6-5.9,0.5-7.1c12.3-4.8,29.2-7.2,42.5-7
						C265.2,239.6,266.9,243.8,264.4,246.3z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#999999" d="M241.9,273.4c1,1.7,3,3.8,5.1,2.8c1-0.5,1.5-1.5,1.5-2.6
						c0.1-1.7,0.4-3.5,0.7-5.3c-3.3,0.4-6.4,0.9-9.2,1.5C240.7,271,241.3,272.2,241.9,273.4L241.9,273.4z M250.2,264.4
						c0.4-1.6,0.9-3.1,1.5-4.7c-6.5,0.5-12.2,1.6-16.8,2.7c1.1,1.3,2.1,2.6,3.1,4C241.5,265.6,245.6,264.9,250.2,264.4L250.2,264.4z
						 M253.3,255.9c0.7-1.4,1.4-2.8,2.1-4.1c-11.3,0.3-20.7,2.4-26.9,4.1c1.2,1.1,2.4,2.2,3.6,3.4C237.5,257.9,244.8,256.5,253.3,255.9
						L253.3,255.9z M257.8,248c-14.4,0.1-26.2,3-32.8,5.1c-1.4-1.1-2.9-2-4.4-2.9c12-4.7,28.1-6.9,41-6.7
						C260.2,244.8,259,246.3,257.8,248z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M410.2,246.3c6.6,6.5,11.2,18.3,11.9,27.5
						c0.6,7.3,9.8,9.4,14.1,1.3c3.6-8.2,11.8-17,19.7-21.5c2.9-1.6,2.6-5.9-0.5-7.1c-12.3-4.8-29.2-7.2-42.5-7
						C409.4,239.6,407.7,243.8,410.2,246.3z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#999999" d="M432.7,273.4c-1,1.7-3,3.8-5.1,2.8c-1-0.5-1.5-1.5-1.5-2.6
						c-0.1-1.7-0.4-3.5-0.7-5.3c3.3,0.4,6.4,0.9,9.2,1.5C433.8,271,433.2,272.2,432.7,273.4L432.7,273.4z M424.4,264.4
						c-0.4-1.6-0.9-3.1-1.5-4.7c6.5,0.5,12.2,1.6,16.8,2.7c-1.1,1.3-2.1,2.6-3.1,4C433,265.6,428.9,264.9,424.4,264.4L424.4,264.4z
						 M421.3,255.9c-0.7-1.4-1.4-2.8-2.1-4.1c11.3,0.3,20.7,2.4,26.9,4.1c-1.2,1.1-2.4,2.2-3.6,3.4C437,257.9,429.7,256.5,421.3,255.9
						L421.3,255.9z M416.8,248c14.4,0.1,26.2,3,32.8,5.1c1.4-1.1,2.9-2,4.4-2.9c-12-4.7-28.1-6.9-41-6.7
						C414.3,244.8,415.6,246.3,416.8,248z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" d="M238.2,284.2c-10.1-28.6-37.4-49-69.4-49c-32,0-59.3,20.5-69.4,49
						c-2.2,9.8,7.4,10.6,14.2,10.3H224C230.9,294.8,240.4,294.1,238.2,284.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#1A1A1A" d="M102.2,292.7c3,1.8,7.6,2,11.4,1.8H224c3.8,0.2,8.4,0,11.4-1.8
						c-11.8-24.9-37.2-42.1-66.6-42.1C139.4,250.6,114,267.8,102.2,292.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#49A6BD" d="M168.8,288.2c8.6,0,15.7-7,15.7-15.7c0-8.6-7-15.7-15.7-15.7
						c-8.6,0-15.7,7-15.7,15.7C153.1,281.2,160.2,288.2,168.8,288.2z"/>
					<g>
						<path fill-rule="evenodd" clip-rule="evenodd" fill="#49A6BD" d="M199.7,288.2c5.6,0,10.1-4.6,10.1-10.1c0-5.6-4.6-10.1-10.1-10.1
							c-5.6,0-10.1,4.6-10.1,10.1C189.5,283.7,194.1,288.2,199.7,288.2z"/>
						<path fill-rule="evenodd" clip-rule="evenodd" fill="#49A6BD" d="M137.9,288.2c5.6,0,10.1-4.6,10.1-10.1c0-5.6-4.6-10.1-10.1-10.1
							c-5.6,0-10.1,4.6-10.1,10.1C127.8,283.7,132.3,288.2,137.9,288.2z"/>
					</g>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FAE2D0" d="M582.1,319.5c-7.5-3.5-15.8-5.5-24.6-5.5h-90.7
						c-8.8,0-17.1,2-24.5,5.5H582.1z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#F2D7C2" d="M582.1,319.5c-7.5-3.5-15.8-5.5-24.6-5.5h-47.1
						c-2.8,1.7-5.4,3.6-8.1,5.5H582.1z"/>
					<g>
						<rect x="486.2" y="314" fill-rule="evenodd" clip-rule="evenodd" fill="#C9AA91" width="7.2" height="5.5"/>
						<rect x="531.1" y="314" fill-rule="evenodd" clip-rule="evenodd" fill="#C9AA91" width="7.2" height="5.5"/>
					</g>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FAE1CD" d="M564.8,319.5c2.7-3.3,4.4-7.6,4.4-12.2v0
						c0-10.6-8.7-19.3-19.3-19.3h-75.3c-10.6,0-19.3,8.7-19.3,19.3v0c0,4.6,1.6,8.8,4.4,12.2H564.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#E8CAB2" d="M564.8,319.5c0.8-1,1.5-2,2.1-3.1c-3.3-6.1-9.7-10.3-17-10.3
						h-75.3c-7.3,0-13.8,4.2-17,10.3c0.6,1.1,1.3,2.2,2.1,3.1H564.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#333333" d="M337.3,203.1c18.9,0,35.4,5.5,44.2,13.7H293
						C301.8,208.6,318.3,203.1,337.3,203.1z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#49A6BD" d="M337.3,206.2c13,0,24.4,3.3,30.4,8.3h-60.8
						C312.9,209.5,324.3,206.2,337.3,206.2z"/>
					<rect x="335.9" y="267" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="3.8" height="43.7"/>
					<rect x="296.7" y="280.4" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="63.2" height="3.8"/>
					
						<rect x="365.2" y="278.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -97.6977 344.5614)" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="3.8" height="24"/>
					<rect x="335.9" y="296.6" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="66.6" height="3.8"/>
					<rect x="272.1" y="296.6" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="86.4" height="3.8"/>
					
						<rect x="291.6" y="267.2" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -111.4799 289.2828)" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="3.8" height="24"/>
					
						<rect x="302" y="277.6" transform="matrix(0.7069 -0.7073 0.7073 0.7069 -115.7841 299.7752)" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="3.8" height="24"/>
					<rect x="281.2" y="267" fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" width="3.8" height="43.7"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#E7302A" d="M337.8,288.5c2.4,0,4.3-2,4.3-4.3c0-2.4-2-4.3-4.3-4.3
						c-2.4,0-4.3,2-4.3,4.3C333.5,286.6,335.5,288.5,337.8,288.5z"/>
					
					<path class="animate-ping" fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M337.8,273.9c5.7,0,10.3,4.6,10.3,10.3c0,5.7-4.6,10.3-10.3,10.3
						s-10.3-4.6-10.3-10.3C327.6,278.5,332.2,273.9,337.8,273.9L337.8,273.9z M337.8,274.8c-5.2,0-9.4,4.2-9.4,9.4
						c0,5.2,4.2,9.4,9.4,9.4c5.2,0,9.4-4.2,9.4-9.4C347.2,279,343,274.8,337.8,274.8z" />

					<!-- <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M337.8,281.1c1.7,0,3.1,1.4,3.1,3.1c0,1.7-1.4,3.1-3.1,3.1
						c-1.7,0-3.1-1.4-3.1-3.1C334.8,282.5,336.1,281.1,337.8,281.1L337.8,281.1z M337.8,281.6c-1.4,0-2.6,1.2-2.6,2.6
						c0,1.4,1.2,2.6,2.6,2.6c1.4,0,2.6-1.2,2.6-2.6C340.5,282.7,339.3,281.6,337.8,281.6z"/> -->
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M370.8,276.5h20.9c0.8,0,1.5,0.7,1.5,1.5v0c0,0.8-0.7,1.5-1.5,1.5
						h-20.9c-0.9,0-1.5-0.7-1.5-1.5v0C369.2,277.2,369.9,276.5,370.8,276.5z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M382.8,282h8.9c0.8,0,1.5,0.7,1.5,1.5v0c0,0.8-0.7,1.5-1.5,1.5
						h-8.9c-0.9,0-1.5-0.7-1.5-1.5v0C381.2,282.7,381.9,282,382.8,282z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M335.6,209.7L335.6,209.7c0.9,0,1.6,0.7,1.6,1.5v0
						c0,0.8-0.7,1.5-1.5,1.5h-0.1c-0.8,0-1.5-0.7-1.5-1.5v0C334.1,210.4,334.8,209.7,335.6,209.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M339.5,209.7L339.5,209.7c0.9,0,1.6,0.7,1.6,1.5v0
						c0,0.8-0.7,1.5-1.5,1.5h-0.1c-0.8,0-1.5-0.7-1.5-1.5v0C338,210.4,338.7,209.7,339.5,209.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M322.1,209.7h9.1c0.8,0,1.5,0.7,1.5,1.5v0c0,0.8-0.7,1.5-1.5,1.5
						h-9.1c-0.8,0-1.5-0.7-1.5-1.5v0C320.5,210.4,321.2,209.7,322.1,209.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" d="M168.8,285c6.8,0,12.4-5.6,12.4-12.4c0-6.8-5.6-12.4-12.4-12.4
						c-6.8,0-12.4,5.6-12.4,12.4C156.4,279.4,162,285,168.8,285z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#99FFFF" d="M137.9,284.8c3.7,0,6.7-3,6.7-6.7c0-3.7-3-6.7-6.7-6.7
						c-3.7,0-6.7,3-6.7,6.7C131.2,281.8,134.2,284.8,137.9,284.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M168.8,263.1c5.2,0,9.4,4.2,9.4,9.4c0,5.2-4.2,9.4-9.4,9.4
						c-5.2,0-9.4-4.2-9.4-9.4C159.4,267.4,163.6,263.1,168.8,263.1L168.8,263.1z M168.8,265.1c-4.1,0-7.5,3.3-7.5,7.5
						c0,4.1,3.3,7.5,7.5,7.5c4.1,0,7.5-3.3,7.5-7.5C176.3,268.4,172.9,265.1,168.8,265.1z"/>
					<g>
						<path fill-rule="evenodd" clip-rule="evenodd" fill="#333333" d="M197.3,319.5l1-1.7c7.2-12.4,2.9-28.4-9.5-35.5l-11.3-6.5
							c-6.2-3.6-14.2-1.4-17.8,4.8l-22.5,39H197.3z"/>
						<g>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#333333" d="M105.1,319.5c-0.5-11.7,2.2-23.7,8.5-34.6
								c13.9-24,41-35.6,66.9-30.9c18.4,5.8-18,35.5-23.7,45.4l-9,15.6c-0.6,1.1-1.3,2.6-2,4.5h45.9l-2.7-1.6
								c5.7-9.8,14.3-55.6,28.5-42.6c10.5,12.3,15.9,28.2,15.2,44.2h12.4c1-27.3-12.7-54.1-38-68.7c-36.4-21-83-8.6-104.1,27.9
								c-7.4,12.9-10.7,27-10.2,40.8H105.1z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" d="M187.9,319.5l3.1-5.3c4.9-8.5,2-19.5-6.5-24.4l-7.7-4.5
								c-4.3-2.5-9.8-1-12.2,3.3l-17.9,30.9H187.9z"/>
						</g>
					</g>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FAE2D0" d="M232.2,319.5c-7.5-3.5-15.8-5.5-24.6-5.5H117
						c-8.8,0-17.1,2-24.6,5.5H232.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#F2D7C2" d="M232.2,319.5c-7.5-3.5-15.8-5.5-24.6-5.5h-48.4
						c-8.8,0-17.1,2-24.6,5.5H232.2z"/>
					<g>
						<rect x="139" y="314" fill-rule="evenodd" clip-rule="evenodd" fill="#C9AA91" width="7.2" height="5.5"/>
						<rect x="183.9" y="314" fill-rule="evenodd" clip-rule="evenodd" fill="#C9AA91" width="7.2" height="5.5"/>
					</g>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FAE1CD" d="M217.6,319.5c2.7-3.3,4.4-7.6,4.4-12.2v0
						c0-10.6-8.7-19.3-19.3-19.3h-75.3c-10.6,0-19.3,8.7-19.3,19.3v0c0,4.6,1.7,8.9,4.4,12.2H217.6z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#E8CAB2" d="M217.6,319.5c0.8-1,1.5-2,2.1-3.1c-3.3-6.1-9.7-10.3-17-10.3
						h-75.3c-7.3,0-13.8,4.2-17,10.3c0.6,1.1,1.3,2.2,2.1,3.1H217.6z"/>
					<rect x="318" y="302.3" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="11.7" height="6.9"/>
					<rect x="312.8" y="286.2" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5.9" height="6.9"/>
					<rect x="320.2" y="286.2" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5.9" height="6.9"/>
					<rect x="286" y="281.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="4" height="3.4"/>
					<rect x="286" y="285.7" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5.9" height="9.4"/>
					<rect x="286" y="291.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="15.1" height="3.5"/>
					<rect x="293.4" y="286.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="4" height="3.4"/>
					<rect x="289.8" y="302.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="6.7" height="5.9"/>
					<rect x="297.8" y="302.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="6.7" height="5.9"/>
					<rect x="306.8" y="302.6" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="3.4" height="3"/>
					<rect x="306.8" y="306.3" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="8.5" height="3"/>
					<rect x="349.1" y="287.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="8.8" height="8.2"/>
					<rect x="340.8" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="8.8" height="4.1"/>
					<rect x="340.8" y="306.7" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="17.3" height="2.4"/>
					<rect x="353.1" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5" height="6.6"/>
					<rect x="362.2" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5" height="4.5"/>
					<rect x="370.3" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5" height="6.6"/>
					<rect x="378.4" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5" height="4.5"/>
					<rect x="386.5" y="301.4" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="5" height="4.5"/>
					<rect x="381.7" y="286.9" fill-rule="evenodd" clip-rule="evenodd" fill="#298BA3" width="9.2" height="8.3"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7FC652" d="M5,224.7h56.8c11.8,0,21.5,9.7,21.5,21.5v24v21.6h-0.2
						c-1.5-12.1-11.9-21.6-24.5-21.6h-3.7v0H26.5c-11.8,0-21.5-9.7-21.5-21.5V224.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4EC5ED" d="M11.7,231.4v17.3c0,8.1,6.7,14.8,14.8,14.8h50.1v-17.3
						c0-8.1-6.7-14.8-14.8-14.8H11.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#8CE2FF" d="M76.6,246.3v-0.1c0-8.1-6.7-14.8-14.8-14.8H35.5v0.1
						c0,8.1,6.7,14.8,14.8,14.8H76.6z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M21.9,236.4h4.5c1.6,0,3,1.3,3,3v0c0,1.6-1.3,3-3,3h-4.5
						c-1.6,0-3-1.3-3-3v0C18.9,237.8,20.3,236.4,21.9,236.4z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M40.7,236.4h18c1.6,0,3,1.3,3,3v0c0,1.6-1.3,3-3,3h-18
						c-1.6,0-3-1.3-3-3v0C37.7,237.8,39.1,236.4,40.7,236.4z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7FC652" d="M669.5,224.7h-56.8c-11.8,0-21.5,9.7-21.5,21.5v24v21.6h0.2
						c1.5-12.1,11.9-21.6,24.5-21.6h3.7v0H648c11.8,0,21.5-9.7,21.5-21.5V224.7z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4EC5ED" d="M662.8,231.4v17.3c0,8.1-6.7,14.8-14.8,14.8h-50.1v-17.3
						c0-8.1,6.7-14.8,14.8-14.8H662.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M662.6,251.4c-1.3,6.9-7.4,12.1-14.6,12.1h-49.8
						c1.3-6.9,7.4-12.1,14.6-12.1H662.6z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7BB933" d="M46.7,253.8c-2,0-3.6-1.6-3.6-3.6c0-2,1.3-4.4,3.6-6.4V253.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#1D8136" d="M46.7,253.8c2,0,3.6-1.6,3.6-3.6c0-2-1.3-4.4-3.6-6.4V253.8z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#8CE2FF" d="M597.9,246.3v-0.1c0-8.1,6.7-14.8,14.8-14.8h26.3v0.1
						c0,8.1-6.7,14.8-14.8,14.8H597.9z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#4D4D4D" d="M12.2,251.4c1.3,6.9,7.4,12.1,14.6,12.1h49.8
						c-1.3-6.9-7.4-12.1-14.6-12.1H12.2z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#7BB933" d="M60.3,254c-3.6,0-6.5-2.9-6.5-6.5c0-3.6,2.4-8,6.5-11.4V254z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#1D8136" d="M60.3,254c3.6,0,6.5-2.9,6.5-6.5c0-3.6-2.4-8-6.5-11.4V254z"/>
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M628.9,240.9h7.8c0.3-1.9,1.9-3.3,3.8-3.3c1.1,0,2.1,0.5,2.8,1.3
						c0.4-2.6,2.7-4.7,5.5-4.7c3.1,0,5.5,2.5,5.5,5.5c0,0.4,0,0.8-0.1,1.2h1.9c1.6,0,3,1.3,3,3l0,0c0,1.6-1.3,3-3,3h-27.2
						c-1.6,0-3-1.3-3-3l0,0C625.9,242.3,627.3,240.9,628.9,240.9z"/>
					<!-- <path fill-rule="evenodd" clip-rule="evenodd" fill="#FCF0AE" d="M172.8,30.7c0-13.2,10.7-23.9,23.9-23.9s23.9,10.7,23.9,23.9
						c0,13.2-10.7,23.9-23.9,23.9S172.8,43.9,172.8,30.7z M196.7,46.1c8.5,0,15.4-6.9,15.4-15.4c0-8.5-6.9-15.4-15.4-15.4
						c-8.5,0-15.4,6.9-15.4,15.4C181.3,39.2,188.2,46.1,196.7,46.1z M152.5,15.2L152.5,15.2c0,1.6,1.3,3,3,3h12.3c1.6,0,3-1.3,3-3v0
						c0-1.6-1.3-3-3-3h-12.3C153.8,12.3,152.5,13.6,152.5,15.2z M137.4,15.2L137.4,15.2c0,1.6,1.3,3,3,3h3.1c1.6,0,3-1.3,3-3v0
						c0-1.6-1.3-3-3-3h-3.1C138.7,12.3,137.4,13.6,137.4,15.2z M226.4,15.2L226.4,15.2c0,1.6,1.3,3,3,3h27.6c1.6,0,3-1.3,3-3v0
						c0-1.6-1.3-3-3-3h-27.6C227.8,12.3,226.4,13.6,226.4,15.2z M265.3,15.2L265.3,15.2c0,1.6,1.3,3,3,3h3.9c1.6,0,3-1.3,3-3v0
						c0-1.6-1.3-3-3-3h-3.9C266.6,12.3,265.3,13.6,265.3,15.2z M217.6,5.4L217.6,5.4c0,1.6,1.3,3,3,3h18.5c1.6,0,3-1.3,3-3v0
						c0-1.6-1.3-3-3-3h-18.5C219,2.4,217.6,3.7,217.6,5.4z"/> -->
				</g>
				
				</svg>







			</div>
		</section>
		  

		<section class="section auto-7">
			
   		<div class="slide">
        <div class="inner-wrap" style="padding-bottom: 0;">
        	<h3>Collaboration</h3>
		  		<p>The Kaizen methodology is crucial to this pre-production experimentation process. A Japanese business philosophy famously used by Toyota, Kaizen promotes a culture of continuous improvement through collaboration across departments. After just a few months of utilizing this approach, companies can quickly identify opportunities for process improvements and cost savings, allowing for maximum production efficiency and minimal assembly line backlog.</p>
		  	</div>
		  	<img class="" src="<?php bloginfo('template_url'); ?>/img/auto/kaizen-collaboration.svg" alt="Kaizen Metholology">
      </div>
      <div class="slide">
				<div class="inner-wrap">
					<p>Many automakers are putting this methodology into practice through additive manufacturing — designing parts for vehicles with special software and using the geometry to print a 3D model. This form of rapid prototyping places a physical part in the designer’s hands in a matter of days or even hours, instead of taking weeks to design and build. In an industry that requires hundreds of small parts to complete each product, automakers simply can’t afford to wait.</p> 
					
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 505.4 109.5" enable-background="new 0 0 505.4 109.5" xml:space="preserve">
					<g id="Layer_1">
						<g>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3A4868" points="201.6,80.9 229.8,80.9 229.8,102.5 201.6,102.5 
								201.6,80.9 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="158.6,4 272.8,4 272.8,84.4 158.6,84.4 158.6,4 		"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" d="M186.2,105.5h58.9c0-1.9,0-3.8,0-5.7l-14.7-4.3h-14.7H201
								l-14.7,4.3V105.5L186.2,105.5z"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#82A0D1" points="186.2,99.8 245.2,99.8 245.2,105.5 186.2,105.5 
								186.2,99.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#C5C12F" points="213.9,77.7 217.5,77.7 217.5,79.8 213.9,79.8 
								213.9,77.7 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="163.1,8.3 268.3,8.3 268.3,73.2 163.1,73.2 163.1,8.3 		
								"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="164.5,15.9 266.9,15.9 266.9,71.8 164.5,71.8 
								164.5,15.9 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#D0E6BC" points="250.9,15.9 266.9,15.9 266.9,71.8 250.9,71.8 
								250.9,15.9 		"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M256.8,10.7h2.6v2.6h-2.6V10.7L256.8,10.7z M260.5,10.7
								c0.9,0,1.8,0,2.6,0c0,0.9,0,1.8,0,2.6c-0.9,0-1.8,0-2.6,0C260.5,12.5,260.5,11.6,260.5,10.7L260.5,10.7z M264.3,10.7h2.6v2.6h-2.6
								V10.7L264.3,10.7z"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" points="164.5,10.7 190.1,10.7 190.1,13.4 164.5,13.4 
								164.5,10.7 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#D0E6BC" points="164.5,34.5 180.5,34.5 180.5,18.5 164.5,18.5 
								164.5,34.5 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="169.2,29.8 175.8,29.8 175.8,23.2 169.2,23.2 
								169.2,29.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" points="164.5,18.5 180.5,18.5 180.5,15.9 164.5,15.9 
								164.5,18.5 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#D0E6BC" points="164.5,53.2 180.5,53.2 180.5,37.2 164.5,37.2 
								164.5,53.2 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="169.2,48.5 175.8,48.5 175.8,41.9 169.2,41.9 
								169.2,48.5 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" points="164.5,37.2 180.5,37.2 180.5,34.5 164.5,34.5 
								164.5,37.2 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#D0E6BC" points="164.5,71.8 180.5,71.8 180.5,55.8 164.5,55.8 
								164.5,71.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="169.2,67.1 175.8,67.1 175.8,60.5 169.2,60.5 
								169.2,67.1 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" points="164.5,55.8 180.5,55.8 180.5,53.2 164.5,53.2 
								164.5,55.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="252.8,23.2 265,23.2 265,20.5 252.8,20.5 252.8,23.2 		
								"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,27.7c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,25.9,252.8,26.8,252.8,27.7L252.8,27.7z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,32.2c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,30.4,252.8,31.3,252.8,32.2L252.8,32.2z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,36.7c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,34.9,252.8,35.8,252.8,36.7L252.8,36.7z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,41.2c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,39.4,252.8,40.3,252.8,41.2L252.8,41.2z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" d="M252.8,49.1c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,47.3,252.8,48.2,252.8,49.1L252.8,49.1z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,53.6c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,51.8,252.8,52.7,252.8,53.6L252.8,53.6z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,58.1c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,56.3,252.8,57.2,252.8,58.1L252.8,58.1z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,62.6c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,60.8,252.8,61.7,252.8,62.6L252.8,62.6z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#F68B1F" d="M252.8,67.1c4.1,0,8.1,0,12.2,0c0-0.9,0-1.8,0-2.7
								c-4.1,0-8.1,0-12.2,0C252.8,65.3,252.8,66.2,252.8,67.1L252.8,67.1z"/>
						</g>
						<g class="animate-360-rotate" style="transform-origin: 216px 0;">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#B2D894" points="198.3,31.7 215.7,23.3 233.1,31.7 215.7,41.1 
								198.3,31.7 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#487838" points="215.7,64.4 233.1,53.4 233.1,31.7 215.7,41.1 
								215.7,64.4 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="215.7,64.4 198.3,53.4 198.3,31.7 215.7,41.1 
								215.7,64.4 		"/>
						</g>
						<g>
							<polygon fill="#2F76BA" points="123.2,53.8 120.3,53.8 124.3,59.8 120.3,65.8 123.2,65.8 127.2,59.8 		"/>
							<polygon fill="#2F76BA" points="118.2,53.8 115.3,53.8 119.2,59.8 115.3,65.8 118.2,65.8 122.1,59.8 		"/>
						</g>
						<g>
							<polygon fill="#2F76BA" points="315.3,53.8 312.4,53.8 316.4,59.8 312.4,65.8 315.3,65.8 319.3,59.8 		"/>
							<polygon fill="#2F76BA" points="310.2,53.8 307.3,53.8 311.3,59.8 307.3,65.8 310.2,65.8 314.2,59.8 		"/>
						</g>
					</g>
					<g id="Layer_2">
						<g>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#82A0D1" points="16.3,67.7 50.1,47.6 50.1,4 16.3,22.4 16.3,67.7 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3A4868" points="83.9,67.7 50.1,47.6 16.3,67.7 50.1,89.1 83.9,67.7 		
								"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="83.9,67.7 50.1,47.6 50.1,4 83.9,22.4 83.9,67.7 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#85728D" points="50.1,96.8 55,105.5 45.2,105.5 50.1,96.8 		"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#695673" d="M45.7,91.3h8.8c1.1,0,2,0.9,2,2v4.5c0,1.1-0.9,2-2,2h-8.8
								c-1.1,0-2-0.9-2-2v-4.5C43.7,92.2,44.6,91.3,45.7,91.3L45.7,91.3z"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#ED5D73" points="50.1,91.3 77.1,43.4 75,43.4 50.1,87.6 25.2,43.4 
								23.1,43.4 50.1,91.3 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="33.3,30.6 50.1,22.5 66.9,30.6 66.9,51.5 50.1,62.1 
								33.3,51.5 33.3,30.6 		"/>
						</g>
						<g class="animate-360-rotate" style="transform-origin: 50px 0;">
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#B2D894" points="35.3,31.8 50.1,24.7 65,31.8 50.1,39.9 35.3,31.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#487838" points="50.1,59.8 65,50.4 65,31.8 50.1,39.9 50.1,59.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" points="50.1,59.8 35.3,50.4 35.3,31.8 50.1,39.9 50.1,59.8 		
								"/>
						</g>
					</g>
					<g id="Layer_3">
						<g>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#D0E6BC" d="M428.2,9.1h46.3c2,0,3.7,1.7,3.7,3.7v11.1h2.2V12.8
								c0-3.2-2.7-5.9-5.9-5.9h-46.3c-3.2,0-5.9,2.7-5.9,5.9v11.1h2.2V12.8C424.5,10.8,426.2,9.1,428.2,9.1L428.2,9.1z"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="456.1,93.1 456.1,81.4 390.7,81.4 390.7,93.1 
								456.1,93.1 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3A4868" points="470.2,98.6 470.2,85.8 376.6,85.8 376.6,98.6 
								470.2,98.6 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="429.3,41.2 429.3,22.2 417.5,22.2 417.5,41.2 
								429.3,41.2 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3A4868" points="433,14.3 433,24.6 413.8,24.6 413.8,14.3 433,14.3 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#695673" points="429.3,47 423.4,54.9 417.5,47 429.3,47 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3A4868" points="432.1,40.4 432.1,47 414.7,47 414.7,40.4 432.1,40.4 		
								"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#695673" points="470.2,33.5 470.2,31.3 376.6,31.3 376.6,33.5 
								470.2,33.5 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="377.2,22.2 377.2,105.4 353.7,105.4 353.7,22.2 
								377.2,22.2 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#2F76BA" points="491.1,22.2 491.1,105.4 467.6,105.4 467.6,22.2 
								491.1,22.2 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="472.2,56.8 478.4,56.8 478.4,60.9 472.2,60.9 
								472.2,56.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="472.2,66.8 478.4,66.8 478.4,62.6 472.2,62.6 
								472.2,66.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="486.4,56.8 480.2,56.8 480.2,60.9 486.4,60.9 
								486.4,56.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="486.4,66.8 480.2,66.8 480.2,62.6 486.4,62.6 
								486.4,66.8 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="472.2,55 478.4,55 478.4,50.9 472.2,50.9 472.2,55 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="472.2,68.5 478.4,68.5 478.4,72.6 472.2,72.6 
								472.2,68.5 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="486.4,55 480.2,55 480.2,50.9 486.4,50.9 486.4,55 		"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#A8BADF" points="486.4,68.5 480.2,68.5 480.2,72.6 486.4,72.6 
								486.4,68.5 		"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#DAD026" d="M479.3,39c1.4,0,2.6,1.2,2.6,2.6c0,1.4-1.2,2.6-2.6,2.6
								c-1.4,0-2.6-1.2-2.6-2.6C476.7,40.2,477.9,39,479.3,39L479.3,39z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#81C452" d="M479.3,30.1c1.4,0,2.6,1.2,2.6,2.6c0,1.4-1.2,2.6-2.6,2.6
								c-1.4,0-2.6-1.2-2.6-2.6C476.7,31.3,477.9,30.1,479.3,30.1L479.3,30.1z"/>
							<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#695673" points="483.5,22.2 475.2,22.2 475.2,18.5 483.5,18.5 
								483.5,22.2 		"/>
						</g>
						<rect class="animate-3d-print-1" id="_x31_" x="412.8" y="74.4" fill="#81C452" width="21.2" height="7.1"/>
						<rect class="animate-3d-print-2" id="_x32_" x="412.8" y="67.3" fill="#81C452" width="21.2" height="7.1"/>
						<rect class="animate-3d-print-3" id="_x33_" x="412.8" y="60.3" fill="#81C452" width="21.2" height="7.1"/>
					</g>
					</svg>

		  		<p>Even after aligning their prototyping processes with their tech business partners’, traditional auto manufacturers still lack some of the specific technologies needed to make connected, intelligent cars — features like web networking, sensors, and software. This skill deficit has been an invitation for high-tech companies like Apple and Google to begin creating proprietary critical components for increasingly autonomous automobiles’ networking and communications capabilities.</p>
		  		
		  	</div>
      </div>
          
	  </section>



	  <section class="section auto-8">
			<div class="slide">
      	<div class="inner-wrap">  
        	<div class="col-8">
          	<h3>Developing Better Relationships with Suppliers</h3>
          	<p>Despite the excitement involved in building lighter, smarter cars, automakers cannot allow budding business opportunities to detract from their existing supplier relationships. According to a <a href="http://www.ppi1.com/2016-annual-north-american-automotive-oem-tier-1-supplier-working-relations-index-study/" target="_blank">2016 North American Automotive OEM-Supplier Working Relations Index Study </a>from Mississippi-based consulting firm Planning Perspectives Inc., suppliers ranked auto OEMs’ interaction efforts as “adequate,” at best.</p>
          	<p>Planning Perspectives President John Henke added, “At a time of record profits when the automakers should be investing in building more collaborative relations with their suppliers, the major indicators of this year’s study suggest this isn’t happening.”</p>
          </div>
          <div class="col-4 col-last">
          	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 87.3 87.3" enable-background="new 0 0 87.3 87.3" xml:space="preserve">
						<polygon fill="#9fd27e" points="32.1,46.4 14.1,61.9 14.1,73.1 32.1,73.1 "/>
						<polygon fill="#9fd27e" points="42.6,37.4 38.1,41.3 38.1,73.1 56.1,73.1 56.1,46 "/>
						<polygon fill="#9fd27e" points="62.1,39.7 62.1,73.1 80.1,73.1 80.1,23.6 77.7,21.5 "/>
						<polygon fill="#9fd27e" points="42.2,32.5 55.7,41 77.3,15.9 81.8,19.7 85.1,2.1 68.2,8.1 72.7,11.9 54.5,33.2 41.7,25 14.1,48.7 14.1,56.6 "/>
						<polygon fill="#9fd27e" points="8.1,79.1 8.1,2.1 2.1,2.1 2.1,85.1 85.1,85.1 85.1,79.1 "/>
						</svg>
          </div>
        </div> 
      </div>
      <div class="slide">
      	<div class="inner-wrap">
      		<div class="col-7">  
      			<p>Price negotiations appear to be one of the biggest strains on these relationships and have the potential to greatly hurt profits. Car makers spend up to 70-80% of their revenue on auto parts from suppliers, but by being too aggressive during negotiations, automakers are not only failing to receive the discounts they’re looking for, they’re also frustrating suppliers in the process.</p>
      			<p>OEMs — and purchasing departments, especially — will need to invest more resources into employee training and partnership programs at every level in order to build trust with suppliers. It may seem like a nonessential focus now, but having solid, reliable supplier relationships is critical for cost savings and productivity when business isn’t booming.</p>
      		</div>
      		<div class="col-5 col-last">
      			<p class="blockquote">Car makers spend up to 70-80% of their revenue on auto parts from suppliers.</p>
      		</div>
        </div>
      </div>
        
    </section>


    <section class="section auto-9">
    	<div class="inner-wrap">
    		<h3>Learn More</h3>
    		<p>We hope you find this information helpful as you hone your business strategy for 2017. <br>Any questions about emerging auto trends? <a href="http://info.cgrproducts.com/contact-us" target="_blank">Contact us today</a> to learn more.</p>
    		<a href="http://info.cgrproducts.com/contact-us" target="_blank" class="btn">Contact Us</a>&emsp; &nbsp;<a href="/2017-automotive-manufacturing-report/#auto-1" class="btn-invert">Return to Top</a>

    	</div>
    </section> 

    </div><!-- /#fullpage -->
	    
	</section><!-- /site-content main -->

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/recommended-resources' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>