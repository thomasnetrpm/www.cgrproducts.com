<?php
/*
	Template Name: Resource
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
	    <div class="inner-wrap">
	    <div class="gdd-005-a-resource-library-infographic">
					<h1 class="scp-page-title">
						<?php if(get_field('alternative_h1')){
                echo get_field('alternative_h1');
            }
            else {
                the_title();
            }
            ?>
					</h1>
					
					<p>Need more information about a material for your project? Want to check on the tolerances required for your material? CGR Products offers free resources and information about material selection, design tips and more.</p>
					<p>Click the resource below to access the guide.</p>
					</div>
					<div class="gdd-005-b-resource-library-infographic">
					<div class="rl-left-content">
                      <h1 class="scp-page-title">
						<?php if(get_field('alternative_h1')){
                echo get_field('alternative_h1');
            }
            else {
                the_title();
            }
            ?>
					</h1>
					
					<p>Need more information about a material for your project? Want to check on the tolerances required for your material? CGR Products offers free resources and information about material selection, design tips and more.</p>
					
</div>
<div class="rl-right-content">
<h2>Featured Resource</h2>
<a href="https://info.cgrproducts.com/lp-closed-cell-sponge-rubber-with-adhesive-ebook"><img class="rl-ebook-img wp-image-1380" src="https://cgrproducts.staging.wpengine.com/wp-content/uploads/3d-ULSpongeRubber.png" alt="" /></a>
<h3 class="rl-ebook-title"><a href="https://info.cgrproducts.com/lp-closed-cell-sponge-rubber-with-adhesive-ebook" target="_blank" rel="noopener">UL Listed Closed Cell Sponge Rubber with Adhesive</a></h3>
</div>
	<div class="rl-navigation">
	<h3>Click here to navigate:</h3>
	<a href="#white-papers"> White Papers & Design Guides</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#material-data">Material Data</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#certifications">Certifications</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#infographics">Infographics </a></div>
    
					

</div>
</div>
         
           
<?php Starkers_Utilities::get_template_parts( array( 'parts/featured-resource-module-new' ) ); ?>     
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
    
	<section class="site-content" role="main">
	    <div class="inner-wrap">
					 <article class="site-content-primary col-9"> 
	       		<?php the_content(); ?> 
						<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
						<?php if (is_page( '350' )) : ?>
							<!--Sitemap Page-->
						   <ul>
						    <?php
						    // Add pages you'd like to exclude in the exclude here
						    wp_list_pages(
						    array(
						    'exclude' => '',
						    'title_li' => '',
						    )
						    );
						    ?>
						   </ul>

						
						<?php endif; ?>                    
	        </article>
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>  
	          <?php Starkers_Utilities::get_template_parts( array( 'parts/gdd-005-b-resource-library-infographic') ); ?>   
		</div>
	</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/recommended-resources' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/cta-banner-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>